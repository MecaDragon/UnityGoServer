using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum EnemyType
{
    Spider = 0,
    Shade = 1,
    Bat = 2,
}

public class EnemyManager
{
    Dictionary<int, Enemy> _enemys = new Dictionary<int, Enemy>();

    public static EnemyManager Instance { get; } = new EnemyManager();

    public void Add(SC_EnemyCreate packet)
    {
        EnemyType type = (EnemyType)packet.etype;
        Object obj = Resources.Load(type.ToString());
        GameObject go = Object.Instantiate(obj) as GameObject;

        Enemy enemy = go.AddComponent<Enemy>();
        enemy.transform.position = new Vector3(packet.posX, packet.posY, packet.posZ);
        enemy.maxHealth = 100;
        enemy.curHealth = 100;
        enemy.target = null;
        enemy.isChase = false;
        enemy.isAttack = false;
        enemy.id = packet.id;
        enemy.hp = packet.hp;
        enemy.transform.rotation = Quaternion.Euler(0, packet.rotate, 0);

        _enemys.Add(packet.id, enemy);
    }

    public void StartChase(SC_EnemyStartChase packet)
    {
        Enemy enemy = null;
        if (_enemys.TryGetValue(packet.id, out enemy))
        {
            enemy.transform.position = new Vector3(packet.posX, packet.posY, packet.posZ);
            //enemy.target = PlayerManager.Instance.GetPlayer(packet.target).transform;
            enemy.ChaseStart();
        }
    }

    public void Chase(SC_EnemyChase packet)
    {
        Enemy enemy = null;
        if (_enemys.TryGetValue(packet.id, out enemy))
        {
            enemy.transform.position = new Vector3(packet.posX, packet.posY, packet.posZ);
            enemy.transform.LookAt(new Vector3(packet.targetPosX, packet.targetposY, packet.targetposZ));
        }
    }

    public void StopChase(SC_EnemyStopChase packet)
    {
        Enemy enemy = null;
        if (_enemys.TryGetValue(packet.id, out enemy))
        {
            enemy.transform.position = new Vector3(packet.posX, packet.posY, packet.posZ);
            enemy.ChaseStop();
        }
    }

    public void Attack(SC_EnemyAttack packet)
    {
        Enemy enemy = null;
        if (_enemys.TryGetValue(packet.id, out enemy))
        {
            enemy.StartAttack();
        }
    }

    public void Hit(SC_EnemyHit packet)
    {
        Enemy enemy = null;
        if (_enemys.TryGetValue(packet.id, out enemy))
        {
            enemy.Hit(packet.hp);
        }
    }

    public void Die(SC_EnemyDie packet)
    {
        Enemy enemy = null;
        if (_enemys.TryGetValue(packet.id, out enemy))
        {
            _enemys.Remove(packet.id);
            enemy.Die(packet.hp);
        }
    }
}
