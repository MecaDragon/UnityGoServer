using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Follow : MonoBehaviour
{
    public static Transform target;

    void Update()
    {
        if (target == null)
            return;
        transform.position = target.position + new Vector3(0,11,-9);
    }
}
