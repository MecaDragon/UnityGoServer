using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum BossType
{
    Phantom = 0,
    Shadow = 1,
}

public class BossManager : MonoBehaviour
{
    Dictionary<int, Boss> _bosses = new Dictionary<int, Boss>();

    public static BossManager Instance { get; } = new BossManager();

    public void Add(SC_BossCreate packet)
    {
        BossType type = (BossType)packet.btype;
        Object obj = Resources.Load(type.ToString());
        GameObject go = Object.Instantiate(obj) as GameObject;

        Boss boss = go.AddComponent<Boss>();
        boss.transform.position = new Vector3(packet.posX, packet.posY, packet.posZ);
        boss.maxHealth = 100;
        boss.curHealth = 100;
        boss.target = null;
        boss.isChase = false;
        boss.isAttack = false;
        boss.id = packet.id;
        boss.hp = packet.hp;
        boss.btype = type;
        boss.transform.rotation = Quaternion.Euler(0, packet.rotate, 0);

        _bosses.Add(packet.id, boss);
    }

    public void Attack(SC_BossAttack packet)
    {
        Boss boss = null;
        if (_bosses.TryGetValue(packet.id, out boss))
        {
            boss.StartAttack();
        }
    }
    /*
    public void Hit(SC_BossHit packet)
    {
        Enemy enemy = null;
        if (_enemys.TryGetValue(packet.id, out enemy))
        {
            enemy.Hit(packet.hp);
        }
    }

    public void Die(SC_BossDie packet)
    {
        Enemy enemy = null;
        if (_enemys.TryGetValue(packet.id, out enemy))
        {
            _enemys.Remove(packet.id);
            enemy.Die(packet.hp);
        }
    }
    */
}
