using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerChatList : MonoBehaviour
{
    public Transform target;
    Canvas canvas;
    Text textMesh;
    int index = 0;
    public float timer { get; set; }

    private void OnEnable()
    {
        canvas = GetComponent<Canvas>();
        canvas.renderMode = RenderMode.WorldSpace;
        canvas.worldCamera = Camera.main;

        textMesh = GetComponentInChildren<Text>();
    }

    public void SetIndex(int i)
    {
        index = i + 1;
    }

    public void SetTextTimer(string text)
    {
        timer = 0;
        textMesh.text = text;
    }

    public void SetText(string text)
    {
        textMesh.text = text;
    }

    public string GetText()
    {
        return textMesh.text;
    }

    void Start()
    {
        gameObject.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
        if (target == null)
            return;

        transform.position = target.position + new Vector3(0, 3.014f + index * 0.85f - (index-1) * 0.1f, 0);
        transform.rotation = Quaternion.Euler(60, 0, 0);

        timer += Time.deltaTime;
        if(timer > 5)
        {
            gameObject.SetActive(false);
        }
    }
}
