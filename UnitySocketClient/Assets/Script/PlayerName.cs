using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;

public class PlayerName : MonoBehaviour
{

    // Update is called once per frame
    public Transform target;
    Canvas canvas;
    Text textMesh;
    public string playerName;

    private void Start()
    {
        canvas = GetComponent<Canvas>();
        canvas.renderMode = RenderMode.WorldSpace;
        canvas.worldCamera = Camera.main;

        textMesh = GetComponentInChildren<Text>();
    }

    void Update()
    {
        if (target == null)
            return;
        transform.position = target.position + new Vector3(0, 3.014f, 0);
        textMesh.text = playerName;
        //transform.rotation = Quaternion.Euler(60, 0, 0);
    }
}
