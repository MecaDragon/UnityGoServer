using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Followtmp : MonoBehaviour
{
    public Transform target;

    void Update()
    {
        if (target == null)
            return;
        transform.position = target.position + new Vector3(0, 11, -9);
    }
}
