using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class LoginManager : MonoBehaviour
{
    private NetworkManager _networkManager;

    Button Btn_Login;
    GameObject Go_Login;
    InputField IPF_Login;
    Text T_ErrorMsg;
    // Start is called before the first frame update
    void Start()
    {
        Btn_Login = GameObject.Find("Login").GetComponent<Button>();
        Btn_Login.onClick.AddListener(delegate { Login(); });

        IPF_Login = GameObject.Find("InputField").GetComponent<InputField>();
        IPF_Login.onSubmit.AddListener(delegate { Login(); });

        T_ErrorMsg = GameObject.Find("ErrorText").GetComponent<Text>();

        Object Obj_Login = Resources.Load("loadingSprite");
        Go_Login = Object.Instantiate(Obj_Login) as GameObject;
        Go_Login.SetActive(false);
        T_ErrorMsg.gameObject.SetActive(false);
    }

    public void Login()
    {
        Btn_Login.gameObject.SetActive(false);
        IPF_Login.gameObject.SetActive(false);
        T_ErrorMsg.gameObject.SetActive(false);
        Go_Login.SetActive(true);

        _networkManager = GameObject.Find("NetworkManager").GetComponent<NetworkManager>();
        CS_Login loginPacket = new CS_Login()
        {
            name = IPF_Login.text,
        };

        if (_networkManager != null)
        {
            _networkManager.Send(loginPacket.Write());
        }
    }

    public void LoginResult(bool success, string reason)
    {
        if(success == true)
        {
            StartCoroutine(LoadScene("SkinScene"));
        }
        else
        {
            Btn_Login.gameObject.SetActive(true);
            IPF_Login.gameObject.SetActive(true);
            T_ErrorMsg.gameObject.SetActive(true);
            Go_Login.gameObject.SetActive(false);

            Btn_Login.onClick.AddListener(delegate { Login(); });
            IPF_Login.onSubmit.AddListener(delegate { Login(); });

            T_ErrorMsg.text = reason;
        }
    }

    IEnumerator LoadScene(string sceneName)
    {
        AsyncOperation op = SceneManager.LoadSceneAsync(sceneName);
        op.allowSceneActivation = false;

        float timer = 0.0f;
        while (!op.isDone)
        {
            yield return null;

            timer += Time.deltaTime;
            if (timer > 3f) 
            {
                op.allowSceneActivation = true;
            }
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
