using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerChat : MonoBehaviour
{
    InputField InputField;
    Text ChatText;
    Canvas canvas;
    public Transform target;

    void OnEnable()
    {
        canvas = transform.Find("UI_Text").GetComponentInChildren<Canvas>();
        ChatText = transform.Find("UI_Text").GetComponentInChildren<Text>();

        canvas.renderMode = RenderMode.WorldSpace;
        canvas.worldCamera = Camera.main;

        InputField = GetComponentInChildren<InputField>();
        InputField.onValueChanged.AddListener(OnValueChanged);
        InputField.onSubmit.AddListener(delegate { OnEndEdit();  });
    }

    void Start()
    {
        gameObject.SetActive(false);
    }

    public void OnEnterChat()
    {
        //InputField.Select();
        InputField.ActivateInputField();
    }

    void OnValueChanged(string text)
    {
        ChatText.text = text;
    }

    void OnEndEdit()
    {
        if(InputField.text.Length > 0)
        {
            NetworkManager _networkManager = GameObject.Find("NetworkManager").GetComponent<NetworkManager>();
            CS_Chat chatPacket = new CS_Chat()
            {
                chat = InputField.text,
            };

            if (_networkManager != null)
            {
                _networkManager.Send(chatPacket.Write());
            }
        }
        InputField.text = "";
    }

    void Update()
    {
        if (target == null)
            return;
        transform.position = target.position + new Vector3(0, 0.85f, 0);
    }
}
