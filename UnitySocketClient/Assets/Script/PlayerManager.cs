using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerManager
{
    MyPlayer _myPlayer;
    Dictionary<int, Player> _players = new Dictionary<int, Player>();

    public static PlayerManager Instance { get; } = new PlayerManager();

    public Player GetPlayer(int playerId)
    {
        Player player = null;
        _players.TryGetValue(playerId, out player);
        return player;
    }

    public void Add(SC_LoginPlayer packet)
    {
        Skin skin = (Skin)packet.skin;
        Object obj = Resources.Load(skin.ToString());
        GameObject go = Object.Instantiate(obj) as GameObject;

        Object obj2 = Resources.Load("UI_PlayerName");
        GameObject go2 = Object.Instantiate(obj2) as GameObject;

        Object obj3 = Resources.Load("UI_Chat");
        GameObject go3 = Object.Instantiate(obj3) as GameObject;

        Object[] chatObj = new Object[5];
        GameObject[] chatGo = new GameObject[5];
        for (int i =0; i<chatObj.Length; i++)
        {
            chatObj[i] = Resources.Load("UI_PlayerName");
            chatGo[i] = Object.Instantiate(chatObj[i]) as GameObject;
        }

        if (packet.isSelf)
        {
            MyPlayer myPlayer = go.AddComponent<MyPlayer>();
            myPlayer.weapon = go.transform.Find("root").transform.Find("pelvis").transform.Find("Weapon").gameObject.AddComponent<Weapon>();
            myPlayer.weapon.rate = 1f;

            myPlayer.PlayerId = packet.playerId;
            myPlayer.skin = skin;
            myPlayer.transform.position = new Vector3(packet.posX, packet.posY, packet.posZ);
            _myPlayer = myPlayer;
            Follow.target = myPlayer.transform;
            myPlayer.nameText = go2.AddComponent<PlayerName>();
            myPlayer.nameText.target = myPlayer.transform;
            myPlayer.nameText.playerName = packet.name;
            myPlayer.chatText = go3.AddComponent<PlayerChat>();
            myPlayer.chatText.target = myPlayer.transform;

            for (int i = 0; i < myPlayer.chatList.Length; i++)
            {
                myPlayer.chatList[i] = chatGo[i].AddComponent<PlayerChatList>();
                myPlayer.chatList[i].SetIndex(i);
            }
        } else {
            Player player = go.AddComponent<Player>();
            player.PlayerId = packet.playerId;
            player.skin = skin;
            player.transform.position = new Vector3(packet.posX, packet.posY, packet.posZ);
            player.transform.rotation = Quaternion.Euler(packet.lookX, packet.lookY, packet.lookZ);
            _players.Add(packet.playerId, player);
            player.nameText = go2.AddComponent<PlayerName>();
            player.nameText.target = player.transform;
            player.nameText.playerName = packet.name;
            player.chatText = go3.AddComponent<PlayerChat>();
            player.chatText.target = player.transform;

            for(int i=0; i< player.chatList.Length; i++)
            {
                player.chatList[i] = chatGo[i].AddComponent<PlayerChatList>();
                player.chatList[i].SetIndex(i);
            }
        }
    }

    public void Move(SC_BroadcastMove packet)
    {
        if (_myPlayer == null)
            return;

        if(_myPlayer.PlayerId == packet.playerId) {
            // TODO : 이동 동기화가 가장 어려운 부분 - 나중에 컨텐츠쪽에서 다루게 
            // 1안) 허락 패킷이 오면 그 때 이동하거나
            // 2안) 클라에서 이동먼저 하고, 서버에서 온값 기준으로 보정
            // 이 예제에서는 1안이 왔다 가정하고 보정
            // _myPlayer.transform.position = new Vector3(packet.posX, packet.posY, packet.posZ);
        } else {
            Player player = null;
            if(_players.TryGetValue(packet.playerId, out player)) {
                player.transform.position = new Vector3(packet.posX, packet.posY, packet.posZ);
                player.transform.rotation = Quaternion.Euler(packet.lookX, packet.lookY, packet.lookZ);
                player.anim.SetBool("isRun", packet.isRun);
                player.anim.SetBool("isWalk", packet.isWalk);
            }
        }
    }

    public void Roll(int playerId)
    {
        if (_myPlayer == null)
            return;

        if (_myPlayer.PlayerId != playerId)
        {
            Player player = null;
            if (_players.TryGetValue(playerId, out player))
            {
                player.anim.SetTrigger("doRoll");
            }
        }
    }

    public void Attack(int playerId)
    {
        if (_myPlayer == null)
            return;

        if (_myPlayer.PlayerId != playerId)
        {
            Player player = null;
            if (_players.TryGetValue(playerId, out player))
            {
                player.anim.SetTrigger("doSwing");
            }
        }
    }

    public void Hit(int playerId, int _hp)
    {
        if (_myPlayer == null)
        {
            _myPlayer.hp = _hp;
            return;
        }

        if (_myPlayer.PlayerId != playerId)
        {
            Player player = null;
            if (_players.TryGetValue(playerId, out player))
            {
                player.anim.SetTrigger("doHit");
                player.hp = _hp;
            }
        }
    }

    public void Chat(SC_Chat packet)
    {
        if (_myPlayer == null)
            return;

        if (_myPlayer.PlayerId == packet.playerId)
        {
            for(int i= _myPlayer.chatList.Length-2; i>=0; i--)
            {
                if(_myPlayer.chatList[i].gameObject.activeSelf == true)
                {
                    if (_myPlayer.chatList[i + 1].gameObject.activeSelf == false)
                    {
                        float tmpTime = _myPlayer.chatList[i].timer;
                        string tmpText = _myPlayer.chatList[i].GetText();
                        _myPlayer.chatList[i + 1].gameObject.SetActive(true);
                        _myPlayer.chatList[i + 1].SetText(tmpText);
                        _myPlayer.chatList[i + 1].timer = tmpTime;
                        _myPlayer.chatList[i + 1].SetIndex(i + 2);
                        _myPlayer.chatList[i + 1].target = _myPlayer.transform;
                    }
                    else
                    {
                        string tmpText = _myPlayer.chatList[i].GetText();
                        _myPlayer.chatList[i + 1].SetText(tmpText);
                    }
                }
            }

            _myPlayer.chatList[0].SetIndex(1);
            _myPlayer.chatList[0].gameObject.SetActive(true);
            _myPlayer.chatList[0].SetTextTimer(packet.chat);
            _myPlayer.chatList[0].target = _myPlayer.transform;
        }
        else
        {
            Player player = null;
            if (_players.TryGetValue(packet.playerId, out player))
            {
                for (int i = player.chatList.Length - 2; i >= 0; i--)
                {
                    if (player.chatList[i].gameObject.activeSelf == true)
                    {
                        if (player.chatList[i + 1].gameObject.activeSelf == false)
                        {
                            float tmpTime = player.chatList[i].timer;
                            string tmpText = player.chatList[i].GetText();
                            player.chatList[i + 1].gameObject.SetActive(true);
                            player.chatList[i + 1].SetText(tmpText);
                            player.chatList[i + 1].timer = tmpTime;
                            player.chatList[i + 1].SetIndex(i + 1);
                            player.chatList[i + 1].target = player.transform;
                        }
                        else
                        {
                            string tmpText = player.chatList[i].GetText();
                            player.chatList[i + 1].SetText(tmpText);
                        }
                    }
                }

                player.chatList[0].SetIndex(0);
                player.chatList[0].gameObject.SetActive(true);
                player.chatList[0].SetTextTimer(packet.chat);
                player.chatList[0].target = player.transform;
            }
        }
    }

    public void EnterGame(SC_BroadcastEnterGame packet)
    {
        // 내 자신은 중복 처리하지 않음
        if(packet.playerId == _myPlayer.PlayerId) {
            return;
        }

        Skin skin = (Skin)packet.skin;
        Object obj = Resources.Load(skin.ToString());
        GameObject go = Object.Instantiate(obj) as GameObject;

        Object obj2 = Resources.Load("UI_PlayerName");
        GameObject go2 = Object.Instantiate(obj2) as GameObject;

        Object obj3 = Resources.Load("UI_Chat");
        GameObject go3 = Object.Instantiate(obj3) as GameObject;

        Object[] chatObj = new Object[5];
        GameObject[] chatGo = new GameObject[5];
        for (int i = 0; i < chatObj.Length; i++)
        {
            chatObj[i] = Resources.Load("UI_PlayerName");
            chatGo[i] = Object.Instantiate(chatObj[i]) as GameObject;
        }

        Player player = go.AddComponent<Player>();
        player.PlayerId = packet.playerId;
        player.skin = skin;
        player.transform.position = new Vector3(packet.posX, packet.posY, packet.posZ);
        _players.Add(packet.playerId, player);
        player.nameText = go2.AddComponent<PlayerName>();
        player.nameText.target = player.transform;
        player.nameText.playerName = packet.name;
        player.chatText = go3.AddComponent<PlayerChat>();
        player.chatText.target = player.transform;

        for (int i = 0; i < player.chatList.Length; i++)
        {
            player.chatList[i] = chatGo[i].AddComponent<PlayerChatList>();
            player.chatList[i].SetIndex(i);
        }
    }

    public void LeaveGame(SC_BroadcastLeaveGame packet)
    {
        if(packet.playerId == _myPlayer.PlayerId) {
            GameObject.Destroy(_myPlayer.gameObject);
            _myPlayer = null;
        } else {
            Player player = null;
            if(_players.TryGetValue(packet.playerId, out player)) {
                GameObject.Destroy(player.gameObject);
                GameObject.Destroy(player.nameText.gameObject);
                for(int i =0; i< player.chatList.Length; i++)
                {
                    GameObject.Destroy(player.chatList[i].gameObject);
                }
                _players.Remove(packet.playerId);
            }
        }
    }
}
