using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Boss : MonoBehaviour
{
    private NetworkManager _networkManager;

    public int maxHealth;
    public int curHealth;
    public Transform target;
    public BoxCollider meleaArea;
    public bool isChase;
    public bool isAttack = false;
    public bool isDamaged = false;
    public int id;
    public int hp;
    public BossType btype;

    Rigidbody rigid;
    BoxCollider boxCollider;
    Material mat;
    Animator anim;

    ParticleSystem ps;

    void Start()
    {
        _networkManager = GameObject.Find("NetworkManager").GetComponent<NetworkManager>();
        boxCollider = GetComponent<BoxCollider>();
        //meleaArea = transform.Find("AttackBox").GetComponent<BoxCollider>();
        //meleaArea.enabled = false;
        anim = GetComponent<Animator>();

        ps = GetComponentInChildren<ParticleSystem>();
        ps.Stop();
    }

    public void StartAttack()
    {
        if (isAttack == false)
            StartCoroutine(Attack());
    }

    IEnumerator Attack()
    {
        switch (btype)
        {
            case (BossType.Phantom):
                ps.Play();
                isChase = false;
                isAttack = true;
                anim.SetTrigger("doAttack");

                //meleaArea.enabled = true;
                yield return new WaitForSeconds(ps.main.duration);
                //meleaArea.enabled = false;

                isChase = true;
                isAttack = false;
                ps.Stop();
                break;
            case (BossType.Shadow):
                ps.Play();
                isChase = false;
                isAttack = true;
                anim.SetTrigger("doAttack");

                //meleaArea.enabled = true;
                yield return new WaitForSeconds(ps.main.duration);
                //meleaArea.enabled = false;

                isChase = true;
                isAttack = false;
                ps.Stop();
                break;
        }
    }

    void Update()
    {
        /*
        if(ps.time >= ps.main.duration)
        {
            ps.Stop();
        }


        if (Input.GetKeyDown(KeyCode.Space))
        {
            if(ps.isStopped)
            {
                ps.Play();
            }
        }
        */
    }
}
