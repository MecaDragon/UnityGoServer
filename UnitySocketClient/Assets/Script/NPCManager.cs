using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NPCManager
{
    Dictionary<int, NPC> _npcs = new Dictionary<int, NPC>();

    public static NPCManager Instance { get; } = new NPCManager();

    public void Add(SC_NPCCreate packet)
    {
        Object obj = Resources.Load("Antelope");
        GameObject go = Object.Instantiate(obj) as GameObject;

        NPC npc = go.AddComponent<NPC>();
        npc.transform.position = new Vector3(packet.posX, packet.posY, packet.posZ);
        npc.transform.rotation = Quaternion.Euler(packet.lookX, packet.lookY, packet.lookZ);

        _npcs.Add(packet.id, npc);
    }

    public void Move(SC_NPCMove packet)
    {
        NPC npc = null;
        if (_npcs.TryGetValue(packet.id, out npc))
        {
            npc.transform.position = new Vector3(packet.posX, packet.posY, packet.posZ);
            npc.transform.rotation = Quaternion.Euler(packet.lookX, packet.lookY, packet.lookZ);
        }
    }
}
