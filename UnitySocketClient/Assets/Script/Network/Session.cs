﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;

namespace ServerCore
{
    public abstract class PacketSession : Session
    {
        public static readonly int HeaderSize = 2;

        public sealed override int OnRecv(ArraySegment<byte> buffer)
        {
            int processLength = 0;
            int packetCount = 0;

            while (true) {
                // 헤더 파싱 가능여부 확인
                if(buffer.Count < HeaderSize) {
                    break;
                }

                // 패킷 완전체 도착 확인
                ushort dataSize = BitConverter.ToUInt16(buffer.Array, buffer.Offset);
                if(buffer.Count < dataSize) {
                    break;
                }

                OnRecvPacket(new ArraySegment<byte>(buffer.Array, buffer.Offset, dataSize));
                packetCount++;
                
                processLength += dataSize;
                buffer = new ArraySegment<byte>(buffer.Array, buffer.Offset + dataSize, buffer.Count - dataSize);
            }

            if (packetCount > 1) {
                Console.WriteLine($"패킷 모아보내기 : {packetCount}");
            }

            return processLength;
        }

        public abstract void OnRecvPacket(ArraySegment<byte> buffer);
    }

    public abstract class Session
    {
        Socket _socket;
        int _disconnected = 0;

        RecvBuffer _recvBuffer = new RecvBuffer(65535); // 64kb

        object _lock = new object();
        Queue<ArraySegment<byte>> _sendQueue = new Queue<ArraySegment<byte>>();
        List<ArraySegment<byte>> _pendingList = new List<ArraySegment<byte>>();

        SocketAsyncEventArgs _sendArgs = new SocketAsyncEventArgs();
        SocketAsyncEventArgs _recvArgs = new SocketAsyncEventArgs();

        public abstract void OnConnected(EndPoint endPoint);
        public abstract int OnRecv(ArraySegment<byte> buffer);
        public abstract void OnSend(int numOfBytes);
        public abstract void OnDisconnected(EndPoint endPoint);

        private void Clear()
        {
            lock (_lock) {
                _sendQueue.Clear();
                _pendingList.Clear();
            }
        }

        public void Start(Socket socket)
        {
            _socket = socket;
            _recvArgs.Completed += new EventHandler<SocketAsyncEventArgs>(OnRecvCompleted);

            RegisterRecv();

            _sendArgs.Completed += new EventHandler<SocketAsyncEventArgs>(OnSendCompleted);

        }

        public void Send(List<ArraySegment<byte>> sendBuffList)
        {
            if(sendBuffList.Count == 0) {
                return;
            }

            lock (_lock) {
                foreach(ArraySegment<byte> sendBuff in sendBuffList) {
                    _sendQueue.Enqueue(sendBuff);

                    if (_pendingList.Count == 0) {
                        RegisterSend();
                    }
                }
            }
        }

        public void Send(ArraySegment<byte> sendBuff)
        {
            lock (_lock) {
                _sendQueue.Enqueue(sendBuff);
                if (_pendingList.Count == 0) {
                    RegisterSend();
                }
            }
        }

        public void Disconnect()
        {
            if (Interlocked.Exchange(ref _disconnected, 1) == 1) {
                return;
            }

            OnDisconnected(_socket.RemoteEndPoint);
            _socket.Shutdown(SocketShutdown.Both);
            _socket.Close();
            Clear();
        }

        #region Network

        void RegisterSend()
        {
            if(_disconnected == 1) {
                return;
            }

            while (_sendQueue.Count > 0) {
                ArraySegment<byte> buff = _sendQueue.Dequeue();
                _pendingList.Add(buff);
            }

            _sendArgs.BufferList = _pendingList;

            try {
                bool pending = _socket.SendAsync(_sendArgs);
                if (pending == false) {
                    OnSendCompleted(null, _sendArgs);
                }
            } catch (Exception e) {
                Console.WriteLine($"Register Send Failed {e}");
            }
        }

        void OnSendCompleted(object sender, SocketAsyncEventArgs args)
        {
            lock (_lock) {
                if (args.BytesTransferred > 0 && args.SocketError == SocketError.Success) {
                    try {
                        _sendArgs.BufferList = null;
                        _pendingList.Clear();

                        OnSend(_sendArgs.BytesTransferred);

                        if (_sendQueue.Count > 0) {
                            RegisterSend();
                        }
                    }
                    catch (Exception e) {
                        Console.WriteLine($"OnSendCompleted Failed! {e}");
                    }
                } else {
                    Disconnect();
                }
            }
        }

        private void RegisterRecv()
        {
            if(_disconnected == 1) {
                return;
            }

            _recvBuffer.Clean();

            ArraySegment<byte> segment = _recvBuffer.WriteSegment;
            // segment.Count는 FreeSize 의미
            _recvArgs.SetBuffer(segment.Array, segment.Offset, segment.Count);

            try {
                bool pending = _socket.ReceiveAsync(_recvArgs);
                if (pending == false) {
                    OnRecvCompleted(null, _recvArgs);
                }
            } catch (Exception e) {
                // TODO : 나중에 이런 로그는 파일로 남길 수 있게 개선해야함
                Console.WriteLine($"RegisterRecv Failed! {e}");
            }
        }

        private void OnRecvCompleted(object sender, SocketAsyncEventArgs args)
        {
            if (args.BytesTransferred > 0 && args.SocketError == SocketError.Success) {
                try {
                    if (_recvBuffer.OnWrite(args.BytesTransferred) == false) {
                        Disconnect();
                        return;
                    }

                    int processLength = OnRecv(_recvBuffer.ReadSegment);
                    if (processLength < 0 || _recvBuffer.DataSize < processLength) {
                        Disconnect();
                        return;
                    }

                    if (_recvBuffer.OnRead(processLength) == false) {
                        Disconnect();
                        return;
                    }

                    RegisterRecv();
                }
                catch (Exception e) {
                    Console.WriteLine($"OnRecvCompleted Failed! {e}");
                }
            } else {
                Disconnect();
            }
        }
        #endregion
    }
}
