﻿using DummyClient;
using ServerCore;
using System;
using System.Collections.Generic;
using System.Text;
using UnityEngine;

class PacketHandler
{
    public static void SC_BroadcastEnterGameHandler(PacketSession session, IPacket packet)
    {
        SC_BroadcastEnterGame pkt = packet as SC_BroadcastEnterGame;
        ServerSession serverSession = session as ServerSession;

        PlayerManager.Instance.EnterGame(pkt);
    }

    public static void SC_BroadcastLeaveGameHandler(PacketSession session, IPacket packet)
    {
        SC_BroadcastLeaveGame pkt = packet as SC_BroadcastLeaveGame;
        ServerSession serverSession = session as ServerSession;
        PlayerManager.Instance.LeaveGame(pkt);
    }

    public static void SC_LoginPlayerHandler(PacketSession session, IPacket packet)
    {
        SC_LoginPlayer pkt = packet as SC_LoginPlayer;
        ServerSession serverSession = session as ServerSession;

        PlayerManager.Instance.Add(pkt);
    }

    public static void SC_BroadcastMoveHandler(PacketSession session, IPacket packet)
    {
        SC_BroadcastMove pkt = packet as SC_BroadcastMove;
        ServerSession serverSession = session as ServerSession;
        PlayerManager.Instance.Move(pkt);
    }

    public static void SC_ChatHandler(PacketSession session, IPacket packet)
    {
        SC_Chat pkt = packet as SC_Chat;
        PlayerManager.Instance.Chat(pkt);

    }

    public static void SC_LoginHandler(PacketSession session, IPacket packet)
    {
        SC_Login pkt = packet as SC_Login;

        LoginManager _loginManager = GameObject.Find("LoginManager").GetComponent<LoginManager>();
        _loginManager.LoginResult(pkt.suceess, pkt.reason);

    }

    public static void SC_NPCCreateHandler(PacketSession session, IPacket packet)
    {
        SC_NPCCreate pkt = packet as SC_NPCCreate;
        NPCManager.Instance.Add(pkt);
    }

    public static void SC_NPCMoveHandler(PacketSession session, IPacket packet)
    {
        SC_NPCMove pkt = packet as SC_NPCMove;
        NPCManager.Instance.Move(pkt);
    }

    public static void SC_MainSceneLoadHandler(PacketSession session, IPacket packet)
    {
        SC_MainSceneLoad pkt = packet as SC_MainSceneLoad;
        SelectSkin _loginManager = GameObject.Find("SkinManager").GetComponent<SelectSkin>();
        _loginManager.ChangeScene();
    }

    public static void SC_RollHandler(PacketSession session, IPacket packet)
    {
        SC_Roll pkt = packet as SC_Roll;
        PlayerManager.Instance.Roll(pkt.id);
    }

    public static void SC_PlayerAttackHandler(PacketSession session, IPacket packet)
    {
        SC_PlayerAttack pkt = packet as SC_PlayerAttack;
        PlayerManager.Instance.Attack(pkt.id);
    }

    public static void SC_EnemyCreateHandler(PacketSession session, IPacket packet)
    {
        SC_EnemyCreate pkt = packet as SC_EnemyCreate;
        EnemyManager.Instance.Add(pkt);
    }

    public static void SC_EnemyStartChaseHandler(PacketSession session, IPacket packet)
    {
        SC_EnemyStartChase pkt = packet as SC_EnemyStartChase;
        EnemyManager.Instance.StartChase(pkt);
    }

    public static void SC_EnemyChaseHandler(PacketSession session, IPacket packet)
    {
        SC_EnemyChase pkt = packet as SC_EnemyChase;
        EnemyManager.Instance.Chase(pkt);
    }

    public static void SC_EnemyStopChaseHandler(PacketSession session, IPacket packet)
    {
        SC_EnemyStopChase pkt = packet as SC_EnemyStopChase;
        EnemyManager.Instance.StopChase(pkt);
    }

    public static void SC_EnemyAttackHandler(PacketSession session, IPacket packet)
    {
        SC_EnemyAttack pkt = packet as SC_EnemyAttack;
        EnemyManager.Instance.Attack(pkt);
    }

    public static void SC_EnemyHitHandler(PacketSession session, IPacket packet)
    {
        SC_EnemyHit pkt = packet as SC_EnemyHit;
        EnemyManager.Instance.Hit(pkt);
    }

    public static void SC_EnemyDieHandler(PacketSession session, IPacket packet)
    {
        SC_EnemyDie pkt = packet as SC_EnemyDie;
        EnemyManager.Instance.Die(pkt);
    }

    public static void SC_PlayerHitHandler(PacketSession session, IPacket packet)
    {
        SC_PlayerHit pkt = packet as SC_PlayerHit;
        PlayerManager.Instance.Hit(pkt.id, pkt.hp);
    }

    public static void SC_BossCreateHandler(PacketSession session, IPacket packet)
    {
        SC_BossCreate pkt = packet as SC_BossCreate;
        BossManager.Instance.Add(pkt);
    }

    public static void SC_BossAttackHandler(PacketSession session, IPacket packet)
    {
        SC_BossAttack pkt = packet as SC_BossAttack;
        BossManager.Instance.Attack(pkt);
    }
}