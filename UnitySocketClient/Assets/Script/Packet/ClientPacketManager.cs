using ServerCore;
using System;
using System.Collections.Generic;

public class PacketManager
{
    #region Singleton
    static PacketManager _instance = new PacketManager();
    public static PacketManager Instance { get { return _instance; } }
    #endregion

    private PacketManager()
    {
        Register();
    }

    Dictionary<ushort, Func<PacketSession, ArraySegment<byte>, IPacket>> _makeFunc = new Dictionary<ushort, Func<PacketSession, ArraySegment<byte>, IPacket>>();
    Dictionary<ushort, Action<PacketSession, IPacket>> _handler = new Dictionary<ushort, Action<PacketSession, IPacket>>();

    public void Register()
    {
       _makeFunc.Add((ushort)PacketID.SC_BroadcastEnterGame, MakePacket<SC_BroadcastEnterGame>);
       _handler.Add((ushort)PacketID.SC_BroadcastEnterGame, PacketHandler.SC_BroadcastEnterGameHandler);
       _makeFunc.Add((ushort)PacketID.SC_BroadcastLeaveGame, MakePacket<SC_BroadcastLeaveGame>);
       _handler.Add((ushort)PacketID.SC_BroadcastLeaveGame, PacketHandler.SC_BroadcastLeaveGameHandler);
       _makeFunc.Add((ushort)PacketID.SC_LoginPlayer, MakePacket<SC_LoginPlayer>);
       _handler.Add((ushort)PacketID.SC_LoginPlayer, PacketHandler.SC_LoginPlayerHandler);
       _makeFunc.Add((ushort)PacketID.SC_BroadcastMove, MakePacket<SC_BroadcastMove>);
       _handler.Add((ushort)PacketID.SC_BroadcastMove, PacketHandler.SC_BroadcastMoveHandler);
       _makeFunc.Add((ushort)PacketID.SC_Chat, MakePacket<SC_Chat>);
       _handler.Add((ushort)PacketID.SC_Chat, PacketHandler.SC_ChatHandler);
       _makeFunc.Add((ushort)PacketID.SC_Login, MakePacket<SC_Login>);
       _handler.Add((ushort)PacketID.SC_Login, PacketHandler.SC_LoginHandler);
       _makeFunc.Add((ushort)PacketID.SC_NPCCreate, MakePacket<SC_NPCCreate>);
       _handler.Add((ushort)PacketID.SC_NPCCreate, PacketHandler.SC_NPCCreateHandler);
       _makeFunc.Add((ushort)PacketID.SC_NPCMove, MakePacket<SC_NPCMove>);
       _handler.Add((ushort)PacketID.SC_NPCMove, PacketHandler.SC_NPCMoveHandler);
       _makeFunc.Add((ushort)PacketID.SC_MainSceneLoad, MakePacket<SC_MainSceneLoad>);
       _handler.Add((ushort)PacketID.SC_MainSceneLoad, PacketHandler.SC_MainSceneLoadHandler);
       _makeFunc.Add((ushort)PacketID.SC_Roll, MakePacket<SC_Roll>);
       _handler.Add((ushort)PacketID.SC_Roll, PacketHandler.SC_RollHandler);
       _makeFunc.Add((ushort)PacketID.SC_PlayerAttack, MakePacket<SC_PlayerAttack>);
       _handler.Add((ushort)PacketID.SC_PlayerAttack, PacketHandler.SC_PlayerAttackHandler);
       _makeFunc.Add((ushort)PacketID.SC_EnemyCreate, MakePacket<SC_EnemyCreate>);
       _handler.Add((ushort)PacketID.SC_EnemyCreate, PacketHandler.SC_EnemyCreateHandler);
       _makeFunc.Add((ushort)PacketID.SC_EnemyStartChase, MakePacket<SC_EnemyStartChase>);
       _handler.Add((ushort)PacketID.SC_EnemyStartChase, PacketHandler.SC_EnemyStartChaseHandler);
       _makeFunc.Add((ushort)PacketID.SC_EnemyChase, MakePacket<SC_EnemyChase>);
       _handler.Add((ushort)PacketID.SC_EnemyChase, PacketHandler.SC_EnemyChaseHandler);
       _makeFunc.Add((ushort)PacketID.SC_EnemyStopChase, MakePacket<SC_EnemyStopChase>);
       _handler.Add((ushort)PacketID.SC_EnemyStopChase, PacketHandler.SC_EnemyStopChaseHandler);
       _makeFunc.Add((ushort)PacketID.SC_EnemyAttack, MakePacket<SC_EnemyAttack>);
       _handler.Add((ushort)PacketID.SC_EnemyAttack, PacketHandler.SC_EnemyAttackHandler);
       _makeFunc.Add((ushort)PacketID.SC_EnemyHit, MakePacket<SC_EnemyHit>);
       _handler.Add((ushort)PacketID.SC_EnemyHit, PacketHandler.SC_EnemyHitHandler);
       _makeFunc.Add((ushort)PacketID.SC_EnemyDie, MakePacket<SC_EnemyDie>);
       _handler.Add((ushort)PacketID.SC_EnemyDie, PacketHandler.SC_EnemyDieHandler);
       _makeFunc.Add((ushort)PacketID.SC_PlayerHit, MakePacket<SC_PlayerHit>);
       _handler.Add((ushort)PacketID.SC_PlayerHit, PacketHandler.SC_PlayerHitHandler);
       _makeFunc.Add((ushort)PacketID.SC_BossCreate, MakePacket<SC_BossCreate>);
       _handler.Add((ushort)PacketID.SC_BossCreate, PacketHandler.SC_BossCreateHandler);
       _makeFunc.Add((ushort)PacketID.SC_BossAttack, MakePacket<SC_BossAttack>);
       _handler.Add((ushort)PacketID.SC_BossAttack, PacketHandler.SC_BossAttackHandler);

    }

    public void OnRecvPacket(PacketSession session, ArraySegment<byte> buffer, Action<PacketSession, IPacket> onRecvCallback = null)
    {
        ushort count = 0;

        ushort size = BitConverter.ToUInt16(buffer.Array, buffer.Offset);
        count += 2;
        ushort id = BitConverter.ToUInt16(buffer.Array, buffer.Offset + count);
        count += 2;

        Func<PacketSession, ArraySegment<byte>, IPacket> func = null;
        if (_makeFunc.TryGetValue(id, out func)) {
            IPacket packet = func.Invoke(session, buffer);

            if(onRecvCallback != null) {
                onRecvCallback.Invoke(session, packet);
            } else {
                HandlePacket(session, packet);
            }
        }
    }

    private T MakePacket<T>(PacketSession session, ArraySegment<byte> buffer) where T : IPacket, new()
    {
        T packet = new T();
        packet.Read(buffer);
        return packet;
    }

    public void HandlePacket(PacketSession session, IPacket packet) {
        Action<PacketSession, IPacket> action = null;
        if (_handler.TryGetValue(packet.Protocol, out action)) {
            action.Invoke(session, packet);
        }
    }
}