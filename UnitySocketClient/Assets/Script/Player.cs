using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour
{
    public Animator anim;
    public PlayerName nameText;
    public PlayerChat chatText;
    public PlayerChatList[] chatList = new PlayerChatList[5];
    public Skin skin { get; set; }
    public int PlayerId { get; set; }
    public int hp { get; set; }

    void Start()
    {
        anim = GetComponentInChildren<Animator>();
    }
}
