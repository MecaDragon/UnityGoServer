using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public enum Skin
{
    Badger = 0,
    Deer = 1,
    Dog = 2,
    Lion = 3,
    Lizard = 4,
    Owl = 5,
    Rabit = 5,
    Rat = 6,
    Fin = 7,
}

public class SelectSkin : MonoBehaviour
{
    private NetworkManager _networkManager;

    Button Btn_Select;
    Button Btn_Change;
    GameObject Go_Skin;
    Skin skin;
    // Start is called before the first frame update
    void Start()
    {
        skin = 0;
        Go_Skin = GameObject.Find(skin.ToString());
        
        Btn_Select = GameObject.Find("Select").GetComponent<Button>();
        Btn_Select.onClick.AddListener(delegate { Select(); });

        Btn_Change = GameObject.Find("Change").GetComponent<Button>();
        Btn_Change.onClick.AddListener(delegate { Chagne(); });
        
    }

    public void Chagne()
    {
        skin += 1;
        if (skin >= Skin.Fin)
            skin = 0;

        Vector3 tmp = Go_Skin.transform.position;
        Go_Skin.transform.position = Vector3.zero;
        Go_Skin = GameObject.Find(skin.ToString());
        Go_Skin.transform.position = tmp;
    }

    public void Select()
    {
        Btn_Select.gameObject.SetActive(false);
        Btn_Change.gameObject.SetActive(false);

        _networkManager = GameObject.Find("NetworkManager").GetComponent<NetworkManager>();
        CS_Skin loginPacket = new CS_Skin()
        {
            skinId = (int)skin,
        };

        if (_networkManager != null)
        {
            _networkManager.Send(loginPacket.Write());
        }
    }

    public void ChangeScene()
    {
        StartCoroutine(LoadScene("Demo_Environment"));
    }

    IEnumerator LoadScene(string sceneName)
    {
        AsyncOperation op = SceneManager.LoadSceneAsync(sceneName);
        op.allowSceneActivation = false;

        float timer = 0.0f;
        while (!op.isDone)
        {
            yield return null;

            timer += Time.deltaTime;
            if (timer > 3f)
            {
                _networkManager = GameObject.Find("NetworkManager").GetComponent<NetworkManager>();
                CS_MainSceneLoad loadPacket = new CS_MainSceneLoad();

                if (_networkManager != null)
                {
                    _networkManager.Send(loadPacket.Write());
                }
                op.allowSceneActivation = true;
            }
        }
    }

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.D))
        {
            skin += 1;
            if (skin >= Skin.Fin)
                skin = 0;

            Vector3 tmp = Go_Skin.transform.position;
            Go_Skin.transform.position = Vector3.zero;
            Go_Skin = GameObject.Find(skin.ToString());
            Go_Skin.transform.position = tmp;
        }
        else if (Input.GetKeyDown(KeyCode.A))
        {
            skin -= 1;
            if (skin < 0)
                skin = Skin.Fin - 1;

            Vector3 tmp = Go_Skin.transform.position;
            Go_Skin.transform.position = Vector3.zero;
            Go_Skin = GameObject.Find(skin.ToString());
            Go_Skin.transform.position = tmp;
        }
    }
}
