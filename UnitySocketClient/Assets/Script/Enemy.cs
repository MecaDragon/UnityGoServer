using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class Enemy : MonoBehaviour
{
    private NetworkManager _networkManager;

    public int maxHealth;
    public int curHealth;
    public Transform target;
    public BoxCollider meleaArea;
    public bool isChase;
    public bool isAttack;
    public bool isDamaged = false;
    public int id;
    public int hp;

    Rigidbody rigid;
    BoxCollider boxCollider;
    Material mat;
    Animator anim;

    void Awake()
    {
        _networkManager = GameObject.Find("NetworkManager").GetComponent<NetworkManager>();
        //rigid = GetComponent<Rigidbody>();
        boxCollider = GetComponent<BoxCollider>();
        meleaArea = transform.Find("AttackBox").GetComponent<BoxCollider>();
        meleaArea.enabled = false;
        //mat = GetComponentInChildren<MeshRenderer>().material;
        anim = GetComponent<Animator>();

        //Invoke("ChaseStart", 2);
    }

    public void ChaseStart()
    {
        isChase = true;
        anim.SetBool("isWalk", true);
    }

    public void ChaseStop()
    {
        isChase = false;
        anim.SetBool("isWalk", false);
    }

    public void StartAttack()
    {
        if(isAttack == false)
            StartCoroutine(Attack());
    }

    public void Hit(int _hp)
    {
        hp = _hp;
        anim.SetTrigger("doHit");
    }

    public void Die(int _hp)
    {
        hp = _hp;
        gameObject.layer = 14;
        isChase = false;
        anim.ResetTrigger("doAttack");
        anim.ResetTrigger("doHit");
        anim.SetTrigger("doDie");

        Destroy(gameObject, 4);
    }

    void Update()
    {
    }

    void FreezeVelocity()
    {
        if (isChase)
        {
            //rigid.velocity = Vector3.zero;
            //rigid.angularVelocity = Vector3.zero;
        }
    }

    void Targeting()
    {
        float targetRadius = 0.5f;
        float targetRange = 1f;

        RaycastHit[] rayHits =
            Physics.SphereCastAll(transform.position,
            targetRadius,
            transform.forward,
            targetRange,
            LayerMask.GetMask("Player"));

        if(rayHits.Length > 0 && !isAttack)
        {
            StartCoroutine(Attack());
        }
    }

    IEnumerator Attack()
    {
        isChase = false;
        isAttack = true;
        anim.SetTrigger("doAttack");

        meleaArea.enabled = true;
        yield return new WaitForSeconds(0.8f);
        meleaArea.enabled = false;

        //yield return new WaitForSeconds(1f);
        //meleaArea.enabled = false;

        //yield return new WaitForSeconds(1f);
        isChase = true;
        isAttack = false;
        //anim.SetBool("isAttack", false);
    }

    void FixedUpdate()
    {
        //Targeting();
        //FreezeVelocity();
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Weapon" && isDamaged == false)
        {
            //Weapon weapon = other.GetComponentInParent<Weapon>();
            //curHealth -= weapon.damage;

            CS_EnemyHit hitPacket = new CS_EnemyHit()
            {
                pid = other.GetComponentInParent<Player>().PlayerId,
                eid = id,
            };

            if (_networkManager != null)
            {
                _networkManager.Send(hitPacket.Write());
            }

            isDamaged = true;
            StartCoroutine(OnServerDamage());
        }
    }

    IEnumerator OnServerDamage()
    {
        yield return new WaitForSeconds(1.0f);
        isDamaged = false;
    }

    IEnumerator OnDamage(Collider other)
    {
        anim.SetTrigger("doHit");
        // mat.color = Color.red;
        yield return new WaitForSeconds(0.1f);

        if(curHealth > 0)
        {
            //mat.color = Color.white;
        } 
        else
        {
            //mat.color = Color.gray;
            gameObject.layer = 14;
            isChase = false;
            anim.SetTrigger("doDie");

            Destroy(gameObject, 4);
        }
    }
}
