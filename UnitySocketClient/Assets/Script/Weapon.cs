using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Weapon : MonoBehaviour
{
    public int damage;
    public float rate; // ���� �ӵ�
    public MeshCollider meleeArea;
    public TrailRenderer trailEffect;

    void Start()
    {
        damage = 5;
        rate = 1f;
        meleeArea = GetComponentInChildren<MeshCollider>();
        meleeArea.enabled = false;
    }

    public void Use()
    {
        StopCoroutine("Swing");
        StartCoroutine("Swing");
    }

    IEnumerator Swing()
    {
        yield return new WaitForSeconds(0.1f);
        meleeArea.enabled = true;
        //trailEffect.enabled = true;

        yield return new WaitForSeconds(0.3f);
        meleeArea.enabled = false;

        yield return new WaitForSeconds(0.3f);
        //trailEffect.enabled=false;
    }
}
