using System.Collections;
using System.Collections.Generic;
using System.Threading;
using UnityEngine;

public class MyPlayer : Player
{
    private NetworkManager _networkManager;

    public float speed = 5;
    float hAxis;
    float vAxis;
    bool wDown; // 움직임
    bool rDown; // 구르기
    bool fDown; // 공격

    bool isRoll;
    bool isFireReay = true;
    bool isBorder;
    bool isDamage = false;

    bool canMove;

    Vector3 moveVec;
    Vector3 rollVec;

    float fireDelay;

    public Weapon weapon;
    Rigidbody rigid;

    // Start is called before the first frame update
    void Start()
    {
        anim = GetComponentInChildren<Animator>();
        _networkManager = GameObject.Find("NetworkManager").GetComponent<NetworkManager>();
        canMove = true;
        chatText.gameObject.SetActive(true);
        
        rigid = GetComponentInParent<Rigidbody>();
    }

    void FreezeVelocity()
    {
        //rigid.angularVelocity = Vector3.zero;
    }

    void StopToWall()
    {
        isBorder = Physics.Raycast(transform.position, transform.forward, 5, LayerMask.GetMask("Wall"));
    }

    void FixedUpdate()
    {
        FreezeVelocity();
        StopToWall();
    }

    // Update is called once per frame
    void Update()
    {
        GetInput();
        Move();
        Turn();
        Attack();
        Roll();
        SendMove();
    }

    void GetInput()
    {
        if(canMove)
        {
            hAxis = Input.GetAxisRaw("Horizontal");
            vAxis = Input.GetAxisRaw("Vertical");
            wDown = Input.GetButton("Walk");
            rDown = Input.GetKeyDown(KeyCode.Space);
            fDown = Input.GetKeyDown(KeyCode.Mouse0);
        }
        else
        {
            hAxis = 0;
            vAxis = 0;
        }

        if (Input.GetKeyDown(KeyCode.Return))
        {
            if(chatText.gameObject.activeSelf == false)
            {
                canMove = false;
                chatText.gameObject.SetActive(true);
                chatText.OnEnterChat();
            }
            else
            {
                canMove = true;
                chatText.gameObject.SetActive(false);
            }
        }
    }

    void Move()
    {
        moveVec = new Vector3(hAxis, 0, vAxis).normalized;

        if(isRoll)
            moveVec = rollVec;

        if (!isFireReay)
            moveVec = Vector3.zero;

        transform.position += moveVec * speed * (wDown ? 0.3f : 1f) * Time.deltaTime;

        anim.SetBool("isRun", moveVec != Vector3.zero);
        anim.SetBool("isWalk", wDown);
    }

    void Turn()
    {
        transform.LookAt(transform.position + moveVec);
    }

    void Attack()
    {
        if (weapon == null)
            return;

        fireDelay += Time.deltaTime;
        isFireReay = weapon.rate < fireDelay;

        if(canMove && fDown && isFireReay && !isRoll)
        {
            weapon.Use();
            anim.SetTrigger("doSwing");
            fireDelay = 0;

            CS_PlayerAttack attackPacket = new CS_PlayerAttack()
            {
            };

            if (_networkManager != null)
            {
                _networkManager.Send(attackPacket.Write());
            }
        }
    }

    void Roll()
    {
        if (canMove && rDown && moveVec != Vector3.zero && !isRoll)
        {
            rollVec = moveVec;
            speed *= 2;
            anim.SetTrigger("doRoll");
            isRoll = true;

            CS_Roll rollPacket = new CS_Roll()
            {
            };

            if (_networkManager != null)
            {
                _networkManager.Send(rollPacket.Write());
            }

            Invoke("RollOut", 0.5f);
        }
            
    }

    void RollOut()
    {
        speed *= 0.5f;
        isRoll = false;
    }

    void OnTriggerStay(Collider other)
    {
        if(other.tag == "EnemyBullet")
        {
            if (isDamage == false)
            {
                StartCoroutine(OnDamage(other));
            }
        }
    }

    IEnumerator OnDamage(Collider other)
    {
        CS_PlayerHit hitPacket = new CS_PlayerHit()
        {
            eid = other.GetComponentInParent<Enemy>().id,
        };

        if (_networkManager != null)
        {
            _networkManager.Send(hitPacket.Write());
        }

        isDamage = true;
        anim.SetTrigger("doHit");

        yield return new WaitForSeconds(1f);

        isDamage = false;
    }

    void SendMove()
    {
        CS_Move movePacket = new CS_Move()
        {
            isRun = anim.GetBool("isRun"),
            isWalk = anim.GetBool("isWalk"),
            posX = transform.position.x,
            posY = transform.position.y,
            posZ = transform.position.z,

            lookX = transform.eulerAngles.x,
            lookY = transform.eulerAngles.y,
            lookZ = transform.eulerAngles.z,
        };

        if (_networkManager != null)
        {
            _networkManager.Send(movePacket.Write());
        }
    }
}
