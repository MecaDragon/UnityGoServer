package main

import (
	"bytes"
	"encoding/binary"
    "reflect"
)

func sizeof(t reflect.Type) int16 {
	switch t.Kind() {
	case reflect.Array:
		if s := sizeof(t.Elem()); s >= 0 {
			return int16(s) * int16(t.Len())

        }

	case reflect.Struct:
		var sum int16
        sum = 0
		for i, n := 0, t.NumField(); i<n; i++ {
			s := sizeof(t.Field(i).Type)
			if s< 0 {
				return -1
			}
            sum += s
		}
		return sum
	case reflect.Bool, reflect.Uint8, reflect.Uint16, reflect.Uint32, reflect.Uint64,
		reflect.Int8, reflect.Int16, reflect.Int32, reflect.Int64,
		reflect.Float32, reflect.Float64, reflect.Complex64, reflect.Complex128:
		return int16(t.Size())
	case reflect.Slice:
		return 0
	case reflect.String:
		return 0
	}
	return -1
}

const (
    CS_LeaveGame = 1
	CS_Move = 2
	SC_BroadcastEnterGame = 3
	SC_BroadcastLeaveGame = 4
	SC_LoginPlayer = 5
	SC_BroadcastMove = 6
	CS_Chat = 7
	SC_Chat = 8
	CS_Login = 9
	SC_Login = 10
	CS_MainSceneLoad = 11
	SC_NPCCreate = 12
	SC_NPCMove = 13
	CS_Skin = 14
	SC_MainSceneLoad = 15
	CS_Roll = 16
	SC_Roll = 17
	CS_PlayerAttack = 18
	SC_PlayerAttack = 19
	SC_EnemyCreate = 20
	SC_EnemyStartChase = 21
	SC_EnemyChase = 22
	SC_EnemyStopChase = 23
	SC_EnemyAttack = 24
	CS_EnemyHit = 25
	SC_EnemyHit = 26
	SC_EnemyDie = 27
	CS_PlayerHit = 28
	SC_PlayerHit = 29
	SC_BossCreate = 30
	SC_BossAttack = 31
	
)


type CS_LeaveGamePacket struct {
	Size int16
	Type int16
    
}

func (packet *CS_LeaveGamePacket) getType() int16 {
	return CS_LeaveGame
}

func (packet *CS_LeaveGamePacket) write() []byte {
    buf := new(bytes.Buffer)
    packetSize := sizeof(reflect.TypeOf(*packet))
    

	binary.Write(buf, binary.LittleEndian, packetSize)
	binary.Write(buf, binary.LittleEndian, packet.getType())
        

    return buf.Bytes()
}

func (packet *CS_LeaveGamePacket) read(data []byte) {
    readbuf := bytes.NewBuffer(data)
	binary.Read(readbuf, binary.LittleEndian, &packet.Size)
	binary.Read(readbuf, binary.LittleEndian, &packet.Type)
    
}

type CS_MovePacket struct {
	Size int16
	Type int16
    isRun bool;
	isWalk bool;
	isRoll bool;
	posX float32;
	posY float32;
	posZ float32;
	lookX float32;
	lookY float32;
	lookZ float32;
}

func (packet *CS_MovePacket) getType() int16 {
	return CS_Move
}

func (packet *CS_MovePacket) write() []byte {
    buf := new(bytes.Buffer)
    packetSize := sizeof(reflect.TypeOf(*packet))
    

	binary.Write(buf, binary.LittleEndian, packetSize)
	binary.Write(buf, binary.LittleEndian, packet.getType())
    binary.Write(buf, binary.LittleEndian, packet.isRun)
	binary.Write(buf, binary.LittleEndian, packet.isWalk)
	binary.Write(buf, binary.LittleEndian, packet.isRoll)
	binary.Write(buf, binary.LittleEndian, packet.posX)
	binary.Write(buf, binary.LittleEndian, packet.posY)
	binary.Write(buf, binary.LittleEndian, packet.posZ)
	binary.Write(buf, binary.LittleEndian, packet.lookX)
	binary.Write(buf, binary.LittleEndian, packet.lookY)
	binary.Write(buf, binary.LittleEndian, packet.lookZ)    

    return buf.Bytes()
}

func (packet *CS_MovePacket) read(data []byte) {
    readbuf := bytes.NewBuffer(data)
	binary.Read(readbuf, binary.LittleEndian, &packet.Size)
	binary.Read(readbuf, binary.LittleEndian, &packet.Type)
    binary.Read(readbuf, binary.LittleEndian, &packet.isRun)
	binary.Read(readbuf, binary.LittleEndian, &packet.isWalk)
	binary.Read(readbuf, binary.LittleEndian, &packet.isRoll)
	binary.Read(readbuf, binary.LittleEndian, &packet.posX)
	binary.Read(readbuf, binary.LittleEndian, &packet.posY)
	binary.Read(readbuf, binary.LittleEndian, &packet.posZ)
	binary.Read(readbuf, binary.LittleEndian, &packet.lookX)
	binary.Read(readbuf, binary.LittleEndian, &packet.lookY)
	binary.Read(readbuf, binary.LittleEndian, &packet.lookZ)
}

type SC_BroadcastEnterGamePacket struct {
	Size int16
	Type int16
    playerId int32;
	posX float32;
	posY float32;
	posZ float32;
	skin int32;
	name string;
}

func (packet *SC_BroadcastEnterGamePacket) getType() int16 {
	return SC_BroadcastEnterGame
}

func (packet *SC_BroadcastEnterGamePacket) write() []byte {
    buf := new(bytes.Buffer)
    packetSize := sizeof(reflect.TypeOf(*packet))
    packetSize += 2
	packetSize += int16(len(packet.name))
	

	binary.Write(buf, binary.LittleEndian, packetSize)
	binary.Write(buf, binary.LittleEndian, packet.getType())
    binary.Write(buf, binary.LittleEndian, packet.playerId)
	binary.Write(buf, binary.LittleEndian, packet.posX)
	binary.Write(buf, binary.LittleEndian, packet.posY)
	binary.Write(buf, binary.LittleEndian, packet.posZ)
	binary.Write(buf, binary.LittleEndian, packet.skin)
	nameSize := int16(len(packet.name))
	binary.Write(buf, binary.LittleEndian, nameSize)
	binary.Write(buf, binary.LittleEndian, []byte(packet.name))
	    

    return buf.Bytes()
}

func (packet *SC_BroadcastEnterGamePacket) read(data []byte) {
    readbuf := bytes.NewBuffer(data)
	binary.Read(readbuf, binary.LittleEndian, &packet.Size)
	binary.Read(readbuf, binary.LittleEndian, &packet.Type)
    binary.Read(readbuf, binary.LittleEndian, &packet.playerId)
	binary.Read(readbuf, binary.LittleEndian, &packet.posX)
	binary.Read(readbuf, binary.LittleEndian, &packet.posY)
	binary.Read(readbuf, binary.LittleEndian, &packet.posZ)
	binary.Read(readbuf, binary.LittleEndian, &packet.skin)
	var nameSize int16
	binary.Read(readbuf, binary.LittleEndian, &nameSize)
	nameBuf := make([]byte, nameSize)
	binary.Read(readbuf, binary.LittleEndian, &nameBuf)
	packet.name = string(nameBuf)
	
}

type SC_BroadcastLeaveGamePacket struct {
	Size int16
	Type int16
    playerId int32;
}

func (packet *SC_BroadcastLeaveGamePacket) getType() int16 {
	return SC_BroadcastLeaveGame
}

func (packet *SC_BroadcastLeaveGamePacket) write() []byte {
    buf := new(bytes.Buffer)
    packetSize := sizeof(reflect.TypeOf(*packet))
    

	binary.Write(buf, binary.LittleEndian, packetSize)
	binary.Write(buf, binary.LittleEndian, packet.getType())
    binary.Write(buf, binary.LittleEndian, packet.playerId)    

    return buf.Bytes()
}

func (packet *SC_BroadcastLeaveGamePacket) read(data []byte) {
    readbuf := bytes.NewBuffer(data)
	binary.Read(readbuf, binary.LittleEndian, &packet.Size)
	binary.Read(readbuf, binary.LittleEndian, &packet.Type)
    binary.Read(readbuf, binary.LittleEndian, &packet.playerId)
}

type SC_LoginPlayerPacket struct {
	Size int16
	Type int16
    isSelf bool;
	playerId int32;
	isRun bool;
	isWalk bool;
	isRoll bool;
	posX float32;
	posY float32;
	posZ float32;
	lookX float32;
	lookY float32;
	lookZ float32;
	skin int32;
	name string;
}

func (packet *SC_LoginPlayerPacket) getType() int16 {
	return SC_LoginPlayer
}

func (packet *SC_LoginPlayerPacket) write() []byte {
    buf := new(bytes.Buffer)
    packetSize := sizeof(reflect.TypeOf(*packet))
    packetSize += 2
	packetSize += int16(len(packet.name))
	

	binary.Write(buf, binary.LittleEndian, packetSize)
	binary.Write(buf, binary.LittleEndian, packet.getType())
    binary.Write(buf, binary.LittleEndian, packet.isSelf)
	binary.Write(buf, binary.LittleEndian, packet.playerId)
	binary.Write(buf, binary.LittleEndian, packet.isRun)
	binary.Write(buf, binary.LittleEndian, packet.isWalk)
	binary.Write(buf, binary.LittleEndian, packet.isRoll)
	binary.Write(buf, binary.LittleEndian, packet.posX)
	binary.Write(buf, binary.LittleEndian, packet.posY)
	binary.Write(buf, binary.LittleEndian, packet.posZ)
	binary.Write(buf, binary.LittleEndian, packet.lookX)
	binary.Write(buf, binary.LittleEndian, packet.lookY)
	binary.Write(buf, binary.LittleEndian, packet.lookZ)
	binary.Write(buf, binary.LittleEndian, packet.skin)
	nameSize := int16(len(packet.name))
	binary.Write(buf, binary.LittleEndian, nameSize)
	binary.Write(buf, binary.LittleEndian, []byte(packet.name))
	    

    return buf.Bytes()
}

func (packet *SC_LoginPlayerPacket) read(data []byte) {
    readbuf := bytes.NewBuffer(data)
	binary.Read(readbuf, binary.LittleEndian, &packet.Size)
	binary.Read(readbuf, binary.LittleEndian, &packet.Type)
    binary.Read(readbuf, binary.LittleEndian, &packet.isSelf)
	binary.Read(readbuf, binary.LittleEndian, &packet.playerId)
	binary.Read(readbuf, binary.LittleEndian, &packet.isRun)
	binary.Read(readbuf, binary.LittleEndian, &packet.isWalk)
	binary.Read(readbuf, binary.LittleEndian, &packet.isRoll)
	binary.Read(readbuf, binary.LittleEndian, &packet.posX)
	binary.Read(readbuf, binary.LittleEndian, &packet.posY)
	binary.Read(readbuf, binary.LittleEndian, &packet.posZ)
	binary.Read(readbuf, binary.LittleEndian, &packet.lookX)
	binary.Read(readbuf, binary.LittleEndian, &packet.lookY)
	binary.Read(readbuf, binary.LittleEndian, &packet.lookZ)
	binary.Read(readbuf, binary.LittleEndian, &packet.skin)
	var nameSize int16
	binary.Read(readbuf, binary.LittleEndian, &nameSize)
	nameBuf := make([]byte, nameSize)
	binary.Read(readbuf, binary.LittleEndian, &nameBuf)
	packet.name = string(nameBuf)
	
}

type SC_BroadcastMovePacket struct {
	Size int16
	Type int16
    playerId int32;
	isRun bool;
	isWalk bool;
	isRoll bool;
	posX float32;
	posY float32;
	posZ float32;
	lookX float32;
	lookY float32;
	lookZ float32;
}

func (packet *SC_BroadcastMovePacket) getType() int16 {
	return SC_BroadcastMove
}

func (packet *SC_BroadcastMovePacket) write() []byte {
    buf := new(bytes.Buffer)
    packetSize := sizeof(reflect.TypeOf(*packet))
    

	binary.Write(buf, binary.LittleEndian, packetSize)
	binary.Write(buf, binary.LittleEndian, packet.getType())
    binary.Write(buf, binary.LittleEndian, packet.playerId)
	binary.Write(buf, binary.LittleEndian, packet.isRun)
	binary.Write(buf, binary.LittleEndian, packet.isWalk)
	binary.Write(buf, binary.LittleEndian, packet.isRoll)
	binary.Write(buf, binary.LittleEndian, packet.posX)
	binary.Write(buf, binary.LittleEndian, packet.posY)
	binary.Write(buf, binary.LittleEndian, packet.posZ)
	binary.Write(buf, binary.LittleEndian, packet.lookX)
	binary.Write(buf, binary.LittleEndian, packet.lookY)
	binary.Write(buf, binary.LittleEndian, packet.lookZ)    

    return buf.Bytes()
}

func (packet *SC_BroadcastMovePacket) read(data []byte) {
    readbuf := bytes.NewBuffer(data)
	binary.Read(readbuf, binary.LittleEndian, &packet.Size)
	binary.Read(readbuf, binary.LittleEndian, &packet.Type)
    binary.Read(readbuf, binary.LittleEndian, &packet.playerId)
	binary.Read(readbuf, binary.LittleEndian, &packet.isRun)
	binary.Read(readbuf, binary.LittleEndian, &packet.isWalk)
	binary.Read(readbuf, binary.LittleEndian, &packet.isRoll)
	binary.Read(readbuf, binary.LittleEndian, &packet.posX)
	binary.Read(readbuf, binary.LittleEndian, &packet.posY)
	binary.Read(readbuf, binary.LittleEndian, &packet.posZ)
	binary.Read(readbuf, binary.LittleEndian, &packet.lookX)
	binary.Read(readbuf, binary.LittleEndian, &packet.lookY)
	binary.Read(readbuf, binary.LittleEndian, &packet.lookZ)
}

type CS_ChatPacket struct {
	Size int16
	Type int16
    chat string;
}

func (packet *CS_ChatPacket) getType() int16 {
	return CS_Chat
}

func (packet *CS_ChatPacket) write() []byte {
    buf := new(bytes.Buffer)
    packetSize := sizeof(reflect.TypeOf(*packet))
    packetSize += 2
	packetSize += int16(len(packet.chat))
	

	binary.Write(buf, binary.LittleEndian, packetSize)
	binary.Write(buf, binary.LittleEndian, packet.getType())
    chatSize := int16(len(packet.chat))
	binary.Write(buf, binary.LittleEndian, chatSize)
	binary.Write(buf, binary.LittleEndian, []byte(packet.chat))
	    

    return buf.Bytes()
}

func (packet *CS_ChatPacket) read(data []byte) {
    readbuf := bytes.NewBuffer(data)
	binary.Read(readbuf, binary.LittleEndian, &packet.Size)
	binary.Read(readbuf, binary.LittleEndian, &packet.Type)
    var chatSize int16
	binary.Read(readbuf, binary.LittleEndian, &chatSize)
	chatBuf := make([]byte, chatSize)
	binary.Read(readbuf, binary.LittleEndian, &chatBuf)
	packet.chat = string(chatBuf)
	
}

type SC_ChatPacket struct {
	Size int16
	Type int16
    playerId int32;
	chat string;
}

func (packet *SC_ChatPacket) getType() int16 {
	return SC_Chat
}

func (packet *SC_ChatPacket) write() []byte {
    buf := new(bytes.Buffer)
    packetSize := sizeof(reflect.TypeOf(*packet))
    packetSize += 2
	packetSize += int16(len(packet.chat))
	

	binary.Write(buf, binary.LittleEndian, packetSize)
	binary.Write(buf, binary.LittleEndian, packet.getType())
    binary.Write(buf, binary.LittleEndian, packet.playerId)
	chatSize := int16(len(packet.chat))
	binary.Write(buf, binary.LittleEndian, chatSize)
	binary.Write(buf, binary.LittleEndian, []byte(packet.chat))
	    

    return buf.Bytes()
}

func (packet *SC_ChatPacket) read(data []byte) {
    readbuf := bytes.NewBuffer(data)
	binary.Read(readbuf, binary.LittleEndian, &packet.Size)
	binary.Read(readbuf, binary.LittleEndian, &packet.Type)
    binary.Read(readbuf, binary.LittleEndian, &packet.playerId)
	var chatSize int16
	binary.Read(readbuf, binary.LittleEndian, &chatSize)
	chatBuf := make([]byte, chatSize)
	binary.Read(readbuf, binary.LittleEndian, &chatBuf)
	packet.chat = string(chatBuf)
	
}

type CS_LoginPacket struct {
	Size int16
	Type int16
    name string;
}

func (packet *CS_LoginPacket) getType() int16 {
	return CS_Login
}

func (packet *CS_LoginPacket) write() []byte {
    buf := new(bytes.Buffer)
    packetSize := sizeof(reflect.TypeOf(*packet))
    packetSize += 2
	packetSize += int16(len(packet.name))
	

	binary.Write(buf, binary.LittleEndian, packetSize)
	binary.Write(buf, binary.LittleEndian, packet.getType())
    nameSize := int16(len(packet.name))
	binary.Write(buf, binary.LittleEndian, nameSize)
	binary.Write(buf, binary.LittleEndian, []byte(packet.name))
	    

    return buf.Bytes()
}

func (packet *CS_LoginPacket) read(data []byte) {
    readbuf := bytes.NewBuffer(data)
	binary.Read(readbuf, binary.LittleEndian, &packet.Size)
	binary.Read(readbuf, binary.LittleEndian, &packet.Type)
    var nameSize int16
	binary.Read(readbuf, binary.LittleEndian, &nameSize)
	nameBuf := make([]byte, nameSize)
	binary.Read(readbuf, binary.LittleEndian, &nameBuf)
	packet.name = string(nameBuf)
	
}

type SC_LoginPacket struct {
	Size int16
	Type int16
    suceess bool;
	reason string;
}

func (packet *SC_LoginPacket) getType() int16 {
	return SC_Login
}

func (packet *SC_LoginPacket) write() []byte {
    buf := new(bytes.Buffer)
    packetSize := sizeof(reflect.TypeOf(*packet))
    packetSize += 2
	packetSize += int16(len(packet.reason))
	

	binary.Write(buf, binary.LittleEndian, packetSize)
	binary.Write(buf, binary.LittleEndian, packet.getType())
    binary.Write(buf, binary.LittleEndian, packet.suceess)
	reasonSize := int16(len(packet.reason))
	binary.Write(buf, binary.LittleEndian, reasonSize)
	binary.Write(buf, binary.LittleEndian, []byte(packet.reason))
	    

    return buf.Bytes()
}

func (packet *SC_LoginPacket) read(data []byte) {
    readbuf := bytes.NewBuffer(data)
	binary.Read(readbuf, binary.LittleEndian, &packet.Size)
	binary.Read(readbuf, binary.LittleEndian, &packet.Type)
    binary.Read(readbuf, binary.LittleEndian, &packet.suceess)
	var reasonSize int16
	binary.Read(readbuf, binary.LittleEndian, &reasonSize)
	reasonBuf := make([]byte, reasonSize)
	binary.Read(readbuf, binary.LittleEndian, &reasonBuf)
	packet.reason = string(reasonBuf)
	
}

type CS_MainSceneLoadPacket struct {
	Size int16
	Type int16
    
}

func (packet *CS_MainSceneLoadPacket) getType() int16 {
	return CS_MainSceneLoad
}

func (packet *CS_MainSceneLoadPacket) write() []byte {
    buf := new(bytes.Buffer)
    packetSize := sizeof(reflect.TypeOf(*packet))
    

	binary.Write(buf, binary.LittleEndian, packetSize)
	binary.Write(buf, binary.LittleEndian, packet.getType())
        

    return buf.Bytes()
}

func (packet *CS_MainSceneLoadPacket) read(data []byte) {
    readbuf := bytes.NewBuffer(data)
	binary.Read(readbuf, binary.LittleEndian, &packet.Size)
	binary.Read(readbuf, binary.LittleEndian, &packet.Type)
    
}

type SC_NPCCreatePacket struct {
	Size int16
	Type int16
    id int32;
	posX float32;
	posY float32;
	posZ float32;
	lookX float32;
	lookY float32;
	lookZ float32;
}

func (packet *SC_NPCCreatePacket) getType() int16 {
	return SC_NPCCreate
}

func (packet *SC_NPCCreatePacket) write() []byte {
    buf := new(bytes.Buffer)
    packetSize := sizeof(reflect.TypeOf(*packet))
    

	binary.Write(buf, binary.LittleEndian, packetSize)
	binary.Write(buf, binary.LittleEndian, packet.getType())
    binary.Write(buf, binary.LittleEndian, packet.id)
	binary.Write(buf, binary.LittleEndian, packet.posX)
	binary.Write(buf, binary.LittleEndian, packet.posY)
	binary.Write(buf, binary.LittleEndian, packet.posZ)
	binary.Write(buf, binary.LittleEndian, packet.lookX)
	binary.Write(buf, binary.LittleEndian, packet.lookY)
	binary.Write(buf, binary.LittleEndian, packet.lookZ)    

    return buf.Bytes()
}

func (packet *SC_NPCCreatePacket) read(data []byte) {
    readbuf := bytes.NewBuffer(data)
	binary.Read(readbuf, binary.LittleEndian, &packet.Size)
	binary.Read(readbuf, binary.LittleEndian, &packet.Type)
    binary.Read(readbuf, binary.LittleEndian, &packet.id)
	binary.Read(readbuf, binary.LittleEndian, &packet.posX)
	binary.Read(readbuf, binary.LittleEndian, &packet.posY)
	binary.Read(readbuf, binary.LittleEndian, &packet.posZ)
	binary.Read(readbuf, binary.LittleEndian, &packet.lookX)
	binary.Read(readbuf, binary.LittleEndian, &packet.lookY)
	binary.Read(readbuf, binary.LittleEndian, &packet.lookZ)
}

type SC_NPCMovePacket struct {
	Size int16
	Type int16
    id int32;
	posX float32;
	posY float32;
	posZ float32;
	lookX float32;
	lookY float32;
	lookZ float32;
}

func (packet *SC_NPCMovePacket) getType() int16 {
	return SC_NPCMove
}

func (packet *SC_NPCMovePacket) write() []byte {
    buf := new(bytes.Buffer)
    packetSize := sizeof(reflect.TypeOf(*packet))
    

	binary.Write(buf, binary.LittleEndian, packetSize)
	binary.Write(buf, binary.LittleEndian, packet.getType())
    binary.Write(buf, binary.LittleEndian, packet.id)
	binary.Write(buf, binary.LittleEndian, packet.posX)
	binary.Write(buf, binary.LittleEndian, packet.posY)
	binary.Write(buf, binary.LittleEndian, packet.posZ)
	binary.Write(buf, binary.LittleEndian, packet.lookX)
	binary.Write(buf, binary.LittleEndian, packet.lookY)
	binary.Write(buf, binary.LittleEndian, packet.lookZ)    

    return buf.Bytes()
}

func (packet *SC_NPCMovePacket) read(data []byte) {
    readbuf := bytes.NewBuffer(data)
	binary.Read(readbuf, binary.LittleEndian, &packet.Size)
	binary.Read(readbuf, binary.LittleEndian, &packet.Type)
    binary.Read(readbuf, binary.LittleEndian, &packet.id)
	binary.Read(readbuf, binary.LittleEndian, &packet.posX)
	binary.Read(readbuf, binary.LittleEndian, &packet.posY)
	binary.Read(readbuf, binary.LittleEndian, &packet.posZ)
	binary.Read(readbuf, binary.LittleEndian, &packet.lookX)
	binary.Read(readbuf, binary.LittleEndian, &packet.lookY)
	binary.Read(readbuf, binary.LittleEndian, &packet.lookZ)
}

type CS_SkinPacket struct {
	Size int16
	Type int16
    skinId int32;
}

func (packet *CS_SkinPacket) getType() int16 {
	return CS_Skin
}

func (packet *CS_SkinPacket) write() []byte {
    buf := new(bytes.Buffer)
    packetSize := sizeof(reflect.TypeOf(*packet))
    

	binary.Write(buf, binary.LittleEndian, packetSize)
	binary.Write(buf, binary.LittleEndian, packet.getType())
    binary.Write(buf, binary.LittleEndian, packet.skinId)    

    return buf.Bytes()
}

func (packet *CS_SkinPacket) read(data []byte) {
    readbuf := bytes.NewBuffer(data)
	binary.Read(readbuf, binary.LittleEndian, &packet.Size)
	binary.Read(readbuf, binary.LittleEndian, &packet.Type)
    binary.Read(readbuf, binary.LittleEndian, &packet.skinId)
}

type SC_MainSceneLoadPacket struct {
	Size int16
	Type int16
    
}

func (packet *SC_MainSceneLoadPacket) getType() int16 {
	return SC_MainSceneLoad
}

func (packet *SC_MainSceneLoadPacket) write() []byte {
    buf := new(bytes.Buffer)
    packetSize := sizeof(reflect.TypeOf(*packet))
    

	binary.Write(buf, binary.LittleEndian, packetSize)
	binary.Write(buf, binary.LittleEndian, packet.getType())
        

    return buf.Bytes()
}

func (packet *SC_MainSceneLoadPacket) read(data []byte) {
    readbuf := bytes.NewBuffer(data)
	binary.Read(readbuf, binary.LittleEndian, &packet.Size)
	binary.Read(readbuf, binary.LittleEndian, &packet.Type)
    
}

type CS_RollPacket struct {
	Size int16
	Type int16
    
}

func (packet *CS_RollPacket) getType() int16 {
	return CS_Roll
}

func (packet *CS_RollPacket) write() []byte {
    buf := new(bytes.Buffer)
    packetSize := sizeof(reflect.TypeOf(*packet))
    

	binary.Write(buf, binary.LittleEndian, packetSize)
	binary.Write(buf, binary.LittleEndian, packet.getType())
        

    return buf.Bytes()
}

func (packet *CS_RollPacket) read(data []byte) {
    readbuf := bytes.NewBuffer(data)
	binary.Read(readbuf, binary.LittleEndian, &packet.Size)
	binary.Read(readbuf, binary.LittleEndian, &packet.Type)
    
}

type SC_RollPacket struct {
	Size int16
	Type int16
    id int32;
}

func (packet *SC_RollPacket) getType() int16 {
	return SC_Roll
}

func (packet *SC_RollPacket) write() []byte {
    buf := new(bytes.Buffer)
    packetSize := sizeof(reflect.TypeOf(*packet))
    

	binary.Write(buf, binary.LittleEndian, packetSize)
	binary.Write(buf, binary.LittleEndian, packet.getType())
    binary.Write(buf, binary.LittleEndian, packet.id)    

    return buf.Bytes()
}

func (packet *SC_RollPacket) read(data []byte) {
    readbuf := bytes.NewBuffer(data)
	binary.Read(readbuf, binary.LittleEndian, &packet.Size)
	binary.Read(readbuf, binary.LittleEndian, &packet.Type)
    binary.Read(readbuf, binary.LittleEndian, &packet.id)
}

type CS_PlayerAttackPacket struct {
	Size int16
	Type int16
    
}

func (packet *CS_PlayerAttackPacket) getType() int16 {
	return CS_PlayerAttack
}

func (packet *CS_PlayerAttackPacket) write() []byte {
    buf := new(bytes.Buffer)
    packetSize := sizeof(reflect.TypeOf(*packet))
    

	binary.Write(buf, binary.LittleEndian, packetSize)
	binary.Write(buf, binary.LittleEndian, packet.getType())
        

    return buf.Bytes()
}

func (packet *CS_PlayerAttackPacket) read(data []byte) {
    readbuf := bytes.NewBuffer(data)
	binary.Read(readbuf, binary.LittleEndian, &packet.Size)
	binary.Read(readbuf, binary.LittleEndian, &packet.Type)
    
}

type SC_PlayerAttackPacket struct {
	Size int16
	Type int16
    id int32;
}

func (packet *SC_PlayerAttackPacket) getType() int16 {
	return SC_PlayerAttack
}

func (packet *SC_PlayerAttackPacket) write() []byte {
    buf := new(bytes.Buffer)
    packetSize := sizeof(reflect.TypeOf(*packet))
    

	binary.Write(buf, binary.LittleEndian, packetSize)
	binary.Write(buf, binary.LittleEndian, packet.getType())
    binary.Write(buf, binary.LittleEndian, packet.id)    

    return buf.Bytes()
}

func (packet *SC_PlayerAttackPacket) read(data []byte) {
    readbuf := bytes.NewBuffer(data)
	binary.Read(readbuf, binary.LittleEndian, &packet.Size)
	binary.Read(readbuf, binary.LittleEndian, &packet.Type)
    binary.Read(readbuf, binary.LittleEndian, &packet.id)
}

type SC_EnemyCreatePacket struct {
	Size int16
	Type int16
    id int32;
	etype int32;
	hp int32;
	rotate float32;
	posX float32;
	posY float32;
	posZ float32;
}

func (packet *SC_EnemyCreatePacket) getType() int16 {
	return SC_EnemyCreate
}

func (packet *SC_EnemyCreatePacket) write() []byte {
    buf := new(bytes.Buffer)
    packetSize := sizeof(reflect.TypeOf(*packet))
    

	binary.Write(buf, binary.LittleEndian, packetSize)
	binary.Write(buf, binary.LittleEndian, packet.getType())
    binary.Write(buf, binary.LittleEndian, packet.id)
	binary.Write(buf, binary.LittleEndian, packet.etype)
	binary.Write(buf, binary.LittleEndian, packet.hp)
	binary.Write(buf, binary.LittleEndian, packet.rotate)
	binary.Write(buf, binary.LittleEndian, packet.posX)
	binary.Write(buf, binary.LittleEndian, packet.posY)
	binary.Write(buf, binary.LittleEndian, packet.posZ)    

    return buf.Bytes()
}

func (packet *SC_EnemyCreatePacket) read(data []byte) {
    readbuf := bytes.NewBuffer(data)
	binary.Read(readbuf, binary.LittleEndian, &packet.Size)
	binary.Read(readbuf, binary.LittleEndian, &packet.Type)
    binary.Read(readbuf, binary.LittleEndian, &packet.id)
	binary.Read(readbuf, binary.LittleEndian, &packet.etype)
	binary.Read(readbuf, binary.LittleEndian, &packet.hp)
	binary.Read(readbuf, binary.LittleEndian, &packet.rotate)
	binary.Read(readbuf, binary.LittleEndian, &packet.posX)
	binary.Read(readbuf, binary.LittleEndian, &packet.posY)
	binary.Read(readbuf, binary.LittleEndian, &packet.posZ)
}

type SC_EnemyStartChasePacket struct {
	Size int16
	Type int16
    id int32;
	posX float32;
	posY float32;
	posZ float32;
	target int32;
}

func (packet *SC_EnemyStartChasePacket) getType() int16 {
	return SC_EnemyStartChase
}

func (packet *SC_EnemyStartChasePacket) write() []byte {
    buf := new(bytes.Buffer)
    packetSize := sizeof(reflect.TypeOf(*packet))
    

	binary.Write(buf, binary.LittleEndian, packetSize)
	binary.Write(buf, binary.LittleEndian, packet.getType())
    binary.Write(buf, binary.LittleEndian, packet.id)
	binary.Write(buf, binary.LittleEndian, packet.posX)
	binary.Write(buf, binary.LittleEndian, packet.posY)
	binary.Write(buf, binary.LittleEndian, packet.posZ)
	binary.Write(buf, binary.LittleEndian, packet.target)    

    return buf.Bytes()
}

func (packet *SC_EnemyStartChasePacket) read(data []byte) {
    readbuf := bytes.NewBuffer(data)
	binary.Read(readbuf, binary.LittleEndian, &packet.Size)
	binary.Read(readbuf, binary.LittleEndian, &packet.Type)
    binary.Read(readbuf, binary.LittleEndian, &packet.id)
	binary.Read(readbuf, binary.LittleEndian, &packet.posX)
	binary.Read(readbuf, binary.LittleEndian, &packet.posY)
	binary.Read(readbuf, binary.LittleEndian, &packet.posZ)
	binary.Read(readbuf, binary.LittleEndian, &packet.target)
}

type SC_EnemyChasePacket struct {
	Size int16
	Type int16
    id int32;
	posX float32;
	posY float32;
	posZ float32;
	targetPosX float32;
	targetposY float32;
	targetposZ float32;
}

func (packet *SC_EnemyChasePacket) getType() int16 {
	return SC_EnemyChase
}

func (packet *SC_EnemyChasePacket) write() []byte {
    buf := new(bytes.Buffer)
    packetSize := sizeof(reflect.TypeOf(*packet))
    

	binary.Write(buf, binary.LittleEndian, packetSize)
	binary.Write(buf, binary.LittleEndian, packet.getType())
    binary.Write(buf, binary.LittleEndian, packet.id)
	binary.Write(buf, binary.LittleEndian, packet.posX)
	binary.Write(buf, binary.LittleEndian, packet.posY)
	binary.Write(buf, binary.LittleEndian, packet.posZ)
	binary.Write(buf, binary.LittleEndian, packet.targetPosX)
	binary.Write(buf, binary.LittleEndian, packet.targetposY)
	binary.Write(buf, binary.LittleEndian, packet.targetposZ)    

    return buf.Bytes()
}

func (packet *SC_EnemyChasePacket) read(data []byte) {
    readbuf := bytes.NewBuffer(data)
	binary.Read(readbuf, binary.LittleEndian, &packet.Size)
	binary.Read(readbuf, binary.LittleEndian, &packet.Type)
    binary.Read(readbuf, binary.LittleEndian, &packet.id)
	binary.Read(readbuf, binary.LittleEndian, &packet.posX)
	binary.Read(readbuf, binary.LittleEndian, &packet.posY)
	binary.Read(readbuf, binary.LittleEndian, &packet.posZ)
	binary.Read(readbuf, binary.LittleEndian, &packet.targetPosX)
	binary.Read(readbuf, binary.LittleEndian, &packet.targetposY)
	binary.Read(readbuf, binary.LittleEndian, &packet.targetposZ)
}

type SC_EnemyStopChasePacket struct {
	Size int16
	Type int16
    id int32;
	posX float32;
	posY float32;
	posZ float32;
}

func (packet *SC_EnemyStopChasePacket) getType() int16 {
	return SC_EnemyStopChase
}

func (packet *SC_EnemyStopChasePacket) write() []byte {
    buf := new(bytes.Buffer)
    packetSize := sizeof(reflect.TypeOf(*packet))
    

	binary.Write(buf, binary.LittleEndian, packetSize)
	binary.Write(buf, binary.LittleEndian, packet.getType())
    binary.Write(buf, binary.LittleEndian, packet.id)
	binary.Write(buf, binary.LittleEndian, packet.posX)
	binary.Write(buf, binary.LittleEndian, packet.posY)
	binary.Write(buf, binary.LittleEndian, packet.posZ)    

    return buf.Bytes()
}

func (packet *SC_EnemyStopChasePacket) read(data []byte) {
    readbuf := bytes.NewBuffer(data)
	binary.Read(readbuf, binary.LittleEndian, &packet.Size)
	binary.Read(readbuf, binary.LittleEndian, &packet.Type)
    binary.Read(readbuf, binary.LittleEndian, &packet.id)
	binary.Read(readbuf, binary.LittleEndian, &packet.posX)
	binary.Read(readbuf, binary.LittleEndian, &packet.posY)
	binary.Read(readbuf, binary.LittleEndian, &packet.posZ)
}

type SC_EnemyAttackPacket struct {
	Size int16
	Type int16
    id int32;
	target int32;
}

func (packet *SC_EnemyAttackPacket) getType() int16 {
	return SC_EnemyAttack
}

func (packet *SC_EnemyAttackPacket) write() []byte {
    buf := new(bytes.Buffer)
    packetSize := sizeof(reflect.TypeOf(*packet))
    

	binary.Write(buf, binary.LittleEndian, packetSize)
	binary.Write(buf, binary.LittleEndian, packet.getType())
    binary.Write(buf, binary.LittleEndian, packet.id)
	binary.Write(buf, binary.LittleEndian, packet.target)    

    return buf.Bytes()
}

func (packet *SC_EnemyAttackPacket) read(data []byte) {
    readbuf := bytes.NewBuffer(data)
	binary.Read(readbuf, binary.LittleEndian, &packet.Size)
	binary.Read(readbuf, binary.LittleEndian, &packet.Type)
    binary.Read(readbuf, binary.LittleEndian, &packet.id)
	binary.Read(readbuf, binary.LittleEndian, &packet.target)
}

type CS_EnemyHitPacket struct {
	Size int16
	Type int16
    pid int32;
	eid int32;
}

func (packet *CS_EnemyHitPacket) getType() int16 {
	return CS_EnemyHit
}

func (packet *CS_EnemyHitPacket) write() []byte {
    buf := new(bytes.Buffer)
    packetSize := sizeof(reflect.TypeOf(*packet))
    

	binary.Write(buf, binary.LittleEndian, packetSize)
	binary.Write(buf, binary.LittleEndian, packet.getType())
    binary.Write(buf, binary.LittleEndian, packet.pid)
	binary.Write(buf, binary.LittleEndian, packet.eid)    

    return buf.Bytes()
}

func (packet *CS_EnemyHitPacket) read(data []byte) {
    readbuf := bytes.NewBuffer(data)
	binary.Read(readbuf, binary.LittleEndian, &packet.Size)
	binary.Read(readbuf, binary.LittleEndian, &packet.Type)
    binary.Read(readbuf, binary.LittleEndian, &packet.pid)
	binary.Read(readbuf, binary.LittleEndian, &packet.eid)
}

type SC_EnemyHitPacket struct {
	Size int16
	Type int16
    id int32;
	hp int32;
}

func (packet *SC_EnemyHitPacket) getType() int16 {
	return SC_EnemyHit
}

func (packet *SC_EnemyHitPacket) write() []byte {
    buf := new(bytes.Buffer)
    packetSize := sizeof(reflect.TypeOf(*packet))
    

	binary.Write(buf, binary.LittleEndian, packetSize)
	binary.Write(buf, binary.LittleEndian, packet.getType())
    binary.Write(buf, binary.LittleEndian, packet.id)
	binary.Write(buf, binary.LittleEndian, packet.hp)    

    return buf.Bytes()
}

func (packet *SC_EnemyHitPacket) read(data []byte) {
    readbuf := bytes.NewBuffer(data)
	binary.Read(readbuf, binary.LittleEndian, &packet.Size)
	binary.Read(readbuf, binary.LittleEndian, &packet.Type)
    binary.Read(readbuf, binary.LittleEndian, &packet.id)
	binary.Read(readbuf, binary.LittleEndian, &packet.hp)
}

type SC_EnemyDiePacket struct {
	Size int16
	Type int16
    id int32;
	hp int32;
}

func (packet *SC_EnemyDiePacket) getType() int16 {
	return SC_EnemyDie
}

func (packet *SC_EnemyDiePacket) write() []byte {
    buf := new(bytes.Buffer)
    packetSize := sizeof(reflect.TypeOf(*packet))
    

	binary.Write(buf, binary.LittleEndian, packetSize)
	binary.Write(buf, binary.LittleEndian, packet.getType())
    binary.Write(buf, binary.LittleEndian, packet.id)
	binary.Write(buf, binary.LittleEndian, packet.hp)    

    return buf.Bytes()
}

func (packet *SC_EnemyDiePacket) read(data []byte) {
    readbuf := bytes.NewBuffer(data)
	binary.Read(readbuf, binary.LittleEndian, &packet.Size)
	binary.Read(readbuf, binary.LittleEndian, &packet.Type)
    binary.Read(readbuf, binary.LittleEndian, &packet.id)
	binary.Read(readbuf, binary.LittleEndian, &packet.hp)
}

type CS_PlayerHitPacket struct {
	Size int16
	Type int16
    eid int32;
}

func (packet *CS_PlayerHitPacket) getType() int16 {
	return CS_PlayerHit
}

func (packet *CS_PlayerHitPacket) write() []byte {
    buf := new(bytes.Buffer)
    packetSize := sizeof(reflect.TypeOf(*packet))
    

	binary.Write(buf, binary.LittleEndian, packetSize)
	binary.Write(buf, binary.LittleEndian, packet.getType())
    binary.Write(buf, binary.LittleEndian, packet.eid)    

    return buf.Bytes()
}

func (packet *CS_PlayerHitPacket) read(data []byte) {
    readbuf := bytes.NewBuffer(data)
	binary.Read(readbuf, binary.LittleEndian, &packet.Size)
	binary.Read(readbuf, binary.LittleEndian, &packet.Type)
    binary.Read(readbuf, binary.LittleEndian, &packet.eid)
}

type SC_PlayerHitPacket struct {
	Size int16
	Type int16
    id int32;
	hp int32;
}

func (packet *SC_PlayerHitPacket) getType() int16 {
	return SC_PlayerHit
}

func (packet *SC_PlayerHitPacket) write() []byte {
    buf := new(bytes.Buffer)
    packetSize := sizeof(reflect.TypeOf(*packet))
    

	binary.Write(buf, binary.LittleEndian, packetSize)
	binary.Write(buf, binary.LittleEndian, packet.getType())
    binary.Write(buf, binary.LittleEndian, packet.id)
	binary.Write(buf, binary.LittleEndian, packet.hp)    

    return buf.Bytes()
}

func (packet *SC_PlayerHitPacket) read(data []byte) {
    readbuf := bytes.NewBuffer(data)
	binary.Read(readbuf, binary.LittleEndian, &packet.Size)
	binary.Read(readbuf, binary.LittleEndian, &packet.Type)
    binary.Read(readbuf, binary.LittleEndian, &packet.id)
	binary.Read(readbuf, binary.LittleEndian, &packet.hp)
}

type SC_BossCreatePacket struct {
	Size int16
	Type int16
    id int32;
	btype int32;
	hp int32;
	rotate float32;
	posX float32;
	posY float32;
	posZ float32;
}

func (packet *SC_BossCreatePacket) getType() int16 {
	return SC_BossCreate
}

func (packet *SC_BossCreatePacket) write() []byte {
    buf := new(bytes.Buffer)
    packetSize := sizeof(reflect.TypeOf(*packet))
    

	binary.Write(buf, binary.LittleEndian, packetSize)
	binary.Write(buf, binary.LittleEndian, packet.getType())
    binary.Write(buf, binary.LittleEndian, packet.id)
	binary.Write(buf, binary.LittleEndian, packet.btype)
	binary.Write(buf, binary.LittleEndian, packet.hp)
	binary.Write(buf, binary.LittleEndian, packet.rotate)
	binary.Write(buf, binary.LittleEndian, packet.posX)
	binary.Write(buf, binary.LittleEndian, packet.posY)
	binary.Write(buf, binary.LittleEndian, packet.posZ)    

    return buf.Bytes()
}

func (packet *SC_BossCreatePacket) read(data []byte) {
    readbuf := bytes.NewBuffer(data)
	binary.Read(readbuf, binary.LittleEndian, &packet.Size)
	binary.Read(readbuf, binary.LittleEndian, &packet.Type)
    binary.Read(readbuf, binary.LittleEndian, &packet.id)
	binary.Read(readbuf, binary.LittleEndian, &packet.btype)
	binary.Read(readbuf, binary.LittleEndian, &packet.hp)
	binary.Read(readbuf, binary.LittleEndian, &packet.rotate)
	binary.Read(readbuf, binary.LittleEndian, &packet.posX)
	binary.Read(readbuf, binary.LittleEndian, &packet.posY)
	binary.Read(readbuf, binary.LittleEndian, &packet.posZ)
}

type SC_BossAttackPacket struct {
	Size int16
	Type int16
    id int32;
	pattern int32;
}

func (packet *SC_BossAttackPacket) getType() int16 {
	return SC_BossAttack
}

func (packet *SC_BossAttackPacket) write() []byte {
    buf := new(bytes.Buffer)
    packetSize := sizeof(reflect.TypeOf(*packet))
    

	binary.Write(buf, binary.LittleEndian, packetSize)
	binary.Write(buf, binary.LittleEndian, packet.getType())
    binary.Write(buf, binary.LittleEndian, packet.id)
	binary.Write(buf, binary.LittleEndian, packet.pattern)    

    return buf.Bytes()
}

func (packet *SC_BossAttackPacket) read(data []byte) {
    readbuf := bytes.NewBuffer(data)
	binary.Read(readbuf, binary.LittleEndian, &packet.Size)
	binary.Read(readbuf, binary.LittleEndian, &packet.Type)
    binary.Read(readbuf, binary.LittleEndian, &packet.id)
	binary.Read(readbuf, binary.LittleEndian, &packet.pattern)
}

