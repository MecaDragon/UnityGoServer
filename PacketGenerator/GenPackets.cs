using System;
using System.Collections.Generic;
using System.Text;
using System.Net;
using ServerCore;

public enum PacketID
{
    CS_LeaveGame = 1,
	CS_Move = 2,
	SC_BroadcastEnterGame = 3,
	SC_BroadcastLeaveGame = 4,
	SC_LoginPlayer = 5,
	SC_BroadcastMove = 6,
	CS_Chat = 7,
	SC_Chat = 8,
	CS_Login = 9,
	SC_Login = 10,
	CS_MainSceneLoad = 11,
	SC_NPCCreate = 12,
	SC_NPCMove = 13,
	CS_Skin = 14,
	SC_MainSceneLoad = 15,
	CS_Roll = 16,
	SC_Roll = 17,
	CS_PlayerAttack = 18,
	SC_PlayerAttack = 19,
	SC_EnemyCreate = 20,
	SC_EnemyStartChase = 21,
	SC_EnemyChase = 22,
	SC_EnemyStopChase = 23,
	SC_EnemyAttack = 24,
	CS_EnemyHit = 25,
	SC_EnemyHit = 26,
	SC_EnemyDie = 27,
	CS_PlayerHit = 28,
	SC_PlayerHit = 29,
	SC_BossCreate = 30,
	SC_BossAttack = 31,
	
}

public interface IPacket
{
	ushort Protocol { get; }
	void Read(ArraySegment<byte> segment);
	ArraySegment<byte> Write();
}


public class CS_LeaveGame : IPacket
{
    

    public ushort Protocol { get { return (ushort)PacketID.CS_LeaveGame; } }

    public void Read(ArraySegment<byte> segment)
    {
        ushort count = 0;

        ReadOnlySpan<byte> s = new ReadOnlySpan<byte>(segment.Array, segment.Offset, segment.Count);

        // size / packetId
        count += sizeof(ushort);
        count += sizeof(ushort);

        
    }

    public ArraySegment<byte> Write()
    {
        ArraySegment<byte> segment = SendBufferHelper.Open(4096);

        ushort count = 0;
        bool success = true;

        Span<byte> s = new Span<byte>(segment.Array, segment.Offset, segment.Count);
        count += sizeof(ushort);
        success &= BitConverter.TryWriteBytes(s.Slice(count, s.Length - count), (ushort)PacketID.CS_LeaveGame);
        count += sizeof(ushort);

         

        success &= BitConverter.TryWriteBytes(s, count);

        if (success == false) {
            return null;
        }

        return SendBufferHelper.Close(count);
    }
}

public class CS_Move : IPacket
{
    public bool isRun;
	public bool isWalk;
	public bool isRoll;
	public float posX;
	public float posY;
	public float posZ;
	public float lookX;
	public float lookY;
	public float lookZ;

    public ushort Protocol { get { return (ushort)PacketID.CS_Move; } }

    public void Read(ArraySegment<byte> segment)
    {
        ushort count = 0;

        ReadOnlySpan<byte> s = new ReadOnlySpan<byte>(segment.Array, segment.Offset, segment.Count);

        // size / packetId
        count += sizeof(ushort);
        count += sizeof(ushort);

        this.isRun = BitConverter.ToBoolean(s.Slice(count, s.Length - count));
		                count += sizeof(bool);
		this.isWalk = BitConverter.ToBoolean(s.Slice(count, s.Length - count));
		                count += sizeof(bool);
		this.isRoll = BitConverter.ToBoolean(s.Slice(count, s.Length - count));
		                count += sizeof(bool);
		this.posX = BitConverter.ToSingle(s.Slice(count, s.Length - count));
		                count += sizeof(float);
		this.posY = BitConverter.ToSingle(s.Slice(count, s.Length - count));
		                count += sizeof(float);
		this.posZ = BitConverter.ToSingle(s.Slice(count, s.Length - count));
		                count += sizeof(float);
		this.lookX = BitConverter.ToSingle(s.Slice(count, s.Length - count));
		                count += sizeof(float);
		this.lookY = BitConverter.ToSingle(s.Slice(count, s.Length - count));
		                count += sizeof(float);
		this.lookZ = BitConverter.ToSingle(s.Slice(count, s.Length - count));
		                count += sizeof(float);
    }

    public ArraySegment<byte> Write()
    {
        ArraySegment<byte> segment = SendBufferHelper.Open(4096);

        ushort count = 0;
        bool success = true;

        Span<byte> s = new Span<byte>(segment.Array, segment.Offset, segment.Count);
        count += sizeof(ushort);
        success &= BitConverter.TryWriteBytes(s.Slice(count, s.Length - count), (ushort)PacketID.CS_Move);
        count += sizeof(ushort);

        success &= BitConverter.TryWriteBytes(s.Slice(count, s.Length - count), this.isRun);
		count += sizeof(bool);
		
		success &= BitConverter.TryWriteBytes(s.Slice(count, s.Length - count), this.isWalk);
		count += sizeof(bool);
		
		success &= BitConverter.TryWriteBytes(s.Slice(count, s.Length - count), this.isRoll);
		count += sizeof(bool);
		
		success &= BitConverter.TryWriteBytes(s.Slice(count, s.Length - count), this.posX);
		count += sizeof(float);
		
		success &= BitConverter.TryWriteBytes(s.Slice(count, s.Length - count), this.posY);
		count += sizeof(float);
		
		success &= BitConverter.TryWriteBytes(s.Slice(count, s.Length - count), this.posZ);
		count += sizeof(float);
		
		success &= BitConverter.TryWriteBytes(s.Slice(count, s.Length - count), this.lookX);
		count += sizeof(float);
		
		success &= BitConverter.TryWriteBytes(s.Slice(count, s.Length - count), this.lookY);
		count += sizeof(float);
		
		success &= BitConverter.TryWriteBytes(s.Slice(count, s.Length - count), this.lookZ);
		count += sizeof(float);
		 

        success &= BitConverter.TryWriteBytes(s, count);

        if (success == false) {
            return null;
        }

        return SendBufferHelper.Close(count);
    }
}

public class SC_BroadcastEnterGame : IPacket
{
    public int playerId;
	public float posX;
	public float posY;
	public float posZ;
	public int skin;
	public string name;

    public ushort Protocol { get { return (ushort)PacketID.SC_BroadcastEnterGame; } }

    public void Read(ArraySegment<byte> segment)
    {
        ushort count = 0;

        ReadOnlySpan<byte> s = new ReadOnlySpan<byte>(segment.Array, segment.Offset, segment.Count);

        // size / packetId
        count += sizeof(ushort);
        count += sizeof(ushort);

        this.playerId = BitConverter.ToInt32(s.Slice(count, s.Length - count));
		                count += sizeof(int);
		this.posX = BitConverter.ToSingle(s.Slice(count, s.Length - count));
		                count += sizeof(float);
		this.posY = BitConverter.ToSingle(s.Slice(count, s.Length - count));
		                count += sizeof(float);
		this.posZ = BitConverter.ToSingle(s.Slice(count, s.Length - count));
		                count += sizeof(float);
		this.skin = BitConverter.ToInt32(s.Slice(count, s.Length - count));
		                count += sizeof(int);
		ushort nameLength = BitConverter.ToUInt16(s.Slice(count, s.Length - count));
		count += sizeof(ushort);
		this.name = Encoding.UTF8.GetString(s.Slice(count, nameLength));
		count += nameLength;
		
    }

    public ArraySegment<byte> Write()
    {
        ArraySegment<byte> segment = SendBufferHelper.Open(4096);

        ushort count = 0;
        bool success = true;

        Span<byte> s = new Span<byte>(segment.Array, segment.Offset, segment.Count);
        count += sizeof(ushort);
        success &= BitConverter.TryWriteBytes(s.Slice(count, s.Length - count), (ushort)PacketID.SC_BroadcastEnterGame);
        count += sizeof(ushort);

        success &= BitConverter.TryWriteBytes(s.Slice(count, s.Length - count), this.playerId);
		count += sizeof(int);
		
		success &= BitConverter.TryWriteBytes(s.Slice(count, s.Length - count), this.posX);
		count += sizeof(float);
		
		success &= BitConverter.TryWriteBytes(s.Slice(count, s.Length - count), this.posY);
		count += sizeof(float);
		
		success &= BitConverter.TryWriteBytes(s.Slice(count, s.Length - count), this.posZ);
		count += sizeof(float);
		
		success &= BitConverter.TryWriteBytes(s.Slice(count, s.Length - count), this.skin);
		count += sizeof(int);
		
		ushort nameLength = (ushort)Encoding.UTF8.GetBytes(this.name, 0, this.name.Length, segment.Array, segment.Offset + count + sizeof(ushort));
		success &= BitConverter.TryWriteBytes(s.Slice(count, s.Length - count), nameLength);
		count += sizeof(ushort);
		count += nameLength;
		 

        success &= BitConverter.TryWriteBytes(s, count);

        if (success == false) {
            return null;
        }

        return SendBufferHelper.Close(count);
    }
}

public class SC_BroadcastLeaveGame : IPacket
{
    public int playerId;

    public ushort Protocol { get { return (ushort)PacketID.SC_BroadcastLeaveGame; } }

    public void Read(ArraySegment<byte> segment)
    {
        ushort count = 0;

        ReadOnlySpan<byte> s = new ReadOnlySpan<byte>(segment.Array, segment.Offset, segment.Count);

        // size / packetId
        count += sizeof(ushort);
        count += sizeof(ushort);

        this.playerId = BitConverter.ToInt32(s.Slice(count, s.Length - count));
		                count += sizeof(int);
    }

    public ArraySegment<byte> Write()
    {
        ArraySegment<byte> segment = SendBufferHelper.Open(4096);

        ushort count = 0;
        bool success = true;

        Span<byte> s = new Span<byte>(segment.Array, segment.Offset, segment.Count);
        count += sizeof(ushort);
        success &= BitConverter.TryWriteBytes(s.Slice(count, s.Length - count), (ushort)PacketID.SC_BroadcastLeaveGame);
        count += sizeof(ushort);

        success &= BitConverter.TryWriteBytes(s.Slice(count, s.Length - count), this.playerId);
		count += sizeof(int);
		 

        success &= BitConverter.TryWriteBytes(s, count);

        if (success == false) {
            return null;
        }

        return SendBufferHelper.Close(count);
    }
}

public class SC_LoginPlayer : IPacket
{
    public bool isSelf;
	public int playerId;
	public bool isRun;
	public bool isWalk;
	public bool isRoll;
	public float posX;
	public float posY;
	public float posZ;
	public float lookX;
	public float lookY;
	public float lookZ;
	public int skin;
	public string name;

    public ushort Protocol { get { return (ushort)PacketID.SC_LoginPlayer; } }

    public void Read(ArraySegment<byte> segment)
    {
        ushort count = 0;

        ReadOnlySpan<byte> s = new ReadOnlySpan<byte>(segment.Array, segment.Offset, segment.Count);

        // size / packetId
        count += sizeof(ushort);
        count += sizeof(ushort);

        this.isSelf = BitConverter.ToBoolean(s.Slice(count, s.Length - count));
		                count += sizeof(bool);
		this.playerId = BitConverter.ToInt32(s.Slice(count, s.Length - count));
		                count += sizeof(int);
		this.isRun = BitConverter.ToBoolean(s.Slice(count, s.Length - count));
		                count += sizeof(bool);
		this.isWalk = BitConverter.ToBoolean(s.Slice(count, s.Length - count));
		                count += sizeof(bool);
		this.isRoll = BitConverter.ToBoolean(s.Slice(count, s.Length - count));
		                count += sizeof(bool);
		this.posX = BitConverter.ToSingle(s.Slice(count, s.Length - count));
		                count += sizeof(float);
		this.posY = BitConverter.ToSingle(s.Slice(count, s.Length - count));
		                count += sizeof(float);
		this.posZ = BitConverter.ToSingle(s.Slice(count, s.Length - count));
		                count += sizeof(float);
		this.lookX = BitConverter.ToSingle(s.Slice(count, s.Length - count));
		                count += sizeof(float);
		this.lookY = BitConverter.ToSingle(s.Slice(count, s.Length - count));
		                count += sizeof(float);
		this.lookZ = BitConverter.ToSingle(s.Slice(count, s.Length - count));
		                count += sizeof(float);
		this.skin = BitConverter.ToInt32(s.Slice(count, s.Length - count));
		                count += sizeof(int);
		ushort nameLength = BitConverter.ToUInt16(s.Slice(count, s.Length - count));
		count += sizeof(ushort);
		this.name = Encoding.UTF8.GetString(s.Slice(count, nameLength));
		count += nameLength;
		
    }

    public ArraySegment<byte> Write()
    {
        ArraySegment<byte> segment = SendBufferHelper.Open(4096);

        ushort count = 0;
        bool success = true;

        Span<byte> s = new Span<byte>(segment.Array, segment.Offset, segment.Count);
        count += sizeof(ushort);
        success &= BitConverter.TryWriteBytes(s.Slice(count, s.Length - count), (ushort)PacketID.SC_LoginPlayer);
        count += sizeof(ushort);

        success &= BitConverter.TryWriteBytes(s.Slice(count, s.Length - count), this.isSelf);
		count += sizeof(bool);
		
		success &= BitConverter.TryWriteBytes(s.Slice(count, s.Length - count), this.playerId);
		count += sizeof(int);
		
		success &= BitConverter.TryWriteBytes(s.Slice(count, s.Length - count), this.isRun);
		count += sizeof(bool);
		
		success &= BitConverter.TryWriteBytes(s.Slice(count, s.Length - count), this.isWalk);
		count += sizeof(bool);
		
		success &= BitConverter.TryWriteBytes(s.Slice(count, s.Length - count), this.isRoll);
		count += sizeof(bool);
		
		success &= BitConverter.TryWriteBytes(s.Slice(count, s.Length - count), this.posX);
		count += sizeof(float);
		
		success &= BitConverter.TryWriteBytes(s.Slice(count, s.Length - count), this.posY);
		count += sizeof(float);
		
		success &= BitConverter.TryWriteBytes(s.Slice(count, s.Length - count), this.posZ);
		count += sizeof(float);
		
		success &= BitConverter.TryWriteBytes(s.Slice(count, s.Length - count), this.lookX);
		count += sizeof(float);
		
		success &= BitConverter.TryWriteBytes(s.Slice(count, s.Length - count), this.lookY);
		count += sizeof(float);
		
		success &= BitConverter.TryWriteBytes(s.Slice(count, s.Length - count), this.lookZ);
		count += sizeof(float);
		
		success &= BitConverter.TryWriteBytes(s.Slice(count, s.Length - count), this.skin);
		count += sizeof(int);
		
		ushort nameLength = (ushort)Encoding.UTF8.GetBytes(this.name, 0, this.name.Length, segment.Array, segment.Offset + count + sizeof(ushort));
		success &= BitConverter.TryWriteBytes(s.Slice(count, s.Length - count), nameLength);
		count += sizeof(ushort);
		count += nameLength;
		 

        success &= BitConverter.TryWriteBytes(s, count);

        if (success == false) {
            return null;
        }

        return SendBufferHelper.Close(count);
    }
}

public class SC_BroadcastMove : IPacket
{
    public int playerId;
	public bool isRun;
	public bool isWalk;
	public bool isRoll;
	public float posX;
	public float posY;
	public float posZ;
	public float lookX;
	public float lookY;
	public float lookZ;

    public ushort Protocol { get { return (ushort)PacketID.SC_BroadcastMove; } }

    public void Read(ArraySegment<byte> segment)
    {
        ushort count = 0;

        ReadOnlySpan<byte> s = new ReadOnlySpan<byte>(segment.Array, segment.Offset, segment.Count);

        // size / packetId
        count += sizeof(ushort);
        count += sizeof(ushort);

        this.playerId = BitConverter.ToInt32(s.Slice(count, s.Length - count));
		                count += sizeof(int);
		this.isRun = BitConverter.ToBoolean(s.Slice(count, s.Length - count));
		                count += sizeof(bool);
		this.isWalk = BitConverter.ToBoolean(s.Slice(count, s.Length - count));
		                count += sizeof(bool);
		this.isRoll = BitConverter.ToBoolean(s.Slice(count, s.Length - count));
		                count += sizeof(bool);
		this.posX = BitConverter.ToSingle(s.Slice(count, s.Length - count));
		                count += sizeof(float);
		this.posY = BitConverter.ToSingle(s.Slice(count, s.Length - count));
		                count += sizeof(float);
		this.posZ = BitConverter.ToSingle(s.Slice(count, s.Length - count));
		                count += sizeof(float);
		this.lookX = BitConverter.ToSingle(s.Slice(count, s.Length - count));
		                count += sizeof(float);
		this.lookY = BitConverter.ToSingle(s.Slice(count, s.Length - count));
		                count += sizeof(float);
		this.lookZ = BitConverter.ToSingle(s.Slice(count, s.Length - count));
		                count += sizeof(float);
    }

    public ArraySegment<byte> Write()
    {
        ArraySegment<byte> segment = SendBufferHelper.Open(4096);

        ushort count = 0;
        bool success = true;

        Span<byte> s = new Span<byte>(segment.Array, segment.Offset, segment.Count);
        count += sizeof(ushort);
        success &= BitConverter.TryWriteBytes(s.Slice(count, s.Length - count), (ushort)PacketID.SC_BroadcastMove);
        count += sizeof(ushort);

        success &= BitConverter.TryWriteBytes(s.Slice(count, s.Length - count), this.playerId);
		count += sizeof(int);
		
		success &= BitConverter.TryWriteBytes(s.Slice(count, s.Length - count), this.isRun);
		count += sizeof(bool);
		
		success &= BitConverter.TryWriteBytes(s.Slice(count, s.Length - count), this.isWalk);
		count += sizeof(bool);
		
		success &= BitConverter.TryWriteBytes(s.Slice(count, s.Length - count), this.isRoll);
		count += sizeof(bool);
		
		success &= BitConverter.TryWriteBytes(s.Slice(count, s.Length - count), this.posX);
		count += sizeof(float);
		
		success &= BitConverter.TryWriteBytes(s.Slice(count, s.Length - count), this.posY);
		count += sizeof(float);
		
		success &= BitConverter.TryWriteBytes(s.Slice(count, s.Length - count), this.posZ);
		count += sizeof(float);
		
		success &= BitConverter.TryWriteBytes(s.Slice(count, s.Length - count), this.lookX);
		count += sizeof(float);
		
		success &= BitConverter.TryWriteBytes(s.Slice(count, s.Length - count), this.lookY);
		count += sizeof(float);
		
		success &= BitConverter.TryWriteBytes(s.Slice(count, s.Length - count), this.lookZ);
		count += sizeof(float);
		 

        success &= BitConverter.TryWriteBytes(s, count);

        if (success == false) {
            return null;
        }

        return SendBufferHelper.Close(count);
    }
}

public class CS_Chat : IPacket
{
    public string chat;

    public ushort Protocol { get { return (ushort)PacketID.CS_Chat; } }

    public void Read(ArraySegment<byte> segment)
    {
        ushort count = 0;

        ReadOnlySpan<byte> s = new ReadOnlySpan<byte>(segment.Array, segment.Offset, segment.Count);

        // size / packetId
        count += sizeof(ushort);
        count += sizeof(ushort);

        ushort chatLength = BitConverter.ToUInt16(s.Slice(count, s.Length - count));
		count += sizeof(ushort);
		this.chat = Encoding.UTF8.GetString(s.Slice(count, chatLength));
		count += chatLength;
		
    }

    public ArraySegment<byte> Write()
    {
        ArraySegment<byte> segment = SendBufferHelper.Open(4096);

        ushort count = 0;
        bool success = true;

        Span<byte> s = new Span<byte>(segment.Array, segment.Offset, segment.Count);
        count += sizeof(ushort);
        success &= BitConverter.TryWriteBytes(s.Slice(count, s.Length - count), (ushort)PacketID.CS_Chat);
        count += sizeof(ushort);

        ushort chatLength = (ushort)Encoding.UTF8.GetBytes(this.chat, 0, this.chat.Length, segment.Array, segment.Offset + count + sizeof(ushort));
		success &= BitConverter.TryWriteBytes(s.Slice(count, s.Length - count), chatLength);
		count += sizeof(ushort);
		count += chatLength;
		 

        success &= BitConverter.TryWriteBytes(s, count);

        if (success == false) {
            return null;
        }

        return SendBufferHelper.Close(count);
    }
}

public class SC_Chat : IPacket
{
    public int playerId;
	public string chat;

    public ushort Protocol { get { return (ushort)PacketID.SC_Chat; } }

    public void Read(ArraySegment<byte> segment)
    {
        ushort count = 0;

        ReadOnlySpan<byte> s = new ReadOnlySpan<byte>(segment.Array, segment.Offset, segment.Count);

        // size / packetId
        count += sizeof(ushort);
        count += sizeof(ushort);

        this.playerId = BitConverter.ToInt32(s.Slice(count, s.Length - count));
		                count += sizeof(int);
		ushort chatLength = BitConverter.ToUInt16(s.Slice(count, s.Length - count));
		count += sizeof(ushort);
		this.chat = Encoding.UTF8.GetString(s.Slice(count, chatLength));
		count += chatLength;
		
    }

    public ArraySegment<byte> Write()
    {
        ArraySegment<byte> segment = SendBufferHelper.Open(4096);

        ushort count = 0;
        bool success = true;

        Span<byte> s = new Span<byte>(segment.Array, segment.Offset, segment.Count);
        count += sizeof(ushort);
        success &= BitConverter.TryWriteBytes(s.Slice(count, s.Length - count), (ushort)PacketID.SC_Chat);
        count += sizeof(ushort);

        success &= BitConverter.TryWriteBytes(s.Slice(count, s.Length - count), this.playerId);
		count += sizeof(int);
		
		ushort chatLength = (ushort)Encoding.UTF8.GetBytes(this.chat, 0, this.chat.Length, segment.Array, segment.Offset + count + sizeof(ushort));
		success &= BitConverter.TryWriteBytes(s.Slice(count, s.Length - count), chatLength);
		count += sizeof(ushort);
		count += chatLength;
		 

        success &= BitConverter.TryWriteBytes(s, count);

        if (success == false) {
            return null;
        }

        return SendBufferHelper.Close(count);
    }
}

public class CS_Login : IPacket
{
    public string name;

    public ushort Protocol { get { return (ushort)PacketID.CS_Login; } }

    public void Read(ArraySegment<byte> segment)
    {
        ushort count = 0;

        ReadOnlySpan<byte> s = new ReadOnlySpan<byte>(segment.Array, segment.Offset, segment.Count);

        // size / packetId
        count += sizeof(ushort);
        count += sizeof(ushort);

        ushort nameLength = BitConverter.ToUInt16(s.Slice(count, s.Length - count));
		count += sizeof(ushort);
		this.name = Encoding.UTF8.GetString(s.Slice(count, nameLength));
		count += nameLength;
		
    }

    public ArraySegment<byte> Write()
    {
        ArraySegment<byte> segment = SendBufferHelper.Open(4096);

        ushort count = 0;
        bool success = true;

        Span<byte> s = new Span<byte>(segment.Array, segment.Offset, segment.Count);
        count += sizeof(ushort);
        success &= BitConverter.TryWriteBytes(s.Slice(count, s.Length - count), (ushort)PacketID.CS_Login);
        count += sizeof(ushort);

        ushort nameLength = (ushort)Encoding.UTF8.GetBytes(this.name, 0, this.name.Length, segment.Array, segment.Offset + count + sizeof(ushort));
		success &= BitConverter.TryWriteBytes(s.Slice(count, s.Length - count), nameLength);
		count += sizeof(ushort);
		count += nameLength;
		 

        success &= BitConverter.TryWriteBytes(s, count);

        if (success == false) {
            return null;
        }

        return SendBufferHelper.Close(count);
    }
}

public class SC_Login : IPacket
{
    public bool suceess;
	public string reason;

    public ushort Protocol { get { return (ushort)PacketID.SC_Login; } }

    public void Read(ArraySegment<byte> segment)
    {
        ushort count = 0;

        ReadOnlySpan<byte> s = new ReadOnlySpan<byte>(segment.Array, segment.Offset, segment.Count);

        // size / packetId
        count += sizeof(ushort);
        count += sizeof(ushort);

        this.suceess = BitConverter.ToBoolean(s.Slice(count, s.Length - count));
		                count += sizeof(bool);
		ushort reasonLength = BitConverter.ToUInt16(s.Slice(count, s.Length - count));
		count += sizeof(ushort);
		this.reason = Encoding.UTF8.GetString(s.Slice(count, reasonLength));
		count += reasonLength;
		
    }

    public ArraySegment<byte> Write()
    {
        ArraySegment<byte> segment = SendBufferHelper.Open(4096);

        ushort count = 0;
        bool success = true;

        Span<byte> s = new Span<byte>(segment.Array, segment.Offset, segment.Count);
        count += sizeof(ushort);
        success &= BitConverter.TryWriteBytes(s.Slice(count, s.Length - count), (ushort)PacketID.SC_Login);
        count += sizeof(ushort);

        success &= BitConverter.TryWriteBytes(s.Slice(count, s.Length - count), this.suceess);
		count += sizeof(bool);
		
		ushort reasonLength = (ushort)Encoding.UTF8.GetBytes(this.reason, 0, this.reason.Length, segment.Array, segment.Offset + count + sizeof(ushort));
		success &= BitConverter.TryWriteBytes(s.Slice(count, s.Length - count), reasonLength);
		count += sizeof(ushort);
		count += reasonLength;
		 

        success &= BitConverter.TryWriteBytes(s, count);

        if (success == false) {
            return null;
        }

        return SendBufferHelper.Close(count);
    }
}

public class CS_MainSceneLoad : IPacket
{
    

    public ushort Protocol { get { return (ushort)PacketID.CS_MainSceneLoad; } }

    public void Read(ArraySegment<byte> segment)
    {
        ushort count = 0;

        ReadOnlySpan<byte> s = new ReadOnlySpan<byte>(segment.Array, segment.Offset, segment.Count);

        // size / packetId
        count += sizeof(ushort);
        count += sizeof(ushort);

        
    }

    public ArraySegment<byte> Write()
    {
        ArraySegment<byte> segment = SendBufferHelper.Open(4096);

        ushort count = 0;
        bool success = true;

        Span<byte> s = new Span<byte>(segment.Array, segment.Offset, segment.Count);
        count += sizeof(ushort);
        success &= BitConverter.TryWriteBytes(s.Slice(count, s.Length - count), (ushort)PacketID.CS_MainSceneLoad);
        count += sizeof(ushort);

         

        success &= BitConverter.TryWriteBytes(s, count);

        if (success == false) {
            return null;
        }

        return SendBufferHelper.Close(count);
    }
}

public class SC_NPCCreate : IPacket
{
    public int id;
	public float posX;
	public float posY;
	public float posZ;
	public float lookX;
	public float lookY;
	public float lookZ;

    public ushort Protocol { get { return (ushort)PacketID.SC_NPCCreate; } }

    public void Read(ArraySegment<byte> segment)
    {
        ushort count = 0;

        ReadOnlySpan<byte> s = new ReadOnlySpan<byte>(segment.Array, segment.Offset, segment.Count);

        // size / packetId
        count += sizeof(ushort);
        count += sizeof(ushort);

        this.id = BitConverter.ToInt32(s.Slice(count, s.Length - count));
		                count += sizeof(int);
		this.posX = BitConverter.ToSingle(s.Slice(count, s.Length - count));
		                count += sizeof(float);
		this.posY = BitConverter.ToSingle(s.Slice(count, s.Length - count));
		                count += sizeof(float);
		this.posZ = BitConverter.ToSingle(s.Slice(count, s.Length - count));
		                count += sizeof(float);
		this.lookX = BitConverter.ToSingle(s.Slice(count, s.Length - count));
		                count += sizeof(float);
		this.lookY = BitConverter.ToSingle(s.Slice(count, s.Length - count));
		                count += sizeof(float);
		this.lookZ = BitConverter.ToSingle(s.Slice(count, s.Length - count));
		                count += sizeof(float);
    }

    public ArraySegment<byte> Write()
    {
        ArraySegment<byte> segment = SendBufferHelper.Open(4096);

        ushort count = 0;
        bool success = true;

        Span<byte> s = new Span<byte>(segment.Array, segment.Offset, segment.Count);
        count += sizeof(ushort);
        success &= BitConverter.TryWriteBytes(s.Slice(count, s.Length - count), (ushort)PacketID.SC_NPCCreate);
        count += sizeof(ushort);

        success &= BitConverter.TryWriteBytes(s.Slice(count, s.Length - count), this.id);
		count += sizeof(int);
		
		success &= BitConverter.TryWriteBytes(s.Slice(count, s.Length - count), this.posX);
		count += sizeof(float);
		
		success &= BitConverter.TryWriteBytes(s.Slice(count, s.Length - count), this.posY);
		count += sizeof(float);
		
		success &= BitConverter.TryWriteBytes(s.Slice(count, s.Length - count), this.posZ);
		count += sizeof(float);
		
		success &= BitConverter.TryWriteBytes(s.Slice(count, s.Length - count), this.lookX);
		count += sizeof(float);
		
		success &= BitConverter.TryWriteBytes(s.Slice(count, s.Length - count), this.lookY);
		count += sizeof(float);
		
		success &= BitConverter.TryWriteBytes(s.Slice(count, s.Length - count), this.lookZ);
		count += sizeof(float);
		 

        success &= BitConverter.TryWriteBytes(s, count);

        if (success == false) {
            return null;
        }

        return SendBufferHelper.Close(count);
    }
}

public class SC_NPCMove : IPacket
{
    public int id;
	public float posX;
	public float posY;
	public float posZ;
	public float lookX;
	public float lookY;
	public float lookZ;

    public ushort Protocol { get { return (ushort)PacketID.SC_NPCMove; } }

    public void Read(ArraySegment<byte> segment)
    {
        ushort count = 0;

        ReadOnlySpan<byte> s = new ReadOnlySpan<byte>(segment.Array, segment.Offset, segment.Count);

        // size / packetId
        count += sizeof(ushort);
        count += sizeof(ushort);

        this.id = BitConverter.ToInt32(s.Slice(count, s.Length - count));
		                count += sizeof(int);
		this.posX = BitConverter.ToSingle(s.Slice(count, s.Length - count));
		                count += sizeof(float);
		this.posY = BitConverter.ToSingle(s.Slice(count, s.Length - count));
		                count += sizeof(float);
		this.posZ = BitConverter.ToSingle(s.Slice(count, s.Length - count));
		                count += sizeof(float);
		this.lookX = BitConverter.ToSingle(s.Slice(count, s.Length - count));
		                count += sizeof(float);
		this.lookY = BitConverter.ToSingle(s.Slice(count, s.Length - count));
		                count += sizeof(float);
		this.lookZ = BitConverter.ToSingle(s.Slice(count, s.Length - count));
		                count += sizeof(float);
    }

    public ArraySegment<byte> Write()
    {
        ArraySegment<byte> segment = SendBufferHelper.Open(4096);

        ushort count = 0;
        bool success = true;

        Span<byte> s = new Span<byte>(segment.Array, segment.Offset, segment.Count);
        count += sizeof(ushort);
        success &= BitConverter.TryWriteBytes(s.Slice(count, s.Length - count), (ushort)PacketID.SC_NPCMove);
        count += sizeof(ushort);

        success &= BitConverter.TryWriteBytes(s.Slice(count, s.Length - count), this.id);
		count += sizeof(int);
		
		success &= BitConverter.TryWriteBytes(s.Slice(count, s.Length - count), this.posX);
		count += sizeof(float);
		
		success &= BitConverter.TryWriteBytes(s.Slice(count, s.Length - count), this.posY);
		count += sizeof(float);
		
		success &= BitConverter.TryWriteBytes(s.Slice(count, s.Length - count), this.posZ);
		count += sizeof(float);
		
		success &= BitConverter.TryWriteBytes(s.Slice(count, s.Length - count), this.lookX);
		count += sizeof(float);
		
		success &= BitConverter.TryWriteBytes(s.Slice(count, s.Length - count), this.lookY);
		count += sizeof(float);
		
		success &= BitConverter.TryWriteBytes(s.Slice(count, s.Length - count), this.lookZ);
		count += sizeof(float);
		 

        success &= BitConverter.TryWriteBytes(s, count);

        if (success == false) {
            return null;
        }

        return SendBufferHelper.Close(count);
    }
}

public class CS_Skin : IPacket
{
    public int skinId;

    public ushort Protocol { get { return (ushort)PacketID.CS_Skin; } }

    public void Read(ArraySegment<byte> segment)
    {
        ushort count = 0;

        ReadOnlySpan<byte> s = new ReadOnlySpan<byte>(segment.Array, segment.Offset, segment.Count);

        // size / packetId
        count += sizeof(ushort);
        count += sizeof(ushort);

        this.skinId = BitConverter.ToInt32(s.Slice(count, s.Length - count));
		                count += sizeof(int);
    }

    public ArraySegment<byte> Write()
    {
        ArraySegment<byte> segment = SendBufferHelper.Open(4096);

        ushort count = 0;
        bool success = true;

        Span<byte> s = new Span<byte>(segment.Array, segment.Offset, segment.Count);
        count += sizeof(ushort);
        success &= BitConverter.TryWriteBytes(s.Slice(count, s.Length - count), (ushort)PacketID.CS_Skin);
        count += sizeof(ushort);

        success &= BitConverter.TryWriteBytes(s.Slice(count, s.Length - count), this.skinId);
		count += sizeof(int);
		 

        success &= BitConverter.TryWriteBytes(s, count);

        if (success == false) {
            return null;
        }

        return SendBufferHelper.Close(count);
    }
}

public class SC_MainSceneLoad : IPacket
{
    

    public ushort Protocol { get { return (ushort)PacketID.SC_MainSceneLoad; } }

    public void Read(ArraySegment<byte> segment)
    {
        ushort count = 0;

        ReadOnlySpan<byte> s = new ReadOnlySpan<byte>(segment.Array, segment.Offset, segment.Count);

        // size / packetId
        count += sizeof(ushort);
        count += sizeof(ushort);

        
    }

    public ArraySegment<byte> Write()
    {
        ArraySegment<byte> segment = SendBufferHelper.Open(4096);

        ushort count = 0;
        bool success = true;

        Span<byte> s = new Span<byte>(segment.Array, segment.Offset, segment.Count);
        count += sizeof(ushort);
        success &= BitConverter.TryWriteBytes(s.Slice(count, s.Length - count), (ushort)PacketID.SC_MainSceneLoad);
        count += sizeof(ushort);

         

        success &= BitConverter.TryWriteBytes(s, count);

        if (success == false) {
            return null;
        }

        return SendBufferHelper.Close(count);
    }
}

public class CS_Roll : IPacket
{
    

    public ushort Protocol { get { return (ushort)PacketID.CS_Roll; } }

    public void Read(ArraySegment<byte> segment)
    {
        ushort count = 0;

        ReadOnlySpan<byte> s = new ReadOnlySpan<byte>(segment.Array, segment.Offset, segment.Count);

        // size / packetId
        count += sizeof(ushort);
        count += sizeof(ushort);

        
    }

    public ArraySegment<byte> Write()
    {
        ArraySegment<byte> segment = SendBufferHelper.Open(4096);

        ushort count = 0;
        bool success = true;

        Span<byte> s = new Span<byte>(segment.Array, segment.Offset, segment.Count);
        count += sizeof(ushort);
        success &= BitConverter.TryWriteBytes(s.Slice(count, s.Length - count), (ushort)PacketID.CS_Roll);
        count += sizeof(ushort);

         

        success &= BitConverter.TryWriteBytes(s, count);

        if (success == false) {
            return null;
        }

        return SendBufferHelper.Close(count);
    }
}

public class SC_Roll : IPacket
{
    public int id;

    public ushort Protocol { get { return (ushort)PacketID.SC_Roll; } }

    public void Read(ArraySegment<byte> segment)
    {
        ushort count = 0;

        ReadOnlySpan<byte> s = new ReadOnlySpan<byte>(segment.Array, segment.Offset, segment.Count);

        // size / packetId
        count += sizeof(ushort);
        count += sizeof(ushort);

        this.id = BitConverter.ToInt32(s.Slice(count, s.Length - count));
		                count += sizeof(int);
    }

    public ArraySegment<byte> Write()
    {
        ArraySegment<byte> segment = SendBufferHelper.Open(4096);

        ushort count = 0;
        bool success = true;

        Span<byte> s = new Span<byte>(segment.Array, segment.Offset, segment.Count);
        count += sizeof(ushort);
        success &= BitConverter.TryWriteBytes(s.Slice(count, s.Length - count), (ushort)PacketID.SC_Roll);
        count += sizeof(ushort);

        success &= BitConverter.TryWriteBytes(s.Slice(count, s.Length - count), this.id);
		count += sizeof(int);
		 

        success &= BitConverter.TryWriteBytes(s, count);

        if (success == false) {
            return null;
        }

        return SendBufferHelper.Close(count);
    }
}

public class CS_PlayerAttack : IPacket
{
    

    public ushort Protocol { get { return (ushort)PacketID.CS_PlayerAttack; } }

    public void Read(ArraySegment<byte> segment)
    {
        ushort count = 0;

        ReadOnlySpan<byte> s = new ReadOnlySpan<byte>(segment.Array, segment.Offset, segment.Count);

        // size / packetId
        count += sizeof(ushort);
        count += sizeof(ushort);

        
    }

    public ArraySegment<byte> Write()
    {
        ArraySegment<byte> segment = SendBufferHelper.Open(4096);

        ushort count = 0;
        bool success = true;

        Span<byte> s = new Span<byte>(segment.Array, segment.Offset, segment.Count);
        count += sizeof(ushort);
        success &= BitConverter.TryWriteBytes(s.Slice(count, s.Length - count), (ushort)PacketID.CS_PlayerAttack);
        count += sizeof(ushort);

         

        success &= BitConverter.TryWriteBytes(s, count);

        if (success == false) {
            return null;
        }

        return SendBufferHelper.Close(count);
    }
}

public class SC_PlayerAttack : IPacket
{
    public int id;

    public ushort Protocol { get { return (ushort)PacketID.SC_PlayerAttack; } }

    public void Read(ArraySegment<byte> segment)
    {
        ushort count = 0;

        ReadOnlySpan<byte> s = new ReadOnlySpan<byte>(segment.Array, segment.Offset, segment.Count);

        // size / packetId
        count += sizeof(ushort);
        count += sizeof(ushort);

        this.id = BitConverter.ToInt32(s.Slice(count, s.Length - count));
		                count += sizeof(int);
    }

    public ArraySegment<byte> Write()
    {
        ArraySegment<byte> segment = SendBufferHelper.Open(4096);

        ushort count = 0;
        bool success = true;

        Span<byte> s = new Span<byte>(segment.Array, segment.Offset, segment.Count);
        count += sizeof(ushort);
        success &= BitConverter.TryWriteBytes(s.Slice(count, s.Length - count), (ushort)PacketID.SC_PlayerAttack);
        count += sizeof(ushort);

        success &= BitConverter.TryWriteBytes(s.Slice(count, s.Length - count), this.id);
		count += sizeof(int);
		 

        success &= BitConverter.TryWriteBytes(s, count);

        if (success == false) {
            return null;
        }

        return SendBufferHelper.Close(count);
    }
}

public class SC_EnemyCreate : IPacket
{
    public int id;
	public int etype;
	public int hp;
	public float rotate;
	public float posX;
	public float posY;
	public float posZ;

    public ushort Protocol { get { return (ushort)PacketID.SC_EnemyCreate; } }

    public void Read(ArraySegment<byte> segment)
    {
        ushort count = 0;

        ReadOnlySpan<byte> s = new ReadOnlySpan<byte>(segment.Array, segment.Offset, segment.Count);

        // size / packetId
        count += sizeof(ushort);
        count += sizeof(ushort);

        this.id = BitConverter.ToInt32(s.Slice(count, s.Length - count));
		                count += sizeof(int);
		this.etype = BitConverter.ToInt32(s.Slice(count, s.Length - count));
		                count += sizeof(int);
		this.hp = BitConverter.ToInt32(s.Slice(count, s.Length - count));
		                count += sizeof(int);
		this.rotate = BitConverter.ToSingle(s.Slice(count, s.Length - count));
		                count += sizeof(float);
		this.posX = BitConverter.ToSingle(s.Slice(count, s.Length - count));
		                count += sizeof(float);
		this.posY = BitConverter.ToSingle(s.Slice(count, s.Length - count));
		                count += sizeof(float);
		this.posZ = BitConverter.ToSingle(s.Slice(count, s.Length - count));
		                count += sizeof(float);
    }

    public ArraySegment<byte> Write()
    {
        ArraySegment<byte> segment = SendBufferHelper.Open(4096);

        ushort count = 0;
        bool success = true;

        Span<byte> s = new Span<byte>(segment.Array, segment.Offset, segment.Count);
        count += sizeof(ushort);
        success &= BitConverter.TryWriteBytes(s.Slice(count, s.Length - count), (ushort)PacketID.SC_EnemyCreate);
        count += sizeof(ushort);

        success &= BitConverter.TryWriteBytes(s.Slice(count, s.Length - count), this.id);
		count += sizeof(int);
		
		success &= BitConverter.TryWriteBytes(s.Slice(count, s.Length - count), this.etype);
		count += sizeof(int);
		
		success &= BitConverter.TryWriteBytes(s.Slice(count, s.Length - count), this.hp);
		count += sizeof(int);
		
		success &= BitConverter.TryWriteBytes(s.Slice(count, s.Length - count), this.rotate);
		count += sizeof(float);
		
		success &= BitConverter.TryWriteBytes(s.Slice(count, s.Length - count), this.posX);
		count += sizeof(float);
		
		success &= BitConverter.TryWriteBytes(s.Slice(count, s.Length - count), this.posY);
		count += sizeof(float);
		
		success &= BitConverter.TryWriteBytes(s.Slice(count, s.Length - count), this.posZ);
		count += sizeof(float);
		 

        success &= BitConverter.TryWriteBytes(s, count);

        if (success == false) {
            return null;
        }

        return SendBufferHelper.Close(count);
    }
}

public class SC_EnemyStartChase : IPacket
{
    public int id;
	public float posX;
	public float posY;
	public float posZ;
	public int target;

    public ushort Protocol { get { return (ushort)PacketID.SC_EnemyStartChase; } }

    public void Read(ArraySegment<byte> segment)
    {
        ushort count = 0;

        ReadOnlySpan<byte> s = new ReadOnlySpan<byte>(segment.Array, segment.Offset, segment.Count);

        // size / packetId
        count += sizeof(ushort);
        count += sizeof(ushort);

        this.id = BitConverter.ToInt32(s.Slice(count, s.Length - count));
		                count += sizeof(int);
		this.posX = BitConverter.ToSingle(s.Slice(count, s.Length - count));
		                count += sizeof(float);
		this.posY = BitConverter.ToSingle(s.Slice(count, s.Length - count));
		                count += sizeof(float);
		this.posZ = BitConverter.ToSingle(s.Slice(count, s.Length - count));
		                count += sizeof(float);
		this.target = BitConverter.ToInt32(s.Slice(count, s.Length - count));
		                count += sizeof(int);
    }

    public ArraySegment<byte> Write()
    {
        ArraySegment<byte> segment = SendBufferHelper.Open(4096);

        ushort count = 0;
        bool success = true;

        Span<byte> s = new Span<byte>(segment.Array, segment.Offset, segment.Count);
        count += sizeof(ushort);
        success &= BitConverter.TryWriteBytes(s.Slice(count, s.Length - count), (ushort)PacketID.SC_EnemyStartChase);
        count += sizeof(ushort);

        success &= BitConverter.TryWriteBytes(s.Slice(count, s.Length - count), this.id);
		count += sizeof(int);
		
		success &= BitConverter.TryWriteBytes(s.Slice(count, s.Length - count), this.posX);
		count += sizeof(float);
		
		success &= BitConverter.TryWriteBytes(s.Slice(count, s.Length - count), this.posY);
		count += sizeof(float);
		
		success &= BitConverter.TryWriteBytes(s.Slice(count, s.Length - count), this.posZ);
		count += sizeof(float);
		
		success &= BitConverter.TryWriteBytes(s.Slice(count, s.Length - count), this.target);
		count += sizeof(int);
		 

        success &= BitConverter.TryWriteBytes(s, count);

        if (success == false) {
            return null;
        }

        return SendBufferHelper.Close(count);
    }
}

public class SC_EnemyChase : IPacket
{
    public int id;
	public float posX;
	public float posY;
	public float posZ;
	public float targetPosX;
	public float targetposY;
	public float targetposZ;

    public ushort Protocol { get { return (ushort)PacketID.SC_EnemyChase; } }

    public void Read(ArraySegment<byte> segment)
    {
        ushort count = 0;

        ReadOnlySpan<byte> s = new ReadOnlySpan<byte>(segment.Array, segment.Offset, segment.Count);

        // size / packetId
        count += sizeof(ushort);
        count += sizeof(ushort);

        this.id = BitConverter.ToInt32(s.Slice(count, s.Length - count));
		                count += sizeof(int);
		this.posX = BitConverter.ToSingle(s.Slice(count, s.Length - count));
		                count += sizeof(float);
		this.posY = BitConverter.ToSingle(s.Slice(count, s.Length - count));
		                count += sizeof(float);
		this.posZ = BitConverter.ToSingle(s.Slice(count, s.Length - count));
		                count += sizeof(float);
		this.targetPosX = BitConverter.ToSingle(s.Slice(count, s.Length - count));
		                count += sizeof(float);
		this.targetposY = BitConverter.ToSingle(s.Slice(count, s.Length - count));
		                count += sizeof(float);
		this.targetposZ = BitConverter.ToSingle(s.Slice(count, s.Length - count));
		                count += sizeof(float);
    }

    public ArraySegment<byte> Write()
    {
        ArraySegment<byte> segment = SendBufferHelper.Open(4096);

        ushort count = 0;
        bool success = true;

        Span<byte> s = new Span<byte>(segment.Array, segment.Offset, segment.Count);
        count += sizeof(ushort);
        success &= BitConverter.TryWriteBytes(s.Slice(count, s.Length - count), (ushort)PacketID.SC_EnemyChase);
        count += sizeof(ushort);

        success &= BitConverter.TryWriteBytes(s.Slice(count, s.Length - count), this.id);
		count += sizeof(int);
		
		success &= BitConverter.TryWriteBytes(s.Slice(count, s.Length - count), this.posX);
		count += sizeof(float);
		
		success &= BitConverter.TryWriteBytes(s.Slice(count, s.Length - count), this.posY);
		count += sizeof(float);
		
		success &= BitConverter.TryWriteBytes(s.Slice(count, s.Length - count), this.posZ);
		count += sizeof(float);
		
		success &= BitConverter.TryWriteBytes(s.Slice(count, s.Length - count), this.targetPosX);
		count += sizeof(float);
		
		success &= BitConverter.TryWriteBytes(s.Slice(count, s.Length - count), this.targetposY);
		count += sizeof(float);
		
		success &= BitConverter.TryWriteBytes(s.Slice(count, s.Length - count), this.targetposZ);
		count += sizeof(float);
		 

        success &= BitConverter.TryWriteBytes(s, count);

        if (success == false) {
            return null;
        }

        return SendBufferHelper.Close(count);
    }
}

public class SC_EnemyStopChase : IPacket
{
    public int id;
	public float posX;
	public float posY;
	public float posZ;

    public ushort Protocol { get { return (ushort)PacketID.SC_EnemyStopChase; } }

    public void Read(ArraySegment<byte> segment)
    {
        ushort count = 0;

        ReadOnlySpan<byte> s = new ReadOnlySpan<byte>(segment.Array, segment.Offset, segment.Count);

        // size / packetId
        count += sizeof(ushort);
        count += sizeof(ushort);

        this.id = BitConverter.ToInt32(s.Slice(count, s.Length - count));
		                count += sizeof(int);
		this.posX = BitConverter.ToSingle(s.Slice(count, s.Length - count));
		                count += sizeof(float);
		this.posY = BitConverter.ToSingle(s.Slice(count, s.Length - count));
		                count += sizeof(float);
		this.posZ = BitConverter.ToSingle(s.Slice(count, s.Length - count));
		                count += sizeof(float);
    }

    public ArraySegment<byte> Write()
    {
        ArraySegment<byte> segment = SendBufferHelper.Open(4096);

        ushort count = 0;
        bool success = true;

        Span<byte> s = new Span<byte>(segment.Array, segment.Offset, segment.Count);
        count += sizeof(ushort);
        success &= BitConverter.TryWriteBytes(s.Slice(count, s.Length - count), (ushort)PacketID.SC_EnemyStopChase);
        count += sizeof(ushort);

        success &= BitConverter.TryWriteBytes(s.Slice(count, s.Length - count), this.id);
		count += sizeof(int);
		
		success &= BitConverter.TryWriteBytes(s.Slice(count, s.Length - count), this.posX);
		count += sizeof(float);
		
		success &= BitConverter.TryWriteBytes(s.Slice(count, s.Length - count), this.posY);
		count += sizeof(float);
		
		success &= BitConverter.TryWriteBytes(s.Slice(count, s.Length - count), this.posZ);
		count += sizeof(float);
		 

        success &= BitConverter.TryWriteBytes(s, count);

        if (success == false) {
            return null;
        }

        return SendBufferHelper.Close(count);
    }
}

public class SC_EnemyAttack : IPacket
{
    public int id;
	public int target;

    public ushort Protocol { get { return (ushort)PacketID.SC_EnemyAttack; } }

    public void Read(ArraySegment<byte> segment)
    {
        ushort count = 0;

        ReadOnlySpan<byte> s = new ReadOnlySpan<byte>(segment.Array, segment.Offset, segment.Count);

        // size / packetId
        count += sizeof(ushort);
        count += sizeof(ushort);

        this.id = BitConverter.ToInt32(s.Slice(count, s.Length - count));
		                count += sizeof(int);
		this.target = BitConverter.ToInt32(s.Slice(count, s.Length - count));
		                count += sizeof(int);
    }

    public ArraySegment<byte> Write()
    {
        ArraySegment<byte> segment = SendBufferHelper.Open(4096);

        ushort count = 0;
        bool success = true;

        Span<byte> s = new Span<byte>(segment.Array, segment.Offset, segment.Count);
        count += sizeof(ushort);
        success &= BitConverter.TryWriteBytes(s.Slice(count, s.Length - count), (ushort)PacketID.SC_EnemyAttack);
        count += sizeof(ushort);

        success &= BitConverter.TryWriteBytes(s.Slice(count, s.Length - count), this.id);
		count += sizeof(int);
		
		success &= BitConverter.TryWriteBytes(s.Slice(count, s.Length - count), this.target);
		count += sizeof(int);
		 

        success &= BitConverter.TryWriteBytes(s, count);

        if (success == false) {
            return null;
        }

        return SendBufferHelper.Close(count);
    }
}

public class CS_EnemyHit : IPacket
{
    public int pid;
	public int eid;

    public ushort Protocol { get { return (ushort)PacketID.CS_EnemyHit; } }

    public void Read(ArraySegment<byte> segment)
    {
        ushort count = 0;

        ReadOnlySpan<byte> s = new ReadOnlySpan<byte>(segment.Array, segment.Offset, segment.Count);

        // size / packetId
        count += sizeof(ushort);
        count += sizeof(ushort);

        this.pid = BitConverter.ToInt32(s.Slice(count, s.Length - count));
		                count += sizeof(int);
		this.eid = BitConverter.ToInt32(s.Slice(count, s.Length - count));
		                count += sizeof(int);
    }

    public ArraySegment<byte> Write()
    {
        ArraySegment<byte> segment = SendBufferHelper.Open(4096);

        ushort count = 0;
        bool success = true;

        Span<byte> s = new Span<byte>(segment.Array, segment.Offset, segment.Count);
        count += sizeof(ushort);
        success &= BitConverter.TryWriteBytes(s.Slice(count, s.Length - count), (ushort)PacketID.CS_EnemyHit);
        count += sizeof(ushort);

        success &= BitConverter.TryWriteBytes(s.Slice(count, s.Length - count), this.pid);
		count += sizeof(int);
		
		success &= BitConverter.TryWriteBytes(s.Slice(count, s.Length - count), this.eid);
		count += sizeof(int);
		 

        success &= BitConverter.TryWriteBytes(s, count);

        if (success == false) {
            return null;
        }

        return SendBufferHelper.Close(count);
    }
}

public class SC_EnemyHit : IPacket
{
    public int id;
	public int hp;

    public ushort Protocol { get { return (ushort)PacketID.SC_EnemyHit; } }

    public void Read(ArraySegment<byte> segment)
    {
        ushort count = 0;

        ReadOnlySpan<byte> s = new ReadOnlySpan<byte>(segment.Array, segment.Offset, segment.Count);

        // size / packetId
        count += sizeof(ushort);
        count += sizeof(ushort);

        this.id = BitConverter.ToInt32(s.Slice(count, s.Length - count));
		                count += sizeof(int);
		this.hp = BitConverter.ToInt32(s.Slice(count, s.Length - count));
		                count += sizeof(int);
    }

    public ArraySegment<byte> Write()
    {
        ArraySegment<byte> segment = SendBufferHelper.Open(4096);

        ushort count = 0;
        bool success = true;

        Span<byte> s = new Span<byte>(segment.Array, segment.Offset, segment.Count);
        count += sizeof(ushort);
        success &= BitConverter.TryWriteBytes(s.Slice(count, s.Length - count), (ushort)PacketID.SC_EnemyHit);
        count += sizeof(ushort);

        success &= BitConverter.TryWriteBytes(s.Slice(count, s.Length - count), this.id);
		count += sizeof(int);
		
		success &= BitConverter.TryWriteBytes(s.Slice(count, s.Length - count), this.hp);
		count += sizeof(int);
		 

        success &= BitConverter.TryWriteBytes(s, count);

        if (success == false) {
            return null;
        }

        return SendBufferHelper.Close(count);
    }
}

public class SC_EnemyDie : IPacket
{
    public int id;
	public int hp;

    public ushort Protocol { get { return (ushort)PacketID.SC_EnemyDie; } }

    public void Read(ArraySegment<byte> segment)
    {
        ushort count = 0;

        ReadOnlySpan<byte> s = new ReadOnlySpan<byte>(segment.Array, segment.Offset, segment.Count);

        // size / packetId
        count += sizeof(ushort);
        count += sizeof(ushort);

        this.id = BitConverter.ToInt32(s.Slice(count, s.Length - count));
		                count += sizeof(int);
		this.hp = BitConverter.ToInt32(s.Slice(count, s.Length - count));
		                count += sizeof(int);
    }

    public ArraySegment<byte> Write()
    {
        ArraySegment<byte> segment = SendBufferHelper.Open(4096);

        ushort count = 0;
        bool success = true;

        Span<byte> s = new Span<byte>(segment.Array, segment.Offset, segment.Count);
        count += sizeof(ushort);
        success &= BitConverter.TryWriteBytes(s.Slice(count, s.Length - count), (ushort)PacketID.SC_EnemyDie);
        count += sizeof(ushort);

        success &= BitConverter.TryWriteBytes(s.Slice(count, s.Length - count), this.id);
		count += sizeof(int);
		
		success &= BitConverter.TryWriteBytes(s.Slice(count, s.Length - count), this.hp);
		count += sizeof(int);
		 

        success &= BitConverter.TryWriteBytes(s, count);

        if (success == false) {
            return null;
        }

        return SendBufferHelper.Close(count);
    }
}

public class CS_PlayerHit : IPacket
{
    public int eid;

    public ushort Protocol { get { return (ushort)PacketID.CS_PlayerHit; } }

    public void Read(ArraySegment<byte> segment)
    {
        ushort count = 0;

        ReadOnlySpan<byte> s = new ReadOnlySpan<byte>(segment.Array, segment.Offset, segment.Count);

        // size / packetId
        count += sizeof(ushort);
        count += sizeof(ushort);

        this.eid = BitConverter.ToInt32(s.Slice(count, s.Length - count));
		                count += sizeof(int);
    }

    public ArraySegment<byte> Write()
    {
        ArraySegment<byte> segment = SendBufferHelper.Open(4096);

        ushort count = 0;
        bool success = true;

        Span<byte> s = new Span<byte>(segment.Array, segment.Offset, segment.Count);
        count += sizeof(ushort);
        success &= BitConverter.TryWriteBytes(s.Slice(count, s.Length - count), (ushort)PacketID.CS_PlayerHit);
        count += sizeof(ushort);

        success &= BitConverter.TryWriteBytes(s.Slice(count, s.Length - count), this.eid);
		count += sizeof(int);
		 

        success &= BitConverter.TryWriteBytes(s, count);

        if (success == false) {
            return null;
        }

        return SendBufferHelper.Close(count);
    }
}

public class SC_PlayerHit : IPacket
{
    public int id;
	public int hp;

    public ushort Protocol { get { return (ushort)PacketID.SC_PlayerHit; } }

    public void Read(ArraySegment<byte> segment)
    {
        ushort count = 0;

        ReadOnlySpan<byte> s = new ReadOnlySpan<byte>(segment.Array, segment.Offset, segment.Count);

        // size / packetId
        count += sizeof(ushort);
        count += sizeof(ushort);

        this.id = BitConverter.ToInt32(s.Slice(count, s.Length - count));
		                count += sizeof(int);
		this.hp = BitConverter.ToInt32(s.Slice(count, s.Length - count));
		                count += sizeof(int);
    }

    public ArraySegment<byte> Write()
    {
        ArraySegment<byte> segment = SendBufferHelper.Open(4096);

        ushort count = 0;
        bool success = true;

        Span<byte> s = new Span<byte>(segment.Array, segment.Offset, segment.Count);
        count += sizeof(ushort);
        success &= BitConverter.TryWriteBytes(s.Slice(count, s.Length - count), (ushort)PacketID.SC_PlayerHit);
        count += sizeof(ushort);

        success &= BitConverter.TryWriteBytes(s.Slice(count, s.Length - count), this.id);
		count += sizeof(int);
		
		success &= BitConverter.TryWriteBytes(s.Slice(count, s.Length - count), this.hp);
		count += sizeof(int);
		 

        success &= BitConverter.TryWriteBytes(s, count);

        if (success == false) {
            return null;
        }

        return SendBufferHelper.Close(count);
    }
}

public class SC_BossCreate : IPacket
{
    public int id;
	public int btype;
	public int hp;
	public float rotate;
	public float posX;
	public float posY;
	public float posZ;

    public ushort Protocol { get { return (ushort)PacketID.SC_BossCreate; } }

    public void Read(ArraySegment<byte> segment)
    {
        ushort count = 0;

        ReadOnlySpan<byte> s = new ReadOnlySpan<byte>(segment.Array, segment.Offset, segment.Count);

        // size / packetId
        count += sizeof(ushort);
        count += sizeof(ushort);

        this.id = BitConverter.ToInt32(s.Slice(count, s.Length - count));
		                count += sizeof(int);
		this.btype = BitConverter.ToInt32(s.Slice(count, s.Length - count));
		                count += sizeof(int);
		this.hp = BitConverter.ToInt32(s.Slice(count, s.Length - count));
		                count += sizeof(int);
		this.rotate = BitConverter.ToSingle(s.Slice(count, s.Length - count));
		                count += sizeof(float);
		this.posX = BitConverter.ToSingle(s.Slice(count, s.Length - count));
		                count += sizeof(float);
		this.posY = BitConverter.ToSingle(s.Slice(count, s.Length - count));
		                count += sizeof(float);
		this.posZ = BitConverter.ToSingle(s.Slice(count, s.Length - count));
		                count += sizeof(float);
    }

    public ArraySegment<byte> Write()
    {
        ArraySegment<byte> segment = SendBufferHelper.Open(4096);

        ushort count = 0;
        bool success = true;

        Span<byte> s = new Span<byte>(segment.Array, segment.Offset, segment.Count);
        count += sizeof(ushort);
        success &= BitConverter.TryWriteBytes(s.Slice(count, s.Length - count), (ushort)PacketID.SC_BossCreate);
        count += sizeof(ushort);

        success &= BitConverter.TryWriteBytes(s.Slice(count, s.Length - count), this.id);
		count += sizeof(int);
		
		success &= BitConverter.TryWriteBytes(s.Slice(count, s.Length - count), this.btype);
		count += sizeof(int);
		
		success &= BitConverter.TryWriteBytes(s.Slice(count, s.Length - count), this.hp);
		count += sizeof(int);
		
		success &= BitConverter.TryWriteBytes(s.Slice(count, s.Length - count), this.rotate);
		count += sizeof(float);
		
		success &= BitConverter.TryWriteBytes(s.Slice(count, s.Length - count), this.posX);
		count += sizeof(float);
		
		success &= BitConverter.TryWriteBytes(s.Slice(count, s.Length - count), this.posY);
		count += sizeof(float);
		
		success &= BitConverter.TryWriteBytes(s.Slice(count, s.Length - count), this.posZ);
		count += sizeof(float);
		 

        success &= BitConverter.TryWriteBytes(s, count);

        if (success == false) {
            return null;
        }

        return SendBufferHelper.Close(count);
    }
}

public class SC_BossAttack : IPacket
{
    public int id;
	public int pattern;

    public ushort Protocol { get { return (ushort)PacketID.SC_BossAttack; } }

    public void Read(ArraySegment<byte> segment)
    {
        ushort count = 0;

        ReadOnlySpan<byte> s = new ReadOnlySpan<byte>(segment.Array, segment.Offset, segment.Count);

        // size / packetId
        count += sizeof(ushort);
        count += sizeof(ushort);

        this.id = BitConverter.ToInt32(s.Slice(count, s.Length - count));
		                count += sizeof(int);
		this.pattern = BitConverter.ToInt32(s.Slice(count, s.Length - count));
		                count += sizeof(int);
    }

    public ArraySegment<byte> Write()
    {
        ArraySegment<byte> segment = SendBufferHelper.Open(4096);

        ushort count = 0;
        bool success = true;

        Span<byte> s = new Span<byte>(segment.Array, segment.Offset, segment.Count);
        count += sizeof(ushort);
        success &= BitConverter.TryWriteBytes(s.Slice(count, s.Length - count), (ushort)PacketID.SC_BossAttack);
        count += sizeof(ushort);

        success &= BitConverter.TryWriteBytes(s.Slice(count, s.Length - count), this.id);
		count += sizeof(int);
		
		success &= BitConverter.TryWriteBytes(s.Slice(count, s.Length - count), this.pattern);
		count += sizeof(int);
		 

        success &= BitConverter.TryWriteBytes(s, count);

        if (success == false) {
            return null;
        }

        return SendBufferHelper.Close(count);
    }
}

