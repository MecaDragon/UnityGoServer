START ./GoPacketGenerator.exe ./PDL.xml
XCOPY /Y GenPackets.go "../UnitySocketServer"
XCOPY /Y HandlePacket.go "../UnitySocketServer"

START ./ClientPacketGenerator.exe ./PDL.xml
XCOPY /Y GenPackets.cs "../UnitySocketClient/Assets/Script/Packet"
XCOPY /Y ClientPacketManager.cs "../UnitySocketClient/Assets/Script/Packet"