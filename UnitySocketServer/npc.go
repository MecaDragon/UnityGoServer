package main

import (
	"sync"
	"time"

	"github.com/deeean/go-vector/vector2"
)

type Npc struct {
	id    int32
	state int32
	x     float32
	y     float32
	z     float32
	lookx float32
	looky float32
	lookz float32
}

type Enemy struct {
	id     int32
	etype  int32 // 몬스터 종류
	rotate float32
	x      float32
	y      float32
	z      float32
	rate   float32
	hp     int32
	mutex  sync.Mutex
	target *Player // 타켓팅한 플레이어
}

var enemys map[int32]*Enemy

var EnemyTime float32

var bosses map[int32]*Boss

type Boss struct {
	id     int32
	btype  int32
	rotate float32
	x      float32
	y      float32
	z      float32
	rate   float32
	hp     int32
	mutex  sync.Mutex
	target *Player // 타켓팅한 플레이어
}

func BossInit() {
	bosses = make(map[int32]*Boss)
	b1 := new(Boss)
	b1.id = 0
	b1.btype = 1
	b1.x = -145.45
	b1.y = 0
	b1.z = -23.63
	b1.rotate = 0
	b1.hp = 100
	bosses[0] = b1

	b2 := new(Boss)
	b2.id = 1
	b2.btype = 0
	b2.x = -146.54
	b2.y = 0
	b2.z = 0.84
	b2.rotate = 180
	b2.hp = 100
	bosses[1] = b2

}

func (boss *Boss) attack() {
	boss.rate -= EnemyTime
	if boss.rate <= 0 {
		boss.rate = 8
		packet := SC_BossAttackPacket{id: boss.id, pattern: 0}
		event := Event{Type: E_BossAttack, data: packet}
		LogicChan <- event
	}
}

// 타켓팅할 플레이어 찾음
func (enemy *Enemy) seek() {
	if sessions == nil {
		return
	}

	if enemy.target == nil {
		for _, session := range sessions {
			player := session.player
			if session.player.state == S_LOGIN {
				// dis := distance(player.x, player.z, enemy.x, enemy.z)
				pos := vector2.New(float64(enemy.x), float64(enemy.z))
				target := vector2.New(float64(player.x), float64(player.z))
				dis := pos.Distance(target)
				if dis < 5 {
					enemy.target = player
					packet := SC_EnemyStartChasePacket{id: enemy.id, posX: enemy.x, posY: enemy.y, posZ: enemy.z, target: player.Pid}
					event := Event{Type: E_EnemyStartChase, data: packet}
					LogicChan <- event
					return
				}
			}
		}
	}
	// idle 상태 or 아무데나 이동 TODO
}

func (enemy *Enemy) chase() {
	if enemy.target != nil {
		session, exists := sessions[enemy.target.Pid]
		if !exists || session.player.state == S_Death {
			enemy.target = nil

			packet := SC_EnemyStopChasePacket{id: enemy.id, posX: enemy.x, posY: enemy.y, posZ: enemy.z}
			event := Event{Type: E_EnemyStopChase, data: packet}
			LogicChan <- event
			return
		}

		pos := vector2.New(float64(enemy.x), float64(enemy.z))
		target := vector2.New(float64(enemy.target.x), float64(enemy.target.z))

		dis := pos.Distance(target)
		// 거리가 가까우면 공격
		if dis < 1 {
			enemy.rate -= EnemyTime
			if enemy.rate <= 0 {
				packet := SC_EnemyAttackPacket{id: enemy.id, target: enemy.target.Pid}
				event := Event{Type: E_EnemyAttack, data: packet}
				LogicChan <- event
				enemy.rate = 2
			}
			return
		}

		look := target.Sub(pos).Normalize()

		newpos := pos.Add(look.MulScalars(0.1, 0.1))
		enemy.x = float32(newpos.X)
		enemy.z = float32(newpos.Y)

		packet := SC_EnemyChasePacket{id: enemy.id, posX: enemy.x, posY: enemy.y, posZ: enemy.z, targetPosX: enemy.target.x,
			targetposY: enemy.target.y, targetposZ: enemy.target.z}
		event := Event{Type: E_EnemyChase, data: packet}
		LogicChan <- event
	}
}

func (enemy *Enemy) attack() {
	packet := SC_EnemyAttackPacket{id: enemy.id, target: enemy.target.Pid}
	event := Event{Type: E_EnemyAttack, data: packet}
	LogicChan <- event
}

func NPCLogic(npc Npc) {
	for {
		npc.move()
		time.Sleep(time.Second / 4)
	}
}

func EnemyInit() {
	enemys = make(map[int32]*Enemy)

	e0 := new(Enemy)
	e0.id = 0
	e0.etype = 1
	e0.rotate = -58.49
	e0.x = -143.41
	e0.y = 0
	e0.z = -24.85
	e0.hp = 100
	e0.mutex = sync.Mutex{}
	enemys[0] = e0

	e1 := new(Enemy)
	e1.id = 1
	e1.etype = 1
	e1.rotate = -128.8
	e1.x = -143.1754
	e1.y = 0
	e1.z = -21.86382
	e1.hp = 100
	e1.mutex = sync.Mutex{}
	enemys[1] = e1

	e2 := new(Enemy)
	e2.id = 2
	e2.etype = 1
	e2.rotate = -223.59
	e2.x = -146.79
	e2.y = 0
	e2.z = -21.9
	e2.hp = 100
	e2.mutex = sync.Mutex{}
	enemys[2] = e2

	e3 := new(Enemy)
	e3.id = 3
	e3.etype = 0
	e3.rotate = 75.7
	e3.x = -128.69
	e3.y = 0
	e3.z = 3.22
	e3.hp = 100
	e3.mutex = sync.Mutex{}
	enemys[3] = e3

	e4 := new(Enemy)
	e4.id = 4
	e4.etype = 0
	e4.rotate = -6.28
	e4.x = -126.19
	e4.y = 0
	e4.z = 2.61
	e4.hp = 100
	e4.mutex = sync.Mutex{}
	enemys[4] = e4

	e5 := new(Enemy)
	e5.id = 5
	e5.etype = 0
	e5.rotate = -71
	e5.x = -124.2
	e5.y = 0
	e5.z = 5.03
	e5.hp = 100
	e5.mutex = sync.Mutex{}
	enemys[5] = e5

	e6 := new(Enemy)
	e6.id = 6
	e6.etype = 0
	e6.rotate = 195.4
	e6.x = -125.41
	e6.y = 0
	e6.z = 7.76
	e6.hp = 100
	e6.mutex = sync.Mutex{}
	enemys[6] = e6

	e7 := new(Enemy)
	e7.id = 7
	e7.etype = 0
	e7.rotate = 135.72
	e7.x = -128.13
	e7.y = 0
	e7.z = 6.87
	e7.hp = 100
	e7.mutex = sync.Mutex{}
	enemys[7] = e7

	e8 := new(Enemy)
	e8.id = 8
	e8.etype = 2
	e8.rotate = -142.3
	e8.x = -109.57
	e8.y = 0
	e8.z = -18.89
	e8.hp = 100
	e8.mutex = sync.Mutex{}
	enemys[8] = e8

	e9 := new(Enemy)
	e9.id = 9
	e9.etype = 2
	e9.rotate = -96.35
	e9.x = -107.67
	e9.y = 0
	e9.z = -20.88
	e9.hp = 100
	e9.mutex = sync.Mutex{}
	enemys[9] = e9
}

func EnemyLogic() {
	for {
		start := time.Now()
		for _, enemy := range enemys {
			enemy.mutex.Lock()
			_, exists := enemys[enemy.id]
			if exists == false {
				enemy.mutex.Unlock()
				continue
			}
			if enemy.target == nil {
				enemy.seek()
			} else {
				enemy.chase()
			}
			enemy.mutex.Unlock()
		}

		for _, boss := range bosses {
			boss.attack()
		}

		time.Sleep(time.Second / 30)
		t := time.Now()
		EnemyTime = float32(t.Sub(start).Seconds())
	}
}

func (npc *Npc) move() {
	/*
		d := rand.Intn(4)
		switch d {
		case 0:
			npc.x += 0.2
		case 1:
			npc.x -= 0.2
		case 2:
			npc.z += 0.2
		case 3:
			npc.z -= 0.2
		}
	*/

	npc.z += 0.2

	recvData := SC_NPCMovePacket{id: 0, posX: npc.x, posY: npc.y, posZ: npc.z}
	event := Event{Type: E_NpcMove, data: recvData}
	LogicChan <- event
}
