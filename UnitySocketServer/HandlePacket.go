package main

var PacketHandler map[int16]func(Session, []byte)

func HandlePacket_Init() {
	PacketHandler = make(map[int16]func(Session, []byte))

	PacketHandler[CS_LeaveGame] = CS_LeaveGameHandler
	PacketHandler[CS_Move] = CS_MoveHandler
	PacketHandler[CS_Chat] = CS_ChatHandler
	PacketHandler[CS_Login] = CS_LoginHandler
	PacketHandler[CS_MainSceneLoad] = CS_MainSceneLoadHandler
	PacketHandler[CS_Skin] = CS_SkinHandler
	PacketHandler[CS_Roll] = CS_RollHandler
	PacketHandler[CS_PlayerAttack] = CS_PlayerAttackHandler
	PacketHandler[CS_EnemyHit] = CS_EnemyHitHandler
	PacketHandler[CS_PlayerHit] = CS_PlayerHitHandler
	
}
