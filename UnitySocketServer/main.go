package main

import (
	"bytes"
	"encoding/binary"
	"io"
	"log"
	"net"

	"github.com/mitchellh/mapstructure"
)

type Session struct {
	Conn     net.Conn
	SendChan chan []byte
	player   *Player
}

type Player struct {
	Pid    int32
	state  int32
	x      float32
	y      float32
	z      float32
	lookx  float32
	looky  float32
	lookz  float32
	skinId int32
	isWalk bool
	isRun  bool
	isRoll bool
	name   string
}

type Event struct {
	Type    int16
	session Session
	data    interface{}
}

// 플레이어 상태
const (
	S_LEAVE = 0 // 나감
	S_CON   = 1 // 접속
	S_LOGIN = 2 // 로그인 성공(메인 씬 까지 이동완료)
	S_Death = 3 // 죽었을 때
)

// 이벤트 목록
const (
	E_EnterPlayer      = 1
	E_LeavePlayer      = 2
	E_MovePlayer       = 3
	E_ChatPlayer       = 4
	E_NpcMove          = 5
	E_ChangeMainScene  = 6
	E_RollPlayer       = 7
	E_AttackPlayer     = 8
	E_BroadEnterPlayer = 9
	E_HitPlayer        = 10

	E_EnemyStartChase = 20
	E_EnemyChase      = 21
	E_EnemyStopChase  = 22
	E_EnemyAttack     = 23
	E_EnemyHit        = 24
	E_EnemyDie        = 25

	E_BossCreate = 30
	E_BossAttack = 31
)

// 세션모음
var sessions map[int32]Session

// 게임로직 채널
var LogicChan chan Event

func main() {
	// 소켓 열기
	l, err := net.Listen("tcp", ":8000")
	if nil != err {
		log.Println(err)
	}
	// 소켓 닫기
	// defer는 지금 위치한 함수가 종료되기 직전에 실행한다.
	// 즉 main이 끝나기 직전에 소켓을 닫아준다는 뜻이다.
	defer l.Close()

	// 게임로직 채널과 고루틴
	LogicChan = make(chan Event, 100)
	go GameLogic()

	// NPC 로직
	var npc Npc
	npc.id = 0
	npc.x = 0
	npc.y = 0
	npc.z = 0
	go NPCLogic(npc)

	EnemyInit()
	BossInit()
	go EnemyLogic()

	// 세션 만들기
	sessions = make(map[int32]Session)

	// 패킷 핸들러 세팅
	HandlePacket_Init()

	var playerid int32
	playerid = 0
	// 무한 루프
	for {
		// 플레이어 받고 소켓 생성
		conn, err := l.Accept()
		if nil != err {
			log.Println(err)
			continue
		}
		defer conn.Close()

		playerid++

		ch := make(chan []byte, 100)

		player := new(Player)
		player.Pid = playerid
		player.x = -127.5
		player.y = 0
		player.z = -30.2
		// 접속한 플레이어 세션 생성
		sessions[playerid] = Session{Conn: conn, SendChan: ch, player: player}

		// 플레이어 소켓마다 Send, Recv 고루틴 생성
		go RecvHandler(sessions[playerid])
		go SendHandler(sessions[playerid])

		// 로그인 처리
		//event := Event{Type: E_EnterPlayer, session: sessions[playerid]}
		//LogicChan <- event
	}

}

// 브로드캐스트
func broadcast(packet []byte) {
	// 모든 세션을 돌면서 채널에 패킷 보냄
	for _, val := range sessions {
		if val.player.state == S_LOGIN {
			val.SendChan <- packet
		}
	}
}

// 유니캐스트
func unicast(session Session, packet []byte) {
	session.SendChan <- packet
}

func login(session Session, name string) {
	for _, oldplayer := range sessions {
		if name == oldplayer.player.name {
			// 로그인 실패 처리
			result := SC_LoginPacket{
				suceess: false,
				reason:  "이미 접속중인 중복된 이름이 존재합니다.",
			}
			unicast(session, result.write())
			return
		}
	}
	// 로그인 성공 처리
	result := SC_LoginPacket{
		suceess: true,
		reason:  "성공적으로 로그인이 되었습니다.",
	}

	session.player.name = name
	unicast(session, result.write())
}

func mainSeceneChange(session Session) {
	packet := SC_MainSceneLoadPacket{}
	unicast(session, packet.write())
}

func mainSeceneLoad(session Session) {
	// NPC 정보 전달
	npcPakcet := SC_NPCCreatePacket{
		id: 0,
	}
	unicast(session, npcPakcet.write())

	for _, enemy := range enemys {
		enemyPakcet := SC_EnemyCreatePacket{
			id: enemy.id, rotate: enemy.rotate, etype: enemy.etype, posX: enemy.x, posY: enemy.y, posZ: enemy.z, hp: enemy.hp,
		}
		unicast(session, enemyPakcet.write())
	}

	for _, boss := range bosses {
		bossPacket := SC_BossCreatePacket{
			id: boss.id, btype: boss.btype, posX: boss.x, posY: boss.y, posZ: boss.z, hp: boss.hp,
		}
		unicast(session, bossPacket.write())
	}

	// 플레이어에게 접속중인 플레이어 정보 전달
	for _, oldplayer := range sessions {
		if oldplayer.player.Pid == session.player.Pid {
			// 자기 자신
			oldplayerpacket := SC_LoginPlayerPacket{
				playerId: oldplayer.player.Pid,
				isSelf:   true,
				posX:     oldplayer.player.x,
				posY:     oldplayer.player.y,
				posZ:     oldplayer.player.z,
				skin:     oldplayer.player.skinId,
				name:     oldplayer.player.name,
			}
			unicast(session, oldplayerpacket.write())
		} else {
			if oldplayer.player.state == S_LOGIN {
				// 다른 플레이어
				oldplayerpacket := SC_LoginPlayerPacket{
					playerId: oldplayer.player.Pid,
					isSelf:   false,
					posX:     oldplayer.player.x,
					posY:     oldplayer.player.y,
					posZ:     oldplayer.player.z,
					lookX:    oldplayer.player.lookx,
					lookY:    oldplayer.player.looky,
					lookZ:    oldplayer.player.lookz,
					isRun:    oldplayer.player.isRun,
					isWalk:   oldplayer.player.isWalk,
					isRoll:   oldplayer.player.isRoll,
					skin:     oldplayer.player.skinId,
					name:     oldplayer.player.name,
				}
				unicast(session, oldplayerpacket.write())
			}
		}
	}

	// 모든 플레이어에게 접속한 것을 알림
	newplayerpacket := SC_BroadcastEnterGamePacket{
		playerId: session.player.Pid,
		posX:     session.player.x,
		posY:     session.player.y,
		posZ:     session.player.z,
		skin:     session.player.skinId,
		name:     session.player.name,
	}
	broadcast(newplayerpacket.write())

	session.player.state = S_LOGIN
}

func leave(session Session) {
	// 모든 플레이어에게 접속 종료한 것을 알림
	delete(sessions, session.player.Pid)
	leavepacket := SC_BroadcastLeaveGamePacket{playerId: session.player.Pid}
	broadcast(leavepacket.write())
}

// 플레이어 이동 처리
func move(player *Player, packet CS_MovePacket) {
	player.x = packet.posX
	player.y = packet.posY
	player.z = packet.posZ
	player.lookx = packet.lookX
	player.looky = packet.lookY
	player.lookz = packet.lookZ
	player.isRun = packet.isRun
	player.isWalk = packet.isWalk
	player.isRoll = packet.isRoll

	sendpacket := SC_BroadcastMovePacket{
		playerId: player.Pid, posX: player.x, posY: player.y, posZ: player.z,
		lookX: packet.lookX, lookY: packet.lookY, lookZ: packet.lookZ,
		isRun: packet.isRun, isWalk: packet.isWalk, isRoll: packet.isRoll,
	}

	broadcast(sendpacket.write())
}

// Send 고루틴
func SendHandler(session Session) {
	// 채널에서 받는 게 있다면 소켓에 Send 해준다.
	for {
		select {
		case sendData := <-session.SendChan:
			_, err := session.Conn.Write(sendData)
			if nil != err {
				event := Event{Type: E_LeavePlayer, session: session}
				LogicChan <- event
			}
		}
	}
}

// Recv 고루틴
func RecvHandler(session Session) {
	headerSize := 2
	bufferMaxSize := 4096
	head := 0

	// recvBuf (여기서 한번 할당하고 링버퍼로 계속 사용할 것이다)
	recvBuf := make([]byte, bufferMaxSize)
	// recvBuf에 넣기 전에 임시적으로 사용할 버퍼이다.(recv에서 크기 얼마나 왔는 지 확인용)
	preBuf := make([]byte, 1024)
	for {
		n, err := session.Conn.Read(preBuf)
		if head+n > bufferMaxSize {
			// 최대 버퍼를 넘을 때
			copy(recvBuf[head:bufferMaxSize], preBuf[:(bufferMaxSize-head)])
			copy(recvBuf[0:head+n-bufferMaxSize], preBuf[(bufferMaxSize-head):n])
		} else {
			// 받은 크기 만큼 가져오기
			copy(recvBuf[head:n+head], preBuf)
		}

		/*
			log.Println(recvBuf)
			headText := "                     ↑"
			for i := 0; i < head; i++ {
				headText = "  " + headText
			}
			fmt.Printf("%s \n", headText)
			fmt.Printf("read size: %d \n", n)
		*/
		if nil != err {
			// 로그아웃 처리
			event := Event{Type: E_LeavePlayer, session: session}
			LogicChan <- event

			if io.EOF == err {
				log.Println(err)
				return
			}
			log.Println(err)
			return
		}

		// Recv에서 받은게 있다면
		// headerSize 보다는 커야한다.(size(1byte) + type(1byte))
		if headerSize < n {
			var data []byte
			if head+n > bufferMaxSize {
				// 최대 버퍼를 넘을 때 맨 뒤 남은 버퍼 + 넘어간 맨 앞 버퍼 합치기
				tmp := recvBuf[head:bufferMaxSize]
				data = append(tmp, recvBuf[:head+n-bufferMaxSize]...)
			} else {
				// 받은 크기 만큼 가져오기
				data = recvBuf[head : head+n]
			}

			// 무슨 프로토콜인지 헤더를 까보자
			readbuf := bytes.NewBuffer(data)
			var size int16
			var header int16
			binary.Read(readbuf, binary.LittleEndian, &size)
			binary.Read(readbuf, binary.LittleEndian, &header)

			packetsize := int(size)

			// size 만큼 안 왔다 = 패킷이 다 오지 않았다.
			if n < packetsize {
				continue
			}

			// 해당 프로토콜에 맞게 처리해주기
			packetHandle, ok := PacketHandler[header]
			if ok == true {
				packetHandle(session, data)
			}

			if head+packetsize == bufferMaxSize {
				// 헤드가 맨 끝에 있으면 맨 앞으로 옮겨주자
				head = 0
			} else if head+packetsize > bufferMaxSize {
				// 헤드가 초과하면 맨 앞에서  초과한 만큼옮겨주자
				head = head + packetsize - bufferMaxSize
			} else {
				head = head + packetsize
			}
		}
	}
}

// 게임로직 고루틴
func GameLogic() {
	for {
		select {
		case event := <-LogicChan:
			player := event.session.player
			switch event.Type {
			case E_EnterPlayer:
				packet := CS_LoginPacket{}
				mapstructure.Decode(event.data, &packet)
				login(event.session, packet.name)
			case E_LeavePlayer:
				leave(event.session)
			case E_MovePlayer:
				packet := CS_MovePacket{}
				mapstructure.Decode(event.data, &packet)
				move(player, packet)
			case E_ChatPlayer:
				packet := CS_ChatPacket{}
				mapstructure.Decode(event.data, &packet)
				sendpacket := SC_ChatPacket{chat: packet.chat, playerId: player.Pid}
				broadcast(sendpacket.write())
			case E_NpcMove:
				packet := SC_NPCMovePacket{}
				mapstructure.Decode(event.data, &packet)
				broadcast(packet.write())
			case E_ChangeMainScene:
				mainSeceneChange(event.session)
			case E_RollPlayer:
				packet := SC_RollPacket{id: player.Pid}
				broadcast(packet.write())
			case E_AttackPlayer:
				packet := SC_PlayerAttackPacket{id: player.Pid}
				broadcast(packet.write())
			case E_BroadEnterPlayer:
				mainSeceneLoad(event.session)
			case E_EnemyStartChase:
				packet := SC_EnemyStartChasePacket{}
				mapstructure.Decode(event.data, &packet)
				broadcast(packet.write())
			case E_EnemyChase:
				packet := SC_EnemyChasePacket{}
				mapstructure.Decode(event.data, &packet)
				broadcast(packet.write())
			case E_EnemyStopChase:
				packet := SC_EnemyStopChasePacket{}
				mapstructure.Decode(event.data, &packet)
				broadcast(packet.write())
			case E_EnemyAttack:
				packet := SC_EnemyAttackPacket{}
				mapstructure.Decode(event.data, &packet)
				broadcast(packet.write())
			case E_EnemyHit:
				packet := SC_EnemyHitPacket{}
				mapstructure.Decode(event.data, &packet)
				broadcast(packet.write())
			case E_EnemyDie:
				packet := SC_EnemyDiePacket{}
				mapstructure.Decode(event.data, &packet)
				broadcast(packet.write())
			case E_HitPlayer:
				packet := SC_PlayerHitPacket{id: player.Pid, hp: 100}
				broadcast(packet.write())
			case E_BossAttack:
				packet := SC_BossAttackPacket{}
				mapstructure.Decode(event.data, &packet)
				broadcast(packet.write())
			default:
			}
		}
	}
}
