package main

func CS_LeaveGameHandler(session Session, data []byte) {
	recvData := CS_LeaveGamePacket{}
	recvData.read(data)

}

func CS_MoveHandler(session Session, data []byte) {
	recvData := CS_MovePacket{}
	recvData.read(data)

	event := Event{Type: E_MovePlayer, session: session, data: recvData}
	LogicChan <- event
}

func CS_ChatHandler(session Session, data []byte) {
	recvData := CS_ChatPacket{}
	recvData.read(data)

	event := Event{Type: E_ChatPlayer, session: session, data: recvData}
	LogicChan <- event
}

func CS_LoginHandler(session Session, data []byte) {
	recvData := CS_LoginPacket{}
	recvData.read(data)

	event := Event{Type: E_EnterPlayer, session: session, data: recvData}
	LogicChan <- event
}

func CS_MainSceneLoadHandler(session Session, data []byte) {
	recvData := CS_MainSceneLoadPacket{}
	recvData.read(data)

	event := Event{Type: E_BroadEnterPlayer, session: session}
	LogicChan <- event
}

func CS_SkinHandler(session Session, data []byte) {
	recvData := CS_SkinPacket{}
	recvData.read(data)

	session.player.skinId = recvData.skinId

	session.player.x = -127.5
	session.player.y = 0
	session.player.z = -30.2

	event := Event{Type: E_ChangeMainScene, session: session}
	LogicChan <- event
}

func CS_RollHandler(session Session, data []byte) {
	event := Event{Type: E_RollPlayer, session: session}
	LogicChan <- event
}

func CS_PlayerAttackHandler(session Session, data []byte) {
	event := Event{Type: E_AttackPlayer, session: session}
	LogicChan <- event
}

func CS_EnemyHitHandler(session Session, data []byte) {
	recvData := CS_EnemyHitPacket{}
	recvData.read(data)

	eid := recvData.eid

	enemy, exists := enemys[eid]
	if exists == false {
		return
	}

	enemy.mutex.Lock()
	enemy.hp -= 10
	if enemy.hp > 0 {
		packet := SC_EnemyHitPacket{id: recvData.eid, hp: enemy.hp}
		event := Event{Type: E_EnemyHit, session: session, data: packet}
		LogicChan <- event
	} else {
		delete(enemys, eid)
		packet := SC_EnemyDiePacket{id: recvData.eid, hp: 0}
		event := Event{Type: E_EnemyDie, session: session, data: packet}
		LogicChan <- event
	}
	enemy.mutex.Unlock()
}

func CS_PlayerHitHandler(session Session, data []byte) {
	event := Event{Type: E_HitPlayer, session: session}
	LogicChan <- event
}
