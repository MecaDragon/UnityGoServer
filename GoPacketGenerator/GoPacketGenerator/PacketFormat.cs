﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GoPacketGenerator
{
    class PacketFormat
    {
        // {0} 패킷 이름 / 번호 목록
        // {1} 패킷 목록
        public static string fileFormat =
@"package main

import (
	""bytes""
	""encoding/binary""
    ""reflect""
)

func sizeof(t reflect.Type) int16 {{
	switch t.Kind() {{
	case reflect.Array:
		if s := sizeof(t.Elem()); s >= 0 {{
			return int16(s) * int16(t.Len())

        }}

	case reflect.Struct:
		var sum int16
        sum = 0
		for i, n := 0, t.NumField(); i<n; i++ {{
			s := sizeof(t.Field(i).Type)
			if s< 0 {{
				return -1
			}}
            sum += s
		}}
		return sum
	case reflect.Bool, reflect.Uint8, reflect.Uint16, reflect.Uint32, reflect.Uint64,
		reflect.Int8, reflect.Int16, reflect.Int32, reflect.Int64,
		reflect.Float32, reflect.Float64, reflect.Complex64, reflect.Complex128:
		return int16(t.Size())
	case reflect.Slice:
		return 0
	case reflect.String:
		return 0
	}}
	return -1
}}

const (
    {0}
)

{1}
";
        // 4. 패킷 ID에 대응되는 패킷 Enum
        // 5. 다 추가되면 패킷 제네레이터 Program으로 이동

        // {0} 패킷 이름
        // {1} 패킷 번호
        public static string packetEnumFormat =
@"{0} = {1}";


        // {0} 패킷 이름
        // {1} 멤버 변수 이름
        // {2} 멤버 변수 Read
        // {3} 멤버 변수 Write
        // {4} Packet 뺀 이름
        // {5} 
        public static string packetFormat =
@"
type {0} struct {{
	Size int16
	Type int16
    {1}
}}

func (packet *{0}) getType() int16 {{
	return {4}
}}

func (packet *{0}) write() []byte {{
    buf := new(bytes.Buffer)
    packetSize := sizeof(reflect.TypeOf(*packet))
    {5}

	binary.Write(buf, binary.LittleEndian, packetSize)
	binary.Write(buf, binary.LittleEndian, packet.getType())
    {3}    

    return buf.Bytes()
}}

func (packet *{0}) read(data []byte) {{
    readbuf := bytes.NewBuffer(data)
	binary.Read(readbuf, binary.LittleEndian, &packet.Size)
	binary.Read(readbuf, binary.LittleEndian, &packet.Type)
    {2}
}}
";

        // {0} 변수 이름
        // {1} 변수 형식
        public static string memberFormat =
@"{0} {1};";


        // {0} 변수 이름
        public static string readFormat =
@"binary.Read(readbuf, binary.LittleEndian, &packet.{0})";

        // {0} 변수 이름
        public static string readStringFormat =
@"var {0}Size int16
binary.Read(readbuf, binary.LittleEndian, &{0}Size)
{0}Buf := make([]byte, {0}Size)
binary.Read(readbuf, binary.LittleEndian, &{0}Buf)
packet.{0} = string({0}Buf)
";

        // {0} 변수 이름
        public static string writeFormat =
@"binary.Write(buf, binary.LittleEndian, packet.{0})";

        // {0} 변수 이름
        public static string writeStringFormat =
@"{0}Size := int16(len(packet.{0}))
binary.Write(buf, binary.LittleEndian, {0}Size)
binary.Write(buf, binary.LittleEndian, []byte(packet.{0}))
";

        // {0} 변수 이름
        public static string writeStritngSizeFormat =
@"packetSize += 2
packetSize += int16(len(packet.{0}))
";

        // {0} 패킷핸들러 매핑작업
        // {1} 패킷핸들러 함수
		public static string handlePacketFormat =
@"package main

var PacketHandler map[int16]func(Session, []byte)

func HandlePacket_Init() {{
	PacketHandler = make(map[int16]func(Session, []byte))

	{0}
}}
";

        // {0} 패킷 이름
        public static string handleMappingFormat =
@"PacketHandler[{0}] = {0}Handler";
    }
}
