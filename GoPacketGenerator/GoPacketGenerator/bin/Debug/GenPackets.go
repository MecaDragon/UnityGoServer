package main

import (
	"bytes"
	"encoding/binary"
    "reflect"
)

func sizeof(t reflect.Type) int16 {
	switch t.Kind() {
	case reflect.Array:
		if s := sizeof(t.Elem()); s >= 0 {
			return int16(s) * int16(t.Len())

        }

	case reflect.Struct:
		var sum int16
        sum = 0
		for i, n := 0, t.NumField(); i<n; i++ {
			s := sizeof(t.Field(i).Type)
			if s< 0 {
				return -1
			}
            sum += s
		}
		return sum
	case reflect.Bool, reflect.Uint8, reflect.Uint16, reflect.Uint32, reflect.Uint64,
		reflect.Int8, reflect.Int16, reflect.Int32, reflect.Int64,
		reflect.Float32, reflect.Float64, reflect.Complex64, reflect.Complex128:
		return int16(t.Size())
	case reflect.Slice:
		return 0
	case reflect.String:
		return 0
	}
	return -1
}

const (
    CS_LeaveGame = 1
	CS_Move = 2
	SC_BroadcastEnterGame = 3
	SC_BroadcastLeaveGame = 4
	SC_LoginPlayer = 5
	SC_BroadcastMove = 6
	CS_Text = 7
	SC_Text = 8
	
)


type CS_LeaveGamePacket struct {
	Size int16
	Type int16
    
}

func (packet *CS_LeaveGamePacket) getType() int16 {
	return CS_LeaveGame
}

func (packet *CS_LeaveGamePacket) write() []byte {
    buf := new(bytes.Buffer)
    packetSize := sizeof(reflect.TypeOf(*packet))
    

	binary.Write(buf, binary.LittleEndian, packetSize)
	binary.Write(buf, binary.LittleEndian, packet.getType())
        

    return buf.Bytes()
}

func (packet *CS_LeaveGamePacket) read(data []byte) {
    readbuf := bytes.NewBuffer(data)
	binary.Read(readbuf, binary.LittleEndian, &packet.Size)
	binary.Read(readbuf, binary.LittleEndian, &packet.Type)
    
}

type CS_MovePacket struct {
	Size int16
	Type int16
    dir int32;
}

func (packet *CS_MovePacket) getType() int16 {
	return CS_Move
}

func (packet *CS_MovePacket) write() []byte {
    buf := new(bytes.Buffer)
    packetSize := sizeof(reflect.TypeOf(*packet))
    

	binary.Write(buf, binary.LittleEndian, packetSize)
	binary.Write(buf, binary.LittleEndian, packet.getType())
    binary.Write(buf, binary.LittleEndian, packet.dir)    

    return buf.Bytes()
}

func (packet *CS_MovePacket) read(data []byte) {
    readbuf := bytes.NewBuffer(data)
	binary.Read(readbuf, binary.LittleEndian, &packet.Size)
	binary.Read(readbuf, binary.LittleEndian, &packet.Type)
    binary.Read(readbuf, binary.LittleEndian, &packet.dir)
}

type SC_BroadcastEnterGamePacket struct {
	Size int16
	Type int16
    playerId int32;
	posX float32;
	posY float32;
	posZ float32;
}

func (packet *SC_BroadcastEnterGamePacket) getType() int16 {
	return SC_BroadcastEnterGame
}

func (packet *SC_BroadcastEnterGamePacket) write() []byte {
    buf := new(bytes.Buffer)
    packetSize := sizeof(reflect.TypeOf(*packet))
    

	binary.Write(buf, binary.LittleEndian, packetSize)
	binary.Write(buf, binary.LittleEndian, packet.getType())
    binary.Write(buf, binary.LittleEndian, packet.playerId)
	binary.Write(buf, binary.LittleEndian, packet.posX)
	binary.Write(buf, binary.LittleEndian, packet.posY)
	binary.Write(buf, binary.LittleEndian, packet.posZ)    

    return buf.Bytes()
}

func (packet *SC_BroadcastEnterGamePacket) read(data []byte) {
    readbuf := bytes.NewBuffer(data)
	binary.Read(readbuf, binary.LittleEndian, &packet.Size)
	binary.Read(readbuf, binary.LittleEndian, &packet.Type)
    binary.Read(readbuf, binary.LittleEndian, &packet.playerId)
	binary.Read(readbuf, binary.LittleEndian, &packet.posX)
	binary.Read(readbuf, binary.LittleEndian, &packet.posY)
	binary.Read(readbuf, binary.LittleEndian, &packet.posZ)
}

type SC_BroadcastLeaveGamePacket struct {
	Size int16
	Type int16
    playerId int32;
}

func (packet *SC_BroadcastLeaveGamePacket) getType() int16 {
	return SC_BroadcastLeaveGame
}

func (packet *SC_BroadcastLeaveGamePacket) write() []byte {
    buf := new(bytes.Buffer)
    packetSize := sizeof(reflect.TypeOf(*packet))
    

	binary.Write(buf, binary.LittleEndian, packetSize)
	binary.Write(buf, binary.LittleEndian, packet.getType())
    binary.Write(buf, binary.LittleEndian, packet.playerId)    

    return buf.Bytes()
}

func (packet *SC_BroadcastLeaveGamePacket) read(data []byte) {
    readbuf := bytes.NewBuffer(data)
	binary.Read(readbuf, binary.LittleEndian, &packet.Size)
	binary.Read(readbuf, binary.LittleEndian, &packet.Type)
    binary.Read(readbuf, binary.LittleEndian, &packet.playerId)
}

type SC_LoginPlayerPacket struct {
	Size int16
	Type int16
    isSelf bool;
	playerId int32;
	posX float32;
	posY float32;
	posZ float32;
}

func (packet *SC_LoginPlayerPacket) getType() int16 {
	return SC_LoginPlayer
}

func (packet *SC_LoginPlayerPacket) write() []byte {
    buf := new(bytes.Buffer)
    packetSize := sizeof(reflect.TypeOf(*packet))
    

	binary.Write(buf, binary.LittleEndian, packetSize)
	binary.Write(buf, binary.LittleEndian, packet.getType())
    binary.Write(buf, binary.LittleEndian, packet.isSelf)
	binary.Write(buf, binary.LittleEndian, packet.playerId)
	binary.Write(buf, binary.LittleEndian, packet.posX)
	binary.Write(buf, binary.LittleEndian, packet.posY)
	binary.Write(buf, binary.LittleEndian, packet.posZ)    

    return buf.Bytes()
}

func (packet *SC_LoginPlayerPacket) read(data []byte) {
    readbuf := bytes.NewBuffer(data)
	binary.Read(readbuf, binary.LittleEndian, &packet.Size)
	binary.Read(readbuf, binary.LittleEndian, &packet.Type)
    binary.Read(readbuf, binary.LittleEndian, &packet.isSelf)
	binary.Read(readbuf, binary.LittleEndian, &packet.playerId)
	binary.Read(readbuf, binary.LittleEndian, &packet.posX)
	binary.Read(readbuf, binary.LittleEndian, &packet.posY)
	binary.Read(readbuf, binary.LittleEndian, &packet.posZ)
}

type SC_BroadcastMovePacket struct {
	Size int16
	Type int16
    playerId int32;
	posX float32;
	posY float32;
	posZ float32;
}

func (packet *SC_BroadcastMovePacket) getType() int16 {
	return SC_BroadcastMove
}

func (packet *SC_BroadcastMovePacket) write() []byte {
    buf := new(bytes.Buffer)
    packetSize := sizeof(reflect.TypeOf(*packet))
    

	binary.Write(buf, binary.LittleEndian, packetSize)
	binary.Write(buf, binary.LittleEndian, packet.getType())
    binary.Write(buf, binary.LittleEndian, packet.playerId)
	binary.Write(buf, binary.LittleEndian, packet.posX)
	binary.Write(buf, binary.LittleEndian, packet.posY)
	binary.Write(buf, binary.LittleEndian, packet.posZ)    

    return buf.Bytes()
}

func (packet *SC_BroadcastMovePacket) read(data []byte) {
    readbuf := bytes.NewBuffer(data)
	binary.Read(readbuf, binary.LittleEndian, &packet.Size)
	binary.Read(readbuf, binary.LittleEndian, &packet.Type)
    binary.Read(readbuf, binary.LittleEndian, &packet.playerId)
	binary.Read(readbuf, binary.LittleEndian, &packet.posX)
	binary.Read(readbuf, binary.LittleEndian, &packet.posY)
	binary.Read(readbuf, binary.LittleEndian, &packet.posZ)
}

type CS_TextPacket struct {
	Size int16
	Type int16
    name string;
}

func (packet *CS_TextPacket) getType() int16 {
	return CS_Text
}

func (packet *CS_TextPacket) write() []byte {
    buf := new(bytes.Buffer)
    packetSize := sizeof(reflect.TypeOf(*packet))
    packetSize += 2
	packetSize += int16(len(packet.name))
	

	binary.Write(buf, binary.LittleEndian, packetSize)
	binary.Write(buf, binary.LittleEndian, packet.getType())
    nameSize := int16(len(packet.name))
	binary.Write(buf, binary.LittleEndian, nameSize)
	binary.Write(buf, binary.LittleEndian, []byte(packet.name))
	    

    return buf.Bytes()
}

func (packet *CS_TextPacket) read(data []byte) {
    readbuf := bytes.NewBuffer(data)
	binary.Read(readbuf, binary.LittleEndian, &packet.Size)
	binary.Read(readbuf, binary.LittleEndian, &packet.Type)
    var nameSize int16
	binary.Read(readbuf, binary.LittleEndian, &nameSize)
	nameBuf := make([]byte, nameSize)
	binary.Read(readbuf, binary.LittleEndian, &nameBuf)
	packet.name = string(nameBuf)
	
}

type SC_TextPacket struct {
	Size int16
	Type int16
    name string;
}

func (packet *SC_TextPacket) getType() int16 {
	return SC_Text
}

func (packet *SC_TextPacket) write() []byte {
    buf := new(bytes.Buffer)
    packetSize := sizeof(reflect.TypeOf(*packet))
    packetSize += 2
	packetSize += int16(len(packet.name))
	

	binary.Write(buf, binary.LittleEndian, packetSize)
	binary.Write(buf, binary.LittleEndian, packet.getType())
    nameSize := int16(len(packet.name))
	binary.Write(buf, binary.LittleEndian, nameSize)
	binary.Write(buf, binary.LittleEndian, []byte(packet.name))
	    

    return buf.Bytes()
}

func (packet *SC_TextPacket) read(data []byte) {
    readbuf := bytes.NewBuffer(data)
	binary.Read(readbuf, binary.LittleEndian, &packet.Size)
	binary.Read(readbuf, binary.LittleEndian, &packet.Type)
    var nameSize int16
	binary.Read(readbuf, binary.LittleEndian, &nameSize)
	nameBuf := make([]byte, nameSize)
	binary.Read(readbuf, binary.LittleEndian, &nameBuf)
	packet.name = string(nameBuf)
	
}

