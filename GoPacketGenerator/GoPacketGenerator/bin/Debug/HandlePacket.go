package main

var PacketHandler map[int16]func(Session, []byte)

func HandlePacket_Init() {
	PacketHandler = make(map[int16]func(Session, []byte))

	PacketHandler[CS_LeaveGame] = CS_LeaveGameHandler
	PacketHandler[CS_Move] = CS_MoveHandler
	PacketHandler[CS_Text] = CS_TextHandler
	
}
