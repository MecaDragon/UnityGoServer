﻿using System;
using System.IO;
using System.Xml;

namespace GoPacketGenerator
{
    class Program
    {
        static string genPackets;
        static ushort packetId;
        static string packetEnums;

        static string handlerMapping;

        static void Main(string[] args)
        {
            string pdlPath = "PDL.xml";

            XmlReaderSettings settings = new XmlReaderSettings()
            {
                IgnoreComments = true,
                IgnoreWhitespace = true
            };

            if (args.Length >= 1)
            {
                pdlPath = args[0];
            }

            using (XmlReader reader = XmlReader.Create(pdlPath, settings))
            {
                reader.MoveToContent();

                while (reader.Read())
                {
                    if (reader.Depth == 1 && reader.NodeType == XmlNodeType.Element)
                    {
                        ParsePacket(reader);
                    }
                }

                string fileText = string.Format(PacketFormat.fileFormat, packetEnums, genPackets);
                File.WriteAllText("GenPackets.go", fileText);

                string fileHandleText = string.Format(PacketFormat.handlePacketFormat, handlerMapping);
                File.WriteAllText("HandlePacket.go", fileHandleText);
            }
        }

        public static void ParsePacket(XmlReader r)
        {
            if (r.NodeType == XmlNodeType.EndElement)
            {
                return;
            }

            if (r.Name.ToLower() != "packet")
            {
                Console.WriteLine("Invalid packet node");
                return;
            }

            string packetName = r["name"];
            if (string.IsNullOrEmpty(packetName))
            {
                Console.WriteLine("Packet Without Name");
                return;
            }

            Tuple<string, string, string, string> tuple = ParseMembers(r);
            genPackets += string.Format(PacketFormat.packetFormat, packetName + "Packet", tuple.Item1, tuple.Item2, tuple.Item3, packetName, tuple.Item4);
            packetEnums += string.Format(PacketFormat.packetEnumFormat, packetName, ++packetId) + Environment.NewLine + "\t";
            if (packetName.StartsWith("CS_"))
            {
                handlerMapping += string.Format(PacketFormat.handleMappingFormat, packetName) + Environment.NewLine + "\t";
            }
        }

        // {1} 멤버 변수 이름
        // {2} 멤버 변수 Read
        // {3} 멤버 변수 Write
        public static Tuple<string, string, string, string> ParseMembers(XmlReader r)
        {
            string packetName = r["name"];

            string memberCode = "";
            string readCode = "";
            string writeCode = "";
            string stringSizeCode = "";

            int memberDepth = r.Depth + 1;
            while (r.Read())
            {
                if (r.Depth != memberDepth)
                {
                    break;
                }

                string memberName = r["name"];
                if (string.IsNullOrEmpty(memberName))
                {
                    Console.WriteLine("Member without name");
                    return null;
                }

                if (string.IsNullOrEmpty(memberCode) == false)
                {
                    memberCode += Environment.NewLine;
                }
                if (string.IsNullOrEmpty(readCode) == false)
                {
                    readCode += Environment.NewLine;
                }
                if (string.IsNullOrEmpty(writeCode) == false)
                {
                    writeCode += Environment.NewLine;
                }

                string memberType = r.Name.ToLower();
                switch (memberType)
                {
                    case "int":
                    case "float":
                        memberType += "32";
                        memberCode += string.Format(PacketFormat.memberFormat, memberName, memberType);
                        readCode += string.Format(PacketFormat.readFormat, memberName);
                        writeCode += string.Format(PacketFormat.writeFormat, memberName);
                        break;
                    case "byte":
                    case "bool":
                    case "short":
                    case "ushort":
                    case "long":
                    case "double":
                        memberCode += string.Format(PacketFormat.memberFormat, memberName, memberType);
                        readCode += string.Format(PacketFormat.readFormat, memberName);
                        writeCode += string.Format(PacketFormat.writeFormat, memberName);
                        break;
                    case "string":
                        memberCode += string.Format(PacketFormat.memberFormat, memberName, memberType);
                        readCode += string.Format(PacketFormat.readStringFormat, memberName);
                        writeCode += string.Format(PacketFormat.writeStringFormat, memberName);
                        stringSizeCode += string.Format(PacketFormat.writeStritngSizeFormat, memberName);
                        break;
                    default:
                        break;
                }
            }

            memberCode = memberCode.Replace("\n", "\n\t");
            readCode = readCode.Replace("\n", "\n\t");
            writeCode = writeCode.Replace("\n", "\n\t");
            stringSizeCode = stringSizeCode.Replace("\n", "\n\t");

            return new Tuple<string, string, string, string>(memberCode, readCode, writeCode, stringSizeCode);
        }

        public static string FirstCharToUpper(string input)
        {
            if (string.IsNullOrEmpty(input))
            {
                return string.Empty;
            }

            return input[0].ToString().ToUpper() + input.Substring(1);
        }

        public static string FirstCharToLower(string input)
        {
            if (string.IsNullOrEmpty(input))
            {
                return string.Empty;
            }

            return input[0].ToString().ToLower() + input.Substring(1);
        }
    }
}
